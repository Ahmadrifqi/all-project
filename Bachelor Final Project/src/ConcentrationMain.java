import Utils.DecimalUtils;
import Utils.DeleteFiles;
import org.jfree.ui.RefineryUtilities;
import org.jzy3d.analysis.AnalysisLauncher;
import processor.Constant;
import processor.Field.PlotVelocityField;
import processor.Field.TestCaseField;
import processor.GridGeneration.GridGeneration;
import processor.Input_User;
import processor.flowDispersion.PlotConcentration;
import processor.flowDispersion.StructuredUpwind;
import processor.flowDispersion.TestCase;
import processor.objectClasses.Velocity;

import java.io.FileWriter;
import java.io.PrintWriter;

public class ConcentrationMain {
    public static void main(String[] Args) throws Exception{
        long starttime = System.nanoTime();
        float totaldt = Constant.timeStep;
        int totalconcentration = 0;
        Input_User input = new Input_User();
        double[][] Concentrationt = new double[input.grid_y][input.grid_x],
                Concentrationmint = new double[input.grid_y][input.grid_x],
                Concentrationmin2t = new double[input.grid_y][input.grid_x];
        DecimalUtils round = new DecimalUtils();
        DeleteFiles delete = new DeleteFiles();
        delete.Delete("Concentration");

        GridGeneration Grid = new GridGeneration();
        Grid.initiation();
        Grid.PointDefinition(input.locations);
        Grid.Mesh_Point_Definition("Structured");
        Grid.Total_Grid();
        Grid.Scalar_grid();
        Grid.Staggered_Grid();

        TestCase CaseConcentration = new TestCase();
        CaseConcentration.setConcentration1(Grid.getCoordinate_scalar());
        //CaseConcentration.setConcentration3();
        double Temp[][] = CaseConcentration.getConcentration();
        double Concentration[][] = new double[input.grid_y][input.grid_x];

        for (int i=0; i<input.grid_y; i++){
            for (int j=0; j<input.grid_x; j++){
                Concentration[i][j] = Temp[i][j];
                if (Temp[i][j] != 0){
                    totalconcentration++;
                    System.out.println(i + " " + j);
                }
            }
        }

        double seperateConcentration[][][] = new double[totalconcentration][input.grid_y][input.grid_x];

        int number = 0;
        for (int i=0; i<input.grid_y; i++){
            for (int j=0; j<input.grid_x; j++) {
                Concentrationmint[i][j] = Temp[i][j];
                Concentrationmin2t[i][j] = Temp[i][j];
                if (Temp[i][j] != 0) {
                    seperateConcentration[number][i][j] = Temp[i][j];
                    number++;
                }
            }
        }
        number = 0;

        TestCaseField Case = new TestCaseField(input.grid_x, input.grid_y);
        Case.initiation();
        //Case.CirculatingIrrotational(Grid.getCoordinate_scalar(), 0.001, input.centre);
        //Case.ConstantYVelocity();
        Case.ConstantXVelocity();
        //Case.SourceVelocity(500, Grid.getCoordinate_scalar());
        //Case.SinkVelocity(5000, Grid.getCoordinate_scalar());
        //Case.VariedVelocityMid(Grid.getCoordinate_scalar(), input.centre);
        //Case.VariedVelocity(Grid.getCoordinate_scalar(), input.centre);
        //Case.ConstantObliqueVelocity();
        Case.setVface();
        Velocity[][] velocities = Case.getVelocity();

        PrintWriter print = new PrintWriter(new FileWriter("Concentration.txt", true));

        print.println("Peclet Number : " + String.valueOf(Constant.rho*2.0/(Constant.eddyK/
                (Grid.getCoordinate_scalar()[input.grid_y][input.grid_x].getX() - Grid.getCoordinate_scalar()[input.grid_y][input.grid_x-1].getX()))));
        print.println();

        print.println("Kondisi Awal : ");
        for (int i = input.grid_y - 1; i > -1; i--) {
            for (int j = 0; j < input.grid_x; j++) {
                print.print(round.round(Concentration[i][j], 4) + " ");
            }
            print.println();
        }
        print.println();

        while (number < totalconcentration) {
            StructuredUpwind Upwind = new StructuredUpwind(input.grid_x, input.grid_y, seperateConcentration[number]);
            for (int i=0; i<input.grid_y; i++){
                for (int j=0; j<input.grid_x; j++){
                    Concentrationmint[i][j] = seperateConcentration[number][i][j];
                    Concentrationmin2t[i][j] = seperateConcentration[number][i][j];
                }
            }
            while (totaldt < 1.5) {
                Upwind.Swipe_X(Concentrationmint, Concentrationmin2t, velocities, Grid, totaldt);
                for (int i=0; i<input.grid_y; i++){
                    for (int j=0; j<input.grid_x; j++){
                        Concentrationt[i][j] = Upwind.getConcentration()[i][j];
                    }
                }

                for (int i = 0; i < input.grid_y; i++) {
                    for (int j = 0; j < input.grid_x; j++) {
                        Concentrationmin2t[i][j] = Concentrationmint[i][j];
                        Concentrationmint[i][j] = Concentrationt[i][j];
                    }
                }

                totaldt = totaldt + Constant.timeStep;
                System.out.println("Waktu Simulasi : " + totaldt + " s");
            }
            for (int i = 0; i < input.grid_y; i++) {
                for (int j = 0; j < input.grid_x; j++) {
                    seperateConcentration[number][i][j] = Concentrationt[i][j];
                }
            }
            totaldt = Constant.timeStep;
            number++;
        }

        for (int i = 0; i < input.grid_y; i++) {
            for (int j = 0; j < input.grid_x; j++) {
                double sum = 0;
                for (int k=0; k<totalconcentration; k++){
                    sum += seperateConcentration[k][i][j];
                }
                Concentrationt[i][j] = sum;
            }
        }

        print.println("Kondisi setelah perhitungaan : ");
        for (int i = input.grid_y - 1; i > -1; i--) {
            for (int j = 0; j < input.grid_x; j++) {
                print.print(round.round(Concentrationt[i][j], 4) + " ");
            }
            print.println();
        }
        print.println();

        print.println("Koordinat Y : ");
        for (int i=input.grid_y-1; i>-1; i--){
            for (int j=0; j<input.grid_x; j++){
                print.print(round.round(Grid.getCoordinate_scalar()[i][j].getY(), 4) + " ");
            }
            print.println();
        }
        print.close();

        long EndTime = System.nanoTime();
        System.out.println("Lama Waktu Running :" + (EndTime-starttime)/Math.pow(10,9) + " s");

        PlotConcentration plot = new PlotConcentration();
        plot.input(Concentrationt);
        AnalysisLauncher.open(plot);
//
//        double[] dataCon = new double[input.grid_y], dataY = new double[input.grid_y];
//        for (int i=0; i<input.grid_x; i++){
//           dataCon[i] = Concentrationt[i][(input.grid_x-1)/2];
//           dataY[i] = Grid.getCoordinate_scalar()[i][(input.grid_x-1)/2].getY();
//        }
//
//        PlotVariasiArahY ploty = new PlotVariasiArahY("",
//               "Plot Variasi Konsentrasi Terhadap Sumbu Y dengan X Tertentu",
//               dataCon,
//               dataY);
//        ploty.pack();
//        RefineryUtilities.centerFrameOnScreen(ploty);
//        ploty.setVisible(true);

       //PlotVelocityField plotVelocityField = new PlotVelocityField(velocities, Grid.getStaggerV());
    }
}
