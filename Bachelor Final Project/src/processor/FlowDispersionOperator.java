//package processor;
//
//import processor.objectClasses.Pressure;
//import processor.objectClasses.Toxic;
//import processor.objectClasses.Velocity;
//
//import java.awt.*;
//import java.awt.geom.Point2D;
//import java.util.ArrayList;
//
//public class FlowDispersionOperator extends FieldOperator {
//    private Toxic[][] toxicField;
//    private ArrayList<Point> toxicSourceIdx;
//    private ArrayList<Point> toxicResultIdx;
//
//    public FlowDispersionOperator(VelocityFieldOperator SVFO){
//        super(SVFO.area);
//        this.velocityField = SVFO.getVelocityField();
//        this.pressureField = SVFO.getPressureField();
//        toxicField = new Toxic[SVFO.getLength()][SVFO.getWidth()];
//        toxicSourceIdx = new ArrayList<>();
//        toxicResultIdx = new ArrayList<>();
//    }
//
//    //menginisialisasi setiap elemen grid kecepatan dan tekanan dengan nilai 0
//    @Override
//    public void initiate() {
//        for(int x = 0; x< length; x++){
//            for(int y = 0; y< width; y++){
//                toxicField[x][y] = new Toxic();
//            }
//        }
//    }
//
//    public void addToxic(Toxic toxic, Point2D.Double location){
//        Point idx = realLocationToGridIdx(location);
//        toxicField[idx.x][idx.y] = toxic;
//        toxicSourceIdx.add(idx);
//        toxicResultIdx.add(idx);
//    }
//
//    public Toxic[][] getToxicField() {
//        return toxicField;
//    }
//
//    public void solve(double time){
//        int range = (int)(time*100);
//        int halfRange=range/2;
//        for (Point idx:toxicSourceIdx) {
//            for (int x = -1 * halfRange; x < halfRange; x++) {
//                for (int y = -1 * halfRange; y < halfRange; y++) {
//                    int xIdx = idx.x -x;
//                    int yIdx = idx.y -y;
//                    if(xIdx>=0 && xIdx<length && yIdx>=0 && yIdx < width){
//                        toxicResultIdx.add(new Point(xIdx,yIdx));
//
//                    }
//                }
//            }
//        }
//    }
//
//    public ArrayList<Point> getToxicResultIdx() {
//        return toxicResultIdx;
//    }
//
//    public ArrayList<Point> getToxicSourceIdx() {
//        return toxicSourceIdx;
//    }
//}
