package processor.GridGeneration;

import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

import processor.Input_User;
import processor.objectClasses.CellArea;
import processor.objectClasses.Coordinate;

public class GridGeneration {
    Input_User input = new Input_User();
    private Coordinate coordinate_point[][], Point[][];
    private Coordinate coordinate_scalar[][], StaggerU[][], StaggerV[][];
    private CellArea Area_Scalar[][], Area_staggerU[][], Area_staggerV[][];
    private double[][][] points;
    private double dx, dy;
    private int length, width;
    private String mesh_type;

    public GridGeneration(){
        length = input.grid_x;
        width = input.grid_y;
        mesh_type = input.Type_Mesh;

        coordinate_point = new Coordinate[width+1][length+1];
        Point = new  Coordinate[width+3][length+3];
        coordinate_scalar = new Coordinate[width+2][length+2];
        StaggerU = new Coordinate[width+3][length+2];
        StaggerV = new Coordinate[width+2][length+3];

        Area_Scalar = new CellArea[width+2][length+2];
        Area_staggerU = new CellArea[width+2][length+1];
        Area_staggerV = new CellArea[width+1][length+2];

        points = new double[1][2][4];
    }

    public void initiation(){
        for (int i=0; i<width+3; i++){
            for (int j=0; j<length+3; j++){
                Point[i][j] = new Coordinate();
                if (i<width+1 && j<length+1) {
                    coordinate_point[i][j] = new Coordinate();
                }
                if (i<width+2 && j<length+2){
                    coordinate_scalar[i][j] = new Coordinate();
                    Area_Scalar[i][j] = new CellArea();
                    if (j<length+1){
                        Area_staggerU[i][j] = new CellArea();
                    }
                    if (i<width+1){
                        Area_staggerV[i][j] = new CellArea();
                    }
                }
                if (j<length+2){
                    StaggerU[i][j] = new Coordinate();
                }
                if (i<width+2){
                    StaggerV[i][j] = new Coordinate();
                }
            }
        }
    }

    public void PointDefinition(double locations[][][]){
        for (int j=0; j<4; j++) {
            points[0][0][j] = locations[0][0][j];
            points[0][1][j] = locations[0][1][j];
        }
    }

    public void Mesh_Point_Definition(String condition){
        if (condition == "Structured"){
            if (mesh_type.equals("Normal")){
                //generating boundary points
                for (int point = 0; point < 4; point++) {
                    for (int i=0; i<width+1; i++) {
                        for (int j=0; j<length+1; j++) {
                            if (point == 0 && j>0) {
                                dx = (points[0][0][point + 1] - points[0][0][point])/length;
                                dy = (points[0][1][point + 1] - points[0][1][point])/length;
                                coordinate_point[0][0].setX(points[0][0][0]);
                                coordinate_point[0][0].setY(points[0][1][0]);
                                coordinate_point[0][j].setX(coordinate_point[0][j-1].getX() + dx);
                                coordinate_point[0][j].setY(coordinate_point[0][j-1].getY() + dy);
                            }
                            if (point == 1 && i>0) {
                                dx = (points[0][0][point + 1] - points[0][0][point])/width;
                                dy = (points[0][1][point + 1] - points[0][1][point])/width;
                                coordinate_point[0][length].setX(points[0][0][1]);
                                coordinate_point[0][length].setY(points[0][1][1]);
                                coordinate_point[i][length].setX(coordinate_point[i-1][length].getX() + dx);
                                coordinate_point[i][length].setY(coordinate_point[i-1][length].getY() + dy);
                            }
                            if (point == 2 && j>0) {
                                dx = (points[0][0][point + 1] - points[0][0][point])/length;
                                dy = (points[0][1][point + 1] - points[0][1][point])/length;
                                coordinate_point[width][length].setX(points[0][0][2]);
                                coordinate_point[width][length].setY(points[0][1][2]);
                                coordinate_point[width][length-j].setX(coordinate_point[width][length-(j-1)].getX() + dx);
                                coordinate_point[width][length-j].setY(coordinate_point[width][length-(j-1)].getY() + dy);
                            }
                            if (point == 3 && i>0) {
                                dx = (points[0][0][3] - points[0][0][0])/width;
                                dy = (points[0][1][3] - points[0][1][0])/width;
                                coordinate_point[width][0].setX(points[0][0][3]);
                                coordinate_point[width][0].setY(points[0][1][3]);
                                coordinate_point[i][0].setX(coordinate_point[i-1][0].getX() + dx);
                                coordinate_point[i][0].setY(coordinate_point[i-1][0].getY() + dy);
                            }
                        }
                    }
                }
            }
                //generating internal points
            for (int i=1; i<width; i++){
                for (int j=1; j<length; j++){
                    double mh, mv, a;
                    mh = (coordinate_point[i][length].getY() - coordinate_point[i][0].getY())/
                            (coordinate_point[i][length].getX() - coordinate_point[i][0].getX());

                    mv = (coordinate_point[width][j].getY() - coordinate_point[0][j].getY())/
                            (coordinate_point[width][j].getX() - coordinate_point[0][j].getX());

                    a = mh*coordinate_point[i][length].getX() - mv*coordinate_point[0][j].getX() +
                            coordinate_point[0][j].getY() - coordinate_point[i][length].getY();

                    if (mv != Double.POSITIVE_INFINITY) {
                        coordinate_point[i][j].setX(a / (mh - mv));
                        coordinate_point[i][j].setY(mv * (coordinate_point[i][j].getX() - coordinate_point[0][j].getX()) + coordinate_point[0][j].getY());
                    }

                    if (mv > Math.pow(10, 6) || mv < -Math.pow(10, 6)){
                        coordinate_point[i][j].setX(coordinate_point[0][j].getX());
                        coordinate_point[i][j].setY(mh*(coordinate_point[i][j].getX() - coordinate_point[i][0].getX()) + coordinate_point[i][0].getY());
                    }

//                    if (mh == 0){
//                        coordinate_point[i][j].setY(coordinate_point[i][0].getY());
//                        coordinate_point[i][j].setX((coordinate_point[i][j].getY()-coordinate_point[i][length+2].getY())/mv + coordinate_point[i][length+2].getX());
//                    }

                    if ((mv == Double.POSITIVE_INFINITY || mv == Double.NEGATIVE_INFINITY) && mh == 0){
                        double dy = coordinate_point[i][0].getY() - coordinate_point[i-1][0].getY();
                        double dx = coordinate_point[0][j].getX() - coordinate_point[0][j-1].getX();
                        coordinate_point[i][j].setY(coordinate_point[i-1][j].getY() + dy);
                        coordinate_point[i][j].setX(coordinate_point[i][j-1].getX() + dx);
                    }
                }
            }
            }

            if (mesh_type.equals("Adaptive")){}


        if (condition == "Unstructured"){}
    }

    public void Total_Grid(){
        //Including Boundary grid
        for (int i=0; i<length+1; i++){
            //North Boundary
            Point[width+2][i+1].setY(2*coordinate_point[width][i].getY() - coordinate_point[width-1][i].getY());
            //South Grid
            Point[0][i+1].setY(2*coordinate_point[0][i].getY() - coordinate_point[1][i].getY());

            Point[0][i+1].setX(coordinate_point[width][i].getX());
            Point[width+2][i+1].setX(coordinate_point[width][i].getX());
        }

        for (int i=0; i<width+1; i++){
            //West Boundary
            Point[i+1][0].setX(2*coordinate_point[i][0].getX() - coordinate_point[i][1].getX());
            //East Boundary
            Point[i+1][length+2].setX(2*coordinate_point[i][length].getX() - coordinate_point[i][length-1].getX());

            Point[i+1][0].setY(coordinate_point[i][0].getY());
            Point[i+1][length+2].setY(coordinate_point[i][length].getY());
        }

        Point[0][0].setX(Point[1][0].getX());
        Point[0][0].setY(Point[0][1].getY());
        Point[0][length+2].setX(Point[1][length+2].getX());
        Point[0][length+2].setY(Point[0][length+1].getY());
        Point[width+2][0].setX(Point[width+1][0].getX());
        Point[width+2][0].setY(Point[width+2][1].getY());
        Point[width+2][length+2].setX(Point[width+1][length+2].getX());
        Point[width+2][length+2].setY(Point[width+2][length+1].getY());

        for (int i=0; i<width+2; i++){
            for (int j=0; j<length+2; j++){
                if (i < width+1 && j<length+1){
                    Point[i+1][j+1].setX(coordinate_point[i][j].getX());
                    Point[i+1][j+1].setY(coordinate_point[i][j].getY());
                }
            }
        }
    }

    public void Scalar_grid(){
        for (int i = 0; i < width + 2; i++) {
            for (int j = 0; j < length + 2; j++) {
                double avg_x, avg_y;
                avg_x = (Point[i][j].getX() + Point[i + 1][j].getX() + Point[i][j + 1].getX() + Point[i + 1][j + 1].getX()) / 4;
                avg_y = (Point[i][j].getY() + Point[i + 1][j].getY() + Point[i][j + 1].getY() + Point[i + 1][j + 1].getY()) / 4;
                coordinate_scalar[i][j].setX(avg_x);
                coordinate_scalar[i][j].setY(avg_y);
                Area_Scalar[i][j].setDe(Point[i + 1][j + 1].getY() - Point[i][j + 1].getY());
                Area_Scalar[i][j].setDw(Point[i][j].getY() - Point[i + 1][j].getY());
                Area_Scalar[i][j].setDn(Point[i + 1][j + 1].getX() - Point[i + 1][j].getX());
                Area_Scalar[i][j].setDs(Point[i][j].getX() - Point[i][j + 1].getX());
                double term1, term2, term3, term4;
                term1 = Point[i][j + 1].getX() * Point[i][j].getY() - Point[i][j + 1].getY() * Point[i][j].getX();
                term2 = Point[i + 1][j + 1].getX() * Point[i][j + 1].getY() - Point[i + 1][j + 1].getY() * Point[i][j + 1].getX();
                term3 = Point[i + 1][j].getX() * Point[i + 1][j + 1].getY() - Point[i + 1][j].getY() * Point[i + 1][j + 1].getX();
                term4 = Point[i][j].getX() * Point[i + 1][j].getY() - Point[i][j].getY() * Point[i + 1][j].getX();
                Area_Scalar[i][j].setArea(Math.abs((term1 + term2 + term3 + term4) / 2));
            }
        }
    }

    public void Staggered_Grid(){
        for (int i=0; i<width+3; i++){
            for (int j=0; j<length+2; j++){
                StaggerU[i][j].setX((Point[i][j].getX()+Point[i][j+1].getX())/2);
                StaggerU[i][j].setY((Point[i][j].getY()+Point[i][j+1].getY())/2);
            }
        }
        for (int i=0; i<width+2; i++){
            for (int j=0; j<length+1; j++){
                Area_staggerU[i][j].setDw(StaggerU[i][j].getY() - StaggerU[i+1][j].getY());
                Area_staggerU[i][j].setDe(StaggerU[i+1][j+1].getY() - StaggerU[i][j+1].getY());
                Area_staggerU[i][j].setDn(StaggerU[i+1][j].getX() - StaggerU[i+1][j+1].getX());
                Area_staggerU[i][j].setDs(StaggerU[i][j+1].getX() - StaggerU[i][j].getX());

                double term1, term2, term3, term4;
                term1 = StaggerU[i][j+1].getX()*StaggerU[i][j].getY() - StaggerU[i][j+1].getY()*StaggerU[i][j].getX();
                term2 = StaggerU[i+1][j+1].getX()*StaggerU[i][j+1].getY() - StaggerU[i+1][j+1].getY()*StaggerU[i][j+1].getX();
                term3 = StaggerU[i+1][j].getX()*StaggerU[i+1][j+1].getY() - StaggerU[i+1][j].getY()*StaggerU[i+1][j+1].getX();
                term4 = StaggerU[i][j].getX()*StaggerU[i+1][j].getY() - StaggerU[i][j].getY()*StaggerU[i+1][j].getX();
                Area_staggerU[i][j].setArea(Math.abs((term1+term2+term3+term4)/2));
            }
        }
        for (int i=0; i<width+2; i++){
            for (int j=0; j<length+3; j++){
                StaggerV[i][j].setX((Point[i][j].getX()+Point[i+1][j].getX())/2);
                StaggerV[i][j].setY((Point[i][j].getY()+Point[i+1][j].getY())/2);
            }
        }
        for (int i=0; i<length+2; i++){
            for (int j=0; j<width+1; j++){
                Area_staggerV[j][i].setDe(StaggerV[j+1][i].getY() - StaggerV[j][i].getY());
                Area_staggerV[j][i].setDw(StaggerV[j][i].getY() - StaggerV[j+1][i].getY());
                Area_staggerV[j][i].setDn(StaggerV[j+1][i].getX() - StaggerV[j+1][i+1].getX());
                Area_staggerV[j][i].setDs(StaggerV[j][i+1].getX() - StaggerV[j+1][i].getX());

                double term1, term2, term3, term4;
                term1 = StaggerV[j][i+1].getX()*StaggerV[j][i].getY() - StaggerV[j][i+1].getY()*StaggerV[j][i].getX();
                term2 = StaggerV[j+1][i+1].getX()*StaggerV[j][i+1].getY() - StaggerV[j+1][i+1].getY()*StaggerV[j][i+1].getX();
                term3 = StaggerV[j+1][i].getX()*StaggerV[j+1][i+1].getY() - StaggerV[j+1][i].getY()*StaggerV[j+1][i+1].getX();
                term4 = StaggerV[j][i].getX()*StaggerV[j+1][i].getY() - StaggerV[j][i].getY()*StaggerV[j+1][i].getX();
                Area_staggerV[j][i].setArea(Math.abs((term1+term2+term3+term4)/2));
            }
        }
    }

    public Coordinate[][] getCoordinate_scalar() {
        return coordinate_scalar;
    }

    public Coordinate[][] getPoint() {
        return Point;
    }

    public Coordinate[][] getStaggerU() {
        return StaggerU;
    }

    public Coordinate[][] getStaggerV() {
        return StaggerV;
    }

    public CellArea[][] getArea_Scalar() {
        return Area_Scalar;
    }

    public CellArea[][] getArea_staggerU() {
        CellArea[][] u = Area_staggerU;
        return Area_staggerU;
    }

    public CellArea[][] getArea_staggerV() {
        return Area_staggerV;
    }

    /*/public static void main(String Args[]){
        GridGeneration test = new GridGeneration();
        test.initiation();
        double[][][] locations = new double[1][2][4];
        locations[0][0][0] = 2;
        locations[0][1][0] = 0;
        locations[0][0][1] = 8;
        locations[0][1][1] = 3;
        locations[0][0][2] = 8;
        locations[0][1][2] = 6;
        locations[0][0][3] = 2;
        locations[0][1][3] = 5;
        test.PointDefinition(locations);
        test.Mesh_Point_Definition("Structured");
        test.Total_Grid();
        test.Staggered_Grid();
        test.Scalar_grid();
        CellArea[][] a = test.getArea_staggerU();

        int des = 1;
    }/**/
}
