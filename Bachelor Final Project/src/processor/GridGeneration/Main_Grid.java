package processor.GridGeneration;
import org.jfree.ui.RefineryUtilities;
import processor.Input_User;
import processor.objectClasses.CellArea;

import java.io.IOException;
import java.io.PrintWriter;


public class Main_Grid {

    public static void main(String[] Args) throws IOException {
        Input_User input = new Input_User();
        GridGeneration grid = new GridGeneration();
        grid.initiation();
        grid.PointDefinition(input.locations);
        grid.Mesh_Point_Definition("Structured");
        grid.Total_Grid();
        grid.Scalar_grid();
        CellArea[][] Area_Scalar = grid.getArea_Scalar();

        PrintWriter print = new PrintWriter("Face Vector.txt");
        print.println("Face Dn :");
        for (int i = 0; i < input.grid_y + 2; i++) {
            for (int j = 0; j < input.grid_x + 2; j++) {
                print.print(Area_Scalar[i][j].getDn() + " ");
            }
            print.println();
        }
        print.println();

        print.println("Face Ds :");
        for (int i = 0; i < input.grid_y + 2; i++) {
            for (int j = 0; j < input.grid_x + 2; j++) {
                print.print(Area_Scalar[i][j].getDs() + " ");
            }
            print.println();
        }
        print.close();

        PlotGrid plotGrid = new PlotGrid("Plot Grid");
        plotGrid.pack();
        RefineryUtilities.centerFrameOnScreen(plotGrid);
        plotGrid.setVisible(true);

//        PlotStaggeredGrid plotStaggeredGrid = new PlotStaggeredGrid("Plot Staggered Grid");
//        plotStaggeredGrid.pack();
//        RefineryUtilities.centerFrameOnScreen(plotStaggeredGrid);
//        plotStaggeredGrid.setVisible(true);
    }
}
