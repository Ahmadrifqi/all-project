package processor.GridGeneration;

import java.awt.Color;
import java.awt.BasicStroke;

import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import processor.Input_User;
import processor.objectClasses.Coordinate;

public class PlotGrid extends ApplicationFrame {
    
    public PlotGrid(String Title){
        super(Title);
        JFreeChart xylineChart = ChartFactory.createScatterPlot(
                "Plot Titik Mesh" ,
                "Sumbu X" ,
                "Sumbu Y" ,
                createDataset() ,
                PlotOrientation.VERTICAL ,
                true , true , false);

        XYPlot xyPlot = (XYPlot) xylineChart.getPlot();
        xyPlot.setDomainCrosshairVisible(true);
        xyPlot.setRangeCrosshairVisible(true);

        ChartPanel chartPanel = new ChartPanel( xylineChart );
        chartPanel.setPreferredSize( new java.awt.Dimension( 1200 , 750 ) );

//        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer( );
//        renderer.setSeriesPaint( 0 , Color.BLUE );
//        renderer.setSeriesPaint(1, Color.BLUE);
//        renderer.setSeriesStroke( 0 , new BasicStroke( 4.0f ) );
//        renderer.setSeriesStroke(1, new BasicStroke(4.0f));
//        xyPlot.setRenderer(renderer);
        setContentPane( chartPanel );

        NumberAxis domain = (NumberAxis) xyPlot.getDomainAxis();
        domain.setRange(-250, 1250);
        domain.setTickUnit(new NumberTickUnit(250));
        NumberAxis range = (NumberAxis) xyPlot.getRangeAxis();
        range.setRange(-250,1250);
        range.setTickUnit(new NumberTickUnit(250));
    }

    private static XYDataset createDataset(){
        Input_User input = new Input_User();
        GridGeneration test = new GridGeneration();
        test.initiation();
        double[][][] locations = input.locations;
        test.PointDefinition(locations);
        test.Mesh_Point_Definition("Structured");
        test.Total_Grid();
        test.Scalar_grid();
        Coordinate[][] coordinate = test.getPoint();
        Coordinate[][] scalar = test.getCoordinate_scalar();

        XYSeries data = new XYSeries("Point problem");
        for (int i=0; i<4; i++){
            data.add(locations[0][0][i], locations[0][1][i]);
        }

        XYSeries data2 = new XYSeries("Point Problem");
        data2.add(locations[0][0][3], locations[0][1][3]);
        data2.add(locations[0][0][0], locations[0][1][0]);

        XYSeries point_location = new XYSeries("Mesh Location");
        for (int i=input.grid_y+2; i>-1; i--){
            for (int j=0; j<input.grid_x+3; j++){
                point_location.add(coordinate[i][j].getX(), coordinate[i][j].getY());
            }
        }

        XYSeries Scalar_location = new XYSeries("Scalar Point Location");
        for (int i=0; i<input.grid_y+2; i++){
            for (int j=0; j<input.grid_x+2; j++){
                Scalar_location.add(scalar[i][j].getX(), scalar[i][j].getY());
            }
        }

        XYSeriesCollection total = new XYSeriesCollection();
        total.addSeries(data);
        total.addSeries(point_location);
        total.addSeries(Scalar_location);

        return total;
    }
}
