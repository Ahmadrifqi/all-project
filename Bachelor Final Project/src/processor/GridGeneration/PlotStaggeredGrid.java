package processor.GridGeneration;

import java.awt.Color;
import java.awt.BasicStroke;

import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

import processor.Input_User;
import processor.objectClasses.Coordinate;

public class PlotStaggeredGrid extends ApplicationFrame{
    public PlotStaggeredGrid(String Title){
        super(Title);
        JFreeChart xylineChart = ChartFactory.createScatterPlot(
                "Plot Titik Mesh" ,
                "Sumbu X" ,
                "Sumbu Y" ,
                createDataset() ,
                PlotOrientation.VERTICAL ,
                true , true , false);

        XYPlot xyPlot = (XYPlot) xylineChart.getPlot();
        xyPlot.setDomainCrosshairVisible(true);
        xyPlot.setRangeCrosshairVisible(true);

        ChartPanel chartPanel = new ChartPanel( xylineChart );
        chartPanel.setPreferredSize( new java.awt.Dimension( 1200 , 750 ) );
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer( );
        setContentPane(chartPanel);

        NumberAxis domain = (NumberAxis) xyPlot.getDomainAxis();
        domain.setRange(-5, 15);
        domain.setTickUnit(new NumberTickUnit(0.5));
        NumberAxis range = (NumberAxis) xyPlot.getRangeAxis();
        range.setRange(-5,15);
        range.setTickUnit(new NumberTickUnit(0.5));
    }

    private static XYDataset createDataset(){
        Input_User input = new Input_User();
        GridGeneration test = new GridGeneration();
        test.initiation();
        double[][][] locations = input.locations;
        test.PointDefinition(locations);
        test.Mesh_Point_Definition("Structured");
        test.Total_Grid();
        test.Staggered_Grid();
        Coordinate[][] Stagger_U = test.getStaggerU();
        Coordinate[][] Stagger_V = test.getStaggerV();

        XYSeries data = new XYSeries("Point problem");
        for (int i=0; i<4; i++){
            data.add(locations[0][0][i], locations[0][1][i]);
        }

        XYSeries StaggerU = new XYSeries("U Stagger Grid");
        for (int i=0; i<input.grid_y+3; i++){
            for (int j=0; j<input.grid_x+2; j++){
                StaggerU.add(Stagger_U[i][j].getX(), Stagger_U[i][j].getY());
            }
        }

        XYSeries StaggerV = new XYSeries("V Stagger Grid");
        for (int i=0; i<input.grid_y+2; i++){
            for (int j=0; j<input.grid_x+3; j++){
                StaggerV.add(Stagger_V[i][j].getX(), Stagger_V[i][j].getY());
            }
        }

        XYSeriesCollection total = new XYSeriesCollection();
        total.addSeries(data);
        total.addSeries(StaggerU);
        total.addSeries(StaggerV);

        return total;
    }
}
