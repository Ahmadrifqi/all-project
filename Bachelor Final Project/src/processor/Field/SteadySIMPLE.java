package processor.Field;

import processor.Boundary.Boundary_Condition;
import processor.GridGeneration.GridGeneration;
import processor.Input_User;
import processor.MatrixOperator.TDMASolver;
import processor.objectClasses.CellArea;
import processor.objectClasses.Coordinate;

public class SteadySIMPLE {
    Input_User input = new Input_User();
    private double tol_ref = 0.0001;
    private double[][] u_Correction; //u_Correction @cell-faced non-staggered grid or @cell-centered staggered grid
    private double[][] u_Corrected; //u_Corrected @cell-faced non-staggered grid or @cell-centered staggered grid
    private double[][] uFace;   //@cell-faced non-staggered grid or cell-centered staggered grid

    //define v-component
    private double[][] v_Correction; //v_Correction @cell-faced non-staggered grid or @cell-centered staggered grid
    private double[][] v_Corrected; //v_Corrected @cell-faced non-staggered grid or @cell-centered staggered grid
    private double[][] vFace;   //@cell-faced non-staggered grid or cell-centered staggered grid

    //define pressure [@cell-centered]:
    private double[][] p_Correction;  //@cell-centered non-staggered grid
    private double[][] p_Corrected;  //@cell-centered non-staggered grid

    private String[] Boundary;

    private double dxe,dxw, dyn,dys,dt;
    private double reynolds;
    private double alpha_p,alpha_u,alpha_v;
    private int length, width;
    private Coordinate[][] StaggerV, StaggerU;
    private CellArea[][] Area_StaggerU, Area_StaggerV;

    public SteadySIMPLE(TestCaseField Case, GridGeneration Grid){
        length = input.grid_x;
        width = input.grid_y;

        u_Correction = new double[input.grid_y+2][input.grid_x+1];
        u_Corrected = new double[input.grid_y+2][input.grid_x+1];
        uFace = Case.getU_Face();

        v_Correction = new double[input.grid_y+1][input.grid_x+2];
        v_Corrected = new double[input.grid_y+1][input.grid_x+2];
        vFace = Case.getV_Face();

        p_Correction = new double[input.grid_y+2][input.grid_x+2];
        p_Corrected = new double[input.grid_y+2][input.grid_x+2];

        Boundary = Case.getBoundary();
        StaggerU = Grid.getStaggerU();
        StaggerV = Grid.getStaggerV();
        Area_StaggerU = Grid.getArea_staggerU();
        Area_StaggerV = Grid.getArea_staggerV();
    }

    public void Constant_Initiation(){
        alpha_p = 0.01;
        alpha_u = 0.01;
        alpha_v = 0.01;
        reynolds = 100;
    }

    public void Umomentum(double pCCOld[][], double vface[][], double uface[][]) {//bergerak ke arah x
        double[][] temp = new double[width + 2][length + 1];
        double[][] matriks = new double[length - 1][length - 1];
        double[] vektor = new double[length - 1];
        double error = 1, hasil[], SourceWallP, SourceWallU;

        for (int i = 0; i < width + 2; i++) {
            for (int j = 0; j < length + 1; j++) {
                temp[i][j] = uface[i][j];
            }
        }

        while (error > tol_ref) {
            error = 0;
            for (int i = width; i > 0; i--) {
                for (int j = 1; j < length; j++) {
                    //Perhitungan dx dan dy
                    dxe = (StaggerU[i][j + 2].getX() + StaggerU[i + 1][j + 2].getX() - StaggerU[i + 1][j].getX() - StaggerU[i][j].getX()) / 4.0;
                    dxw = (StaggerU[i + 1][j - 1].getX() + StaggerU[i][j - 1].getX() - StaggerU[i][j + 1].getX() - StaggerU[i + 1][j + 1].getX()) / 4.0;
                    dyn = (StaggerU[i + 2][j].getY() + StaggerU[i + 2][j + 1].getY() - StaggerU[i][j].getY() - StaggerU[i][j + 1].getY()) / 4.0;
                    dys = (StaggerU[i - 1][j].getY() + StaggerU[i - 1][j + 1].getY() - StaggerU[i + 1][j].getY() - StaggerU[i + 1][j + 1].getY()) / 4.0;

                    int J = j - 1;
                    //X convection term
                    matriks[J][J] = matriks[J][J] + 0.25*(temp[i][j] + temp[i][j+1])*Area_StaggerU[i][j].getDe() + 0.25*(temp[i][j-1] + temp[i][j])*Area_StaggerU[i][j].getDw();

                    //Y convection Term
                    matriks[J][J] = matriks[J][J] + 0.25*(vface[i][J] + vface[i][J + 1]) * Area_StaggerU[i][j].getDn() + 0.25*(vface[i-1][J] + vface[i-1][J+1])*Area_StaggerU[i][j].getDs();
                    vektor[J] = vektor[J] - temp[i+1][j]*(0.25*Area_StaggerU[i][j].getDs()*(vface[i-1][J] + vface[i-1][J+1]));
                    vektor[J] = vektor[J] - temp[i-1][j]*(0.25*Area_StaggerU[i][j].getDn()*(vface[i][J] + vface[i-1][J+1]));

                    //diffusive term
                    matriks[J][J] = matriks[J][J] + Area_StaggerU[i][j].getDw()/(dxw*reynolds) + Area_StaggerU[i][j].getDe()/(dxe*reynolds) + Area_StaggerU[i][j].getDn()/(dyn*reynolds) + Area_StaggerU[i][j].getDs()/(dys*reynolds);
                    vektor[J] = vektor[J] - temp[i+1][j]*Area_StaggerU[i][j].getDn()/(reynolds*dyn) + temp[i-1][j]*Area_StaggerU[i][j].getDs()/(reynolds*dys);

                    //Source Term
                    vektor[J] = vektor[J] - Area_StaggerU[i][j].getDe()*(pCCOld[i][j+1] - pCCOld[i][j]);

                    if (J < length - 2) {
                        matriks[J][J+1] = (0.25*(temp[i][j] + temp[i][j+1])*Area_StaggerU[i][j].getDe() + Area_StaggerU[i][j].getDe()/(reynolds*dxe));
                    }
                    if (J > 0) {
                        matriks[J][J-1] = (0.25*(temp[i][j] + temp[i][j-1])*Area_StaggerU[i][j].getDw() + Area_StaggerU[i][j].getDw()/(reynolds*dxw));
                    }

                    //Boundary Treatment
                    SourceWallP = 0;
                    SourceWallU = 0;

                    if (i == 1) {
                        SourceWallP = Area_StaggerU[i][j].getDn()/(Math.abs(dys)/2.0);

                        if (Boundary[2] == "Stationary Wall"){
                            SourceWallU = 0;
                        }

                        if (Boundary[2] == "Moving Wall"){
                            double Uwall = (uface[i][j] + uface[i-1][j])/2.0;
                            SourceWallU = Uwall*Area_StaggerU[i][j].getDn()/(Math.abs(dys)/2.0);
                        }
                    }
                    if (i == length - 1) {
                        SourceWallP = Area_StaggerU[i][j].getDs()/(Math.abs(dyn)/2.0);

                        if (Boundary[0] == "Stationary Wall"){
                            SourceWallU = 0;
                        }

                        if (Boundary[0] == "Moving Wall"){
                            double Uwall = (uface[i][j] + uface[i+1][j])/2.0;
                            SourceWallU = Uwall*Area_StaggerU[i][j].getDn()/(Math.abs(dys)/2.0);
                        }
                    }

                    //Input efek Boundary Wall
                    matriks[J][J] -= SourceWallP*Area_StaggerU[i][j].getArea();
                    vektor[J] += SourceWallU*Area_StaggerU[i][j].getArea();
                }
                TDMASolver tdmaSolver = new TDMASolver(length - 1);
                tdmaSolver.DefinisiMatriksBaru(length - 1, vektor, matriks);
                tdmaSolver.penyelesaian(length - 1);
                hasil = tdmaSolver.getAkhir();
                for (int k = 0; k < length - 1; k++) {
                    uFace[i][k + 1] = hasil[k];
                }
                for (int j=0; j<length-1; j++){
                    for (int k=0; k<length-1; k++){
                        matriks[j][k] = 0;
                    }
                    vektor[j] = 0;
                }
            }
            for (int i = 1; i < length; i++) {
                for (int j = 1; j < length; j++) {
                    double error_tmp;
                    error_tmp = Math.abs(uFace[i][j] - temp[i][j]);
                    error = Math.max(error, error_tmp);
                }
            }
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < length + 1; j++) {
                    temp[i][j] = uFace[i][j];
                }
            }
        }
    }

    public void Vmomentum(double pCCOld[][], double vface[][], double uface[][]) { //v bergarak positif ke arah y
        double[][] temp = new double[width + 1][length + 2];
        double[][] matriks = new double[width - 1][width - 1];
        double[] vektor = new double[width - 1];
        double error = 1, hasil[], SourceWallP, SourceWallV;

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width + 1; j++) {
                temp[j][i] = vface[j][i];
            }
        }

        while (error > tol_ref) {
            error = 0;
            for (int i = 1; i < length - 1; i++) {
                for (int j = 1; j < width; j++) {
                    //Perhitungan dx dan dy
                    dxe = (StaggerV[i][j + 2].getX() + StaggerV[i + 1][j + 2].getX() - StaggerV[i + 1][j].getX() - StaggerV[i][j].getX()) / 4.0;
                    dxw = (StaggerV[i][j + 1].getX() + StaggerV[i + 1][j + 1].getX() - StaggerV[i + 1][j - 1].getX() - StaggerV[i][j - 1].getX()) / 4.0;
                    dyn = (StaggerV[i + 2][j].getY() + StaggerV[i + 2][j + 1].getY() - StaggerV[i][j].getY() - StaggerV[i][j + 1].getY()) / 4.0;
                    dys = (StaggerV[i + 1][j].getY() + StaggerV[i + 1][j + 1].getY() - StaggerV[i - 1][j].getY() - StaggerV[i - 1][j + 1].getY()) / 4.0;

                    int J = j - 1;
                    //X Convective Term
                    matriks[J][J] = matriks[J][J] + 0.25 * (uface[j][i] + uface[j + 1][i]) * Area_StaggerV[i][j].getDe();
                    matriks[J][J] = matriks[J][J] - 0.25 * (uface[j][i - 1] + uface[j + 1][i - 1]) * Area_StaggerV[i][j].getDw();
                    vektor[J] = vektor[J] - temp[j][i + 1] * 0.25 * (uface[j][i] + uface[j + 1][i]) * Area_StaggerV[i][j].getDe();
                    vektor[J] = vektor[J] + temp[j][i - 1] * 0.25 * (uface[j][i - 1] + uface[j + 1][i - 1]) * Area_StaggerV[i][j].getDw();

                    //Y Convective term
                    matriks[J][J] = matriks[J][J] + 0.25 * (temp[j][i] + temp[j + 1][i]) * Area_StaggerV[i][j].getDn();
                    matriks[J][J] = matriks[J][J] - 0.25 * (temp[j][i] + temp[j - 1][i]) * Area_StaggerV[i][j].getDs();

                    //diffusive term
                    matriks[J][J] = matriks[J][J] + Area_StaggerV[i][j].getDn() / (reynolds * dyn) + Area_StaggerV[i][j].getDs() / (reynolds * dys) + Area_StaggerV[i][j].getDe() / (reynolds * dxe) + Area_StaggerV[i][j].getDw() / (reynolds * dxw);
                    vektor[J] = vektor[J] + temp[j][i + 1] * Area_StaggerV[i][j].getDn() / (reynolds * dyn) + temp[j][i - 1] * Area_StaggerV[i][j].getDs() / (reynolds * dys);

                    //Source Term
                    vektor[J] = vektor[J] - (pCCOld[j + 1][i] - pCCOld[j][i]) * Area_StaggerV[i][j].getDn();

                    if (J < width - 2) {
                        matriks[J][J + 1] = 0.25 * (temp[j][i] + temp[j + 1][i] - 1.0 / (reynolds * dyn)) * Area_StaggerV[i][j].getDn();
                    }

                    if (J > 0) {
                        matriks[J][J - 1] = -0.25 * (temp[j][i] + temp[j - 1][i] - 1.0 / (reynolds * dys)) * Area_StaggerV[i][j].getDs();
                    }

                    //Boundary Treatment
                    SourceWallP = 0;
                    SourceWallV = 0;

                    if (j == 1) {
                        SourceWallP = Area_StaggerU[i][j].getDn() / (Math.abs(dys) / 2.0);

                        if (Boundary[3] == "Stationary Wall") {
                            SourceWallV = 0;
                        }

                        if (Boundary[3] == "Moving Wall") {
                            double Vwall = (vface[i][j] + vface[i - 1][j]) / 2.0;
                            SourceWallV = Vwall * Area_StaggerV[i][j].getDn() / (Math.abs(dys) / 2.0);
                        }
                    }
                    if (i == length - 1) {
                        SourceWallP = Area_StaggerU[i][j].getDs() / (Math.abs(dyn) / 2.0);

                        if (Boundary[0] == "Stationary Wall") {
                            SourceWallV = 0;
                        }

                        if (Boundary[0] == "Moving Wall") {
                            double Vwall = (vface[i][j] + vface[i + 1][j]) / 2.0;
                            SourceWallV = Vwall * Area_StaggerU[i][j].getDn() / (Math.abs(dys) / 2.0);
                        }
                    }
                    //Input efek Boundary Wall
                    vektor[J] += SourceWallV;
                    matriks[J][J] -= SourceWallP;
                }
                TDMASolver tdmaSolver = new TDMASolver(width - 1);
                tdmaSolver.DefinisiMatriksBaru(width - 1, vektor, matriks);
                tdmaSolver.penyelesaian(width - 1);
                hasil = tdmaSolver.getAkhir();
                for (int k = 0; k < width - 1; k++) {
                    vFace[k + 1][i] = hasil[k];
                }
                for (int j=0; j<width-1; j++){
                    for (int k=0; k<width-1; k++){
                        matriks[j][k] = 0;
                    }
                    vektor[j] = 0;
                }
            }
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < width; j++) {
                    double temp_error;
                    temp_error = Math.abs(temp[i][j] - vFace[i][j]);
                    error = Math.max(error, temp_error);
                }
            }
            for (int i = 0; i < length; i++) {
                for (int j = 0; j < width + 1; j++) {
                    temp[j][i] = vFace[j][i];
                }
            }
        }
    }

    public void setP_Correction(double vFace[][], double uFace[][]) {
        PoissonPressure poissonPressure = new PoissonPressure();
        poissonPressure.SwipeX(uFace, vFace, p_Correction);
        //poissonPressure.SwipeY(uFace, vFace, p_Correction);
        p_Correction = poissonPressure.getHasilAkhir();
    }

    public void setU_Correction(double p_Correction[][]) {
        for (int i = 1; i < width + 1; i++) {
            for (int j = 1; j < length; j++) {
                dxw = (StaggerV[i][j + 1].getX() + StaggerV[i + 1][j + 1].getX() - StaggerV[i + 1][j - 1].getX() - StaggerV[i][j - 1].getX()) / 4.0;
                u_Correction[i][j] = -(p_Correction[i][j] - p_Correction[i][j - 1]) / (dxw * dt);
            }
        }
    }

    public void setV_Correction(double p_Correction[][]) {
        for (int i = 1; i < length - 1; i++) {
            for (int j = 1; j < width; j++) {
                dys = (StaggerV[i + 1][j].getY() + StaggerV[i + 1][j + 1].getY() - StaggerV[i - 1][j].getY() - StaggerV[i - 1][j + 1].getY()) / 4.0;
                v_Correction[i][j] = -(p_Correction[i][j] - p_Correction[i - 1][j]) / (dys * dt);
            }
        }
    }

    public void setP_Corrected(double[][] pCCOld, double[][] p_Correction) {
        for (int i = 1; i < width + 1; i++) {
            for (int j = 1; j < length + 1; j++) {
                p_Corrected[i][j] = pCCOld[i][j] + alpha_p * p_Correction[i][j];
                Boundary_Condition Bc = new Boundary_Condition(1);
            }
        }
    }

    public void setU_Corrected(double[][] uface, double[][] u_correction) {
        for (int i = 0; i < width + 2; i++) {
            for (int j = 0; j < length + 1; j++) {
                u_Corrected[i][j] = uface[i][j] + u_correction[i][j];
            }
        }
    }

    public void setV_Corrected(double[][] vFace, double[][] v_Correction) {
        for (int i = 1; i < length - 1; i++) {
            for (int j = 1; j < width; j++) {
                v_Corrected[j][i] = vFace[j][i] + v_Correction[j][i];
            }
        }
    }

    public double[][] getP_Corrected() {
        return p_Corrected;
    }

    public double[][] getU_Corrected() {
        return u_Corrected;
    }

    public double[][] getV_Corrected() {
        return v_Corrected;
    }

    public double[][] getU_Correction() {
        return u_Correction;
    }

    public double[][] getV_Correction() {
        return v_Correction;
    }

    public double[][] getuFace() {
        return uFace;
    }

    public double[][] getvFace() {
        return vFace;
    }

    public double[][] getP_Correction() {
        return p_Correction;
    }

}
