package processor.Field;

import processor.Input_User;
import processor.objectClasses.Coordinate;
import processor.objectClasses.Velocity;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class TestCaseField {
    Input_User input = new Input_User();
    private double velocity_u[][], u_Face[][];
    private double velocity_v[][], v_Face[][];
    private double p_guess[][];
    private int panjang, lebar;
    private Velocity[][] velocity;
    private String Boundary[];
    private int locx = 0, locy = 0;

    public TestCaseField(int length, int width){
        panjang = length;
        lebar = width;
        velocity_u = new double[lebar+2][panjang+2];
        u_Face = new double[lebar+2][panjang+1];
        velocity_v = new double[lebar+2][panjang+2];
        v_Face = new double[lebar+1][panjang+2];
        velocity = new Velocity[width+2][length+2];
        p_guess =new double[lebar+2][panjang+2];
        Boundary = new String[4];
        //Upper is 0 and moves Clock wise for definition purpose
    }

    public void initiation(){
        for (int i=0; i<lebar+2; i++){
            for (int j=0; j<panjang+2; j++){
                velocity[i][j] = new Velocity();
            }
        }
    }

    public void Lid_driven(){
        for (int i=0; i<lebar+2; i++){
            for (int j=0; j<panjang+2; j++){
                velocity_u[i][j] = 0.01;
                if (i == lebar +1){
                    velocity_u[i][j] = 1.50;
                }
            }
        }
        //setting for Boundary Condition
        //Upper Boundary is Constant Velocity
        //Right and Left Boundary is Wall
        //Lower Boundary is Wall
        Boundary[0] = "Moving Wall";
        Boundary[1] = "Stationary Wall";
        Boundary[2] = "Stationary Wall";
        Boundary[3] = "Stationary Wall";
    }

    public void CirculatingIrrotational(Coordinate ScalarGrid[][], double Omega, Coordinate Centre){
        double x, y;
        for (int i=0; i<lebar+2; i++){
            for (int j=0; j<panjang+2; j++){
                y = Centre.getY() - ScalarGrid[i][j].getY();
                velocity_u[i][j] = Omega*y;
            }
        }

        for (int i=0; i<lebar+2; i++){
            for (int j=0; j<panjang+2; j++){
                x = Centre.getX() - ScalarGrid[i][j].getX();
                velocity_v[i][j] = -Omega*x;
            }
        }
    }

    public void VariedVelocityMid(Coordinate ScalarGrid[][], Coordinate center){
        double maxVelocity = 2.0;
        double y;
        for (int i=0; i<lebar+2; i++){
            y = ScalarGrid[i][0].getY();
            if (y < center.getY()) {
                velocity_u[i][0] = (y - ScalarGrid[0][0].getY())/(center.getY() - ScalarGrid[0][0].getY())* maxVelocity;
            }
            else {
                velocity_u[i][0] = (y - ScalarGrid[lebar+1][0].getY())/(center.getY() - ScalarGrid[lebar+1][0].getY())*maxVelocity;
            }
        }

        for (int i=0; i<lebar+2; i++){
            for (int j=0; j<panjang+2; j++){
                velocity_u[i][j] = velocity_u[i][0];
                velocity_v[i][j] = 0.0;
            }
        }
    }

    public void SourceVelocity(double Source, Coordinate ScalarGrid[][]){
        double k1, k2, grid1, grid2;
        double koorx = 0, koory = 0;
        double x, y, r;
        boolean Condition = false;
        for (int i = 1; i < input.grid_y + 1; i++) {
            for (int j = 1; j < input.grid_x + 1; j++) {
                k1 = input.centre.getX() - ScalarGrid[i][j].getX();
                k2 = input.centre.getY() - ScalarGrid[i][j].getY();

                grid1 = (ScalarGrid[i][j + 1].getX() - ScalarGrid[i][j].getX()) / 2.0;
                grid2 = (ScalarGrid[i + 1][j].getY() - ScalarGrid[i][j].getY()) / 2.0;

                if (grid1 > k1 && grid2 > k2) {
                    koorx = ScalarGrid[i][j].getX();
                    koory = ScalarGrid[i][j].getY();
                    Condition = true;
                    break;
                }
            }
            if (Condition) {
                break;
            }
        }

        for (int i = 0; i < input.grid_y + 2; i++) {
            for (int j = 0; j < input.grid_x + 2; j++) {
                x = ScalarGrid[i][j].getX() - koorx;
                y = ScalarGrid[i][j].getY() - koory;

                r = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

                velocity_u[i][j] = Source*x/Math.pow(r, 2);
                velocity_v[i][j] = Source*y/Math.pow(r, 2);
            }
        }
    }

    public void SinkVelocity(double Source, Coordinate ScalarGrid[][]){
        double k1, k2, grid1, grid2;
        double koorx = 0, koory = 0;
        double x, y, r;
        boolean Condition = false;
        for (int i = 1; i < input.grid_y + 1; i++) {
            for (int j = 1; j < input.grid_x + 1; j++) {
                k1 = input.centre.getX() - ScalarGrid[i][j].getX();
                k2 = input.centre.getY() - ScalarGrid[i][j].getY();

                grid1 = (ScalarGrid[i][j + 1].getX() - ScalarGrid[i][j].getX()) / 2.0;
                grid2 = (ScalarGrid[i + 1][j].getY() - ScalarGrid[i][j].getY()) / 2.0;

                if (grid1 > k1 && grid2 > k2) {
                    koorx = ScalarGrid[i][j].getX();
                    koory = ScalarGrid[i][j].getY();
                    Condition = true;
                    break;
                }
            }
            if (Condition) {
                break;
            }
        }

        for (int i = 0; i < input.grid_y + 2; i++) {
            for (int j = 0; j < input.grid_x + 2; j++) {
                x = ScalarGrid[i][j].getX() - koorx;
                y = ScalarGrid[i][j].getY() - koory;

                r = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

                velocity_u[i][j] = -Source*x/Math.pow(r, 2);
                velocity_v[i][j] = -Source*y/Math.pow(r, 2);
            }
        }
    }

    public void VariedVelocity(Coordinate ScalarGrid[][], Coordinate center){
    	double maxVelocity = 2.0;
    	double y;
    	for (int i=0; i<lebar+2; i++){
    		y = Math.abs(ScalarGrid[i][0].getY() - center.getY())/center.getY();
    		velocity_u[i][0] = y*maxVelocity;
    	}

    	for (int i=0; i<lebar+2; i++){
    	    for (int j=0; j<panjang+2; j++){
    	        velocity_u[i][j] = velocity_u[i][0];
    	        velocity_v[i][j] = 0.0;
            }
        }
    }

    public void ConstantXVelocity(){
        for (int i=0; i<lebar+2; i++){
            for (int j=0; j<panjang+2; j++){
                velocity_u[i][j] = 1.0;
                velocity_v[i][j] = 0.0;
            }
        }

        Boundary[0] = "Opening";
        Boundary[1] = "Opening";
        Boundary[2] = "Opening";
        Boundary[3] = "Inlet";
    }

    public void ConstantYVelocity(){
        for (int i=0; i<lebar+2; i++){
            for (int j=0; j<panjang+2; j++){
                velocity_u[i][j] = 0.0;
                velocity_v[i][j] = 1.0;
            }
        }

        Boundary[0] = "Opening";
        Boundary[1] = "Opening";
        Boundary[2] = "Inlet";
        Boundary[3] = "Opening";
    }

    public void ConstantObliqueVelocity(){
        for (int i=0; i<lebar+2; i++){
            for (int j=0; j<panjang+2; j++){
                velocity_u[i][j] = 2.0;
                velocity_v[i][j] = 2.0;
            }
        }
    }

    public void setP_guess(){
        for (int i =0; i<lebar+2; i++){
            for (int j=0; j< panjang+2; j++){
                p_guess[i][j] = Math.pow(10,5);
            }
        }
    }

    public void setVcell() {
        for (int i = 0; i < lebar + 2; i++) {
            for (int j = 0; j < panjang + 2; j++) {
                velocity[i][j].x = velocity_u[i][j];
                velocity[i][j].y = velocity_v[i][j];
            }
        }
    }

    public void setVface(){
        //set u face
        for (int i=0; i<lebar+2; i++){
            for (int j=0; j<panjang+1; j++){
                u_Face[i][j] = (velocity_u[i][j] + velocity_u[i][j + 1]) / 2;
                velocity[i][j].x = (velocity_u[i][j] + velocity_u[i][j + 1]) / 2;
                if (velocity_u[i][j] < 0 && Double.isNaN(velocity_u[i][j+1])){
                        u_Face[i][j] = (-50000 + velocity_u[i][j])/2;
                        velocity[i][j].x = (-50000 + velocity_u[i][j])/2;
                }
                if (velocity_u[i][j+1] > 0 && Double.isNaN(velocity_u[i][j])){
                        u_Face[i][j] = (50000 + velocity_u[i][j+1])/2;
                        velocity[i][j].x = (50000 + velocity_u[i][j+1])/2;
                }
            }
        }

        //set v face
        for (int i=0; i<lebar+1; i++){
            for (int j=0; j<panjang+2; j++) {
                v_Face[i][j] = (velocity_v[i][j] + velocity_v[i + 1][j]) / 2;
                velocity[i][j].y = (velocity_v[i][j] + velocity_v[i + 1][j]) / 2;
                    if (velocity_v[i][j] < 0 && Double.isNaN(velocity_v[i+1][j])){
                        v_Face[i][j] = (-50000 + velocity_v[i][j])/2;
                        velocity[i][j].y = (-50000 + velocity_v[i][j])/2;
                    }
                    if (velocity_v[i+1][j] > 0 && Double.isNaN(velocity_v[i][j])){
                        v_Face[i][j] = (50000 + velocity_v[i+1][j])/2;
                        velocity[i][j].y = (50000 + velocity_v[i+1][j])/2;
                    }

            }
        }
    }

    public double[][] getU_Face() {
        return u_Face;
    }

    public double[][] getV_Face() {
        return v_Face;
    }

    public double[][] getP_guess() {
        return p_guess;
    }

    public String[] getBoundary() {
        return Boundary;
    }

    public Velocity[][] getVelocity() {
        return velocity;
    }
}
