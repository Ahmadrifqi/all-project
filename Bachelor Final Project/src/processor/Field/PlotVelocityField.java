package processor.Field;

import org.jfree.chart.ChartPanel;
import org.jfree.data.xy.VectorSeries;
import org.jfree.data.xy.VectorSeriesCollection;
import org.jfree.chart.renderer.xy.VectorRenderer;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ChartFrame;
import processor.objectClasses.Coordinate;
import processor.objectClasses.Velocity;

import java.awt.*;

public class PlotVelocityField {

    public PlotVelocityField(Velocity[][] velocities, Coordinate[][] coordinates){
        VectorRenderer r = new VectorRenderer();
        //r.setBasePaint(Color.white);
        r.setSeriesPaint(0, Color.blue);

        XYPlot xyPlot = new XYPlot(Dataset(velocities, coordinates), new NumberAxis("X"), new NumberAxis("Y"), r);

        // Create a Chart
        JFreeChart theChart;

        theChart = new JFreeChart(xyPlot);
        theChart.setTitle("Velocity Field");

        // create and display a frame...
        ChartFrame frame = new ChartFrame("Plot", theChart);
        frame.pack();
        frame.setVisible(true);
    }

    private static VectorSeriesCollection Dataset(Velocity[][] velocities, Coordinate[][] coordinates){
        double Umax = 0, Vmax = 0;
        for (int i = 0; i < velocities.length-1; i++) {
            for (int j = 0; j < velocities[0].length-1; j++) {
                Umax = Math.max(Math.abs(velocities[i][j].getX()), Umax);
                Vmax = Math.max(Math.abs(velocities[i][j].getY()), Vmax);
            }
        }

        // We create a vector series collection
        VectorSeriesCollection dataSet = new VectorSeriesCollection();

        VectorSeries vectorSeries = new VectorSeries("Velocity Vector");
        for (int i = 0; i < velocities.length; i++) {
            for (int j = 0; j < velocities[0].length; j++) {
                velocities[i][j].setX(velocities[i][j].getX() / (10*Umax));
                if (Vmax != 0) {
                    velocities[i][j].setY(velocities[i][j].getY() / (10*Vmax));
                }
                vectorSeries.add(coordinates[i][j].getX(), coordinates[i][j].getY(),
                        velocities[i][j].getX(), velocities[i][j].getY());
            }
        }
        dataSet.addSeries(vectorSeries);

        return dataSet;
    }
}
