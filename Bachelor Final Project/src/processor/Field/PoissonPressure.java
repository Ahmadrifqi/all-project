package processor.Field;

import processor.GridGeneration.GridGeneration;
import processor.Input_User;
import processor.MatrixOperator.*;
import processor.objectClasses.Coordinate;

public class PoissonPressure {
    private double matriks[][];
    private double vektor[];
    private double hasilAkhir[][];
    private int length, width;
    private Coordinate[][] koordinat;

    public PoissonPressure(){
        Input_User input = new Input_User();
        GridGeneration grid = new GridGeneration();
        grid.initiation();
        grid.PointDefinition(input.locations);
        grid.Mesh_Point_Definition("Structured");
        grid.Total_Grid();
        grid.Scalar_grid();
        length = input.grid_x;
        width = input.grid_y;
        matriks = new double[length][length];
        vektor = new double[length];
        hasilAkhir = new double[input.grid_y+2][input.grid_x+2];
        koordinat = grid.getCoordinate_scalar();
    }

    public void SwipeX(double velocity_u[][], double velocity_v[][], double p_Correction [][]){
        double vector[], temp[][] = new double[width+2][length+2];
        double dx, dy, error;
        int k, m;
        error = 1;
        k = 0;
        m = 0;
        for (int i=0; i<width+2; i++){
            for (int j=0; j<length+2; j++){
                hasilAkhir[i][j] = p_Correction[i][j];
                temp[i][j] = p_Correction[i][j];
            }
        }

        while (error > 0.0001){
            error = 0;
            for (int i = 1; i < width + 1; i++) {
                for (int j = 1; j < length + 1; j++) {
                    dx = koordinat[i][j].getX() - koordinat[i][j+1].getX();
                    dy = koordinat[i][j].getY() - koordinat[i+1][j].getY();
                    matriks[k][k] = -2*(1+dx/dy);
                    if (k>0){
                        matriks[k][k-1] = 1;
                    }
                    if(k<length-1){
                        matriks[k][k+1] = 1;
                    }
                    vektor[k] += (velocity_u[i][j] - velocity_u[i][j-1])*dx/dy;
                    vektor[k] += (velocity_v[i][j] - velocity_v[i-1][j])*dx/dy;
                    vektor[k] += (p_Correction[i+1][j] + p_Correction[i-1][j])*dx/dy;
                    if (j==1){
                        vektor[k] = vektor[k] + p_Correction[i][j-1];
                    }
                    if (j==length){
                        vektor[k] = vektor[k] + p_Correction[i][j+1];
                    }
                    k++;
                }
                GaussSeidel gaussSeidel = new GaussSeidel(length);
                gaussSeidel.inisiasi();
                gaussSeidel.utama(matriks, vektor);
                vector = gaussSeidel.getAkhir();
                for (int l = 1; l < length + 1; l++) {
                    hasilAkhir[i][l] = vector[m];
                    vektor[m] = 0;
                    m++;
                }
                k = 0;
                m = 0;
            }
            for (int i=0; i<length; i++){
                for (int j=0; j<width; j++){
                    error = error + (temp[i][j] - hasilAkhir[i][j]);
                }
            }
            for (int i=0; i<width+2; i++){
                for (int j=0; j<length+2; j++) {
                    temp[i][j] = hasilAkhir[i][j];
                }
            }
        }
    }

    public void SwipeY(double velocity_u[][], double velocity_v[][], double p_Correction [][]){
        double vector[], temp[][];
        double dx, dy, error;
        int k, m;
        error = 1;
        k = 0;
        m = 0;
        hasilAkhir = p_Correction;
        temp = p_Correction;

        while (error > 0.0001){
            error = 0;
            for (int i = 1; i <length+1; i++){
                for (int j = 1; j <width+1; j++){
                    dx = koordinat[i][j].getX() - koordinat[i][j+1].getX();
                    dy = koordinat[i][j].getY() - koordinat[i+1][j].getY();
                    matriks[k][k] = -2*(1+dy/dx);
                    if(k>0){
                        matriks[k][k-1] = 1;
                    }
                    if(k<length-1){
                        matriks[k][k+1] = 1;
                    }
                    vektor[k] = vektor[k] + (velocity_u[j][i-1] - velocity_u[j][i]) + (velocity_v[j-1][i] - velocity_v[j][i]);
                    vektor[k] = vektor[k] + (p_Correction[j][i+1] + p_Correction[j][i-1])*dy/dx;
                    if (j==1){
                        vektor[k] = vektor[k] + p_Correction[j-1][i];
                    }
                    if (j==length){
                        vektor[k] = vektor[k] + p_Correction[j+1][i];
                    }
                    k++;
                }
                GaussSeidel gaussSeidel = new GaussSeidel(length);
                gaussSeidel.inisiasi();
                gaussSeidel.utama(matriks, vektor);
                vector = gaussSeidel.getAkhir();
                for (int l = 1; l <width+1; l++) {
                    hasilAkhir[i][l] = vector[m];
                    vektor[m] = 0;
                    m++;
                }
                k = 0;
                m = 0;
            }
            for (int i=1; i<length+1; i++){
                for (int j=1; j<width+1; j++){
                    error = error + (temp[i][j] - hasilAkhir[i][j]);
                }
            }
            temp = hasilAkhir;
        }
    }

    public double[][] getHasilAkhir() {
        return hasilAkhir;
    }
}