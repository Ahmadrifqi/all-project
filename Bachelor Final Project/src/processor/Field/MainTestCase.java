package processor.Field;

import processor.GridGeneration.GridGeneration;
import processor.Input_User;
import processor.objectClasses.Coordinate;
import processor.objectClasses.Velocity;

import java.io.IOException;
import java.io.PrintWriter;

public class MainTestCase {
    public static void main(String[] Args) throws Exception{
        Input_User input = new Input_User();
        Velocity[][] velocity1, velocity2, velocity3, velocity4;
        Velocity[][] velocity5, velocity6;
        velocity1 = new Velocity[input.grid_y+2][input.grid_x+2];
        velocity2 = new Velocity[input.grid_y+2][input.grid_x+2];
        velocity3 = new Velocity[input.grid_y+2][input.grid_x+2];
        velocity4 = new Velocity[input.grid_y+2][input.grid_x+2];
        velocity5 = new Velocity[input.grid_y+2][input.grid_x+2];
        velocity6 = new Velocity[input.grid_y+2][input.grid_x+2];
        for (int i=input.grid_y+1; i>-1; i--) {
            for (int j=0; j<input.grid_x+2; j++) {
                velocity1[i][j] = new Velocity();
                velocity2[i][j] = new Velocity();
                velocity3[i][j] = new Velocity();
                velocity4[i][j] = new Velocity();
                velocity5[i][j] = new Velocity();
                velocity6[i][j]  = new Velocity();
            }
        }
        GridGeneration grid = new GridGeneration();
        grid.initiation();
        grid.PointDefinition(input.locations);
        grid.Mesh_Point_Definition("Structured");
        grid.Total_Grid();
        grid.Scalar_grid();
        Coordinate[][] Scalar_Grid = grid.getCoordinate_scalar();

        TestCaseField Case = new TestCaseField(input.grid_x, input.grid_y);
        TestCaseField Case1 = new TestCaseField(input.grid_x, input.grid_y);
        TestCaseField Case2 = new TestCaseField(input.grid_x, input.grid_y);
        TestCaseField Case3 = new TestCaseField(input.grid_x, input.grid_y);
        TestCaseField Case4 = new TestCaseField(input.grid_x, input.grid_y);
        TestCaseField Case5 = new TestCaseField(input.grid_x, input.grid_y);
        
        Case.initiation();
        Case1.initiation();
        Case2.initiation();
        Case3.initiation();
        Case4.initiation();
        Case5.initiation();

        Case.CirculatingIrrotational(Scalar_Grid, 0.01, input.centre);
        Case1.VariedVelocity(Scalar_Grid, input.centre);
        Case2.ConstantXVelocity();
        Case3.ConstantYVelocity();
        Case4.VariedVelocityMid(Scalar_Grid, input.centre);
        Case5.SinkVelocity(500, Scalar_Grid);

        Case.setVcell();
        Case1.setVcell();
        Case2.setVcell();
        Case3.setVcell();
        Case4.setVcell();
        Case5.setVcell();

        velocity1 = Case.getVelocity();
        velocity2 = Case1.getVelocity();
        velocity3 = Case2.getVelocity();
        velocity4 = Case3.getVelocity();
        velocity5 = Case4.getVelocity();
        velocity6 = Case5.getVelocity();


        PrintWriter printWriter = new PrintWriter("Output Velocity.txt");
        printWriter.println("U untuk Circular rotational : ");
        for (int i=input.grid_y+1; i>-1; i--){
            for (int j=0; j<input.grid_x+2; j++){
                printWriter.print(velocity1[i][j].x + " ");
            }
            printWriter.println();
        }
        printWriter.println();

        printWriter.println("U untuk Varied Velocity (Max Up) : ");
        for (int i=input.grid_y+1; i>-1; i--){
            for (int j=0; j<input.grid_x+2; j++){
                printWriter.print(velocity2[i][j].x + " ");
            }
            printWriter.println();
        }
        printWriter.println();

        printWriter.println("U untuk Constant X vel : ");
        for (int i=input.grid_y+1; i>-1; i--){
            for (int j=0; j<input.grid_x+2; j++){
                printWriter.print(velocity3[i][j].x + " ");
            }
            printWriter.println();
        }
        printWriter.println();

        printWriter.println("U untuk Y Constant : ");
        for (int i=input.grid_y+1; i>-1; i--){
            for (int j=0; j<input.grid_x+2; j++){
                printWriter.print(velocity4[i][j].x + " ");
            }
            printWriter.println();
        }
        printWriter.println();

        printWriter.println("U untuk X Varied (MaxMid) : ");
        for (int i=input.grid_y+1; i>-1; i--){
            for (int j=0; j<input.grid_x+2; j++){
                printWriter.print(velocity5[i][j].x + " ");
            }
            printWriter.println();
        }
        printWriter.println();

        printWriter.println("U untuk Sink : ");
        for (int i=input.grid_y+1; i>-1; i--){
            for (int j=0; j<input.grid_x+2; j++){
                printWriter.print(velocity6[i][j].x + " ");
            }
            printWriter.println();
        }
        printWriter.println();

        printWriter.println("V untuk Circular rotational : ");
        for (int i=input.grid_y+1; i>-1; i--){
            for (int j=0; j<input.grid_x+2; j++){
                printWriter.print(velocity1[i][j].y + " ");
            }
            printWriter.println();
        }
        printWriter.println();

        printWriter.println("V untuk Varied Velocity : ");
        for (int i=input.grid_y+1; i>-1; i--){
            for (int j=0; j<input.grid_x+2; j++){
                printWriter.print(velocity2[i][j].y + " ");
            }
            printWriter.println();
        }
        printWriter.println();

        printWriter.println("V untuk Constant X vel : ");
        for (int i=input.grid_y+1; i>-1; i--){
            for (int j=0; j<input.grid_x+2; j++){
                printWriter.print(velocity3[i][j].y + " ");
            }
            printWriter.println();
        }
        printWriter.println();

        printWriter.println("V untuk Y Constant : ");
        for (int i=input.grid_y+1; i>-1; i--){
            for (int j=0; j<input.grid_x+2; j++){
                printWriter.print(velocity4[i][j].y + " ");
            }
            printWriter.println();
        }
        printWriter.println();

        printWriter.println("V untuk X Varied (MaxMid) : ");
        for (int i=input.grid_y+1; i>-1; i--){
            for (int j=0; j<input.grid_x+2; j++){
                printWriter.print(velocity5[i][j].y + " ");
            }
            printWriter.println();
        }
        printWriter.println();

        printWriter.println("V untuk Sink : ");
        for (int i=input.grid_y+1; i>-1; i--){
            for (int j=0; j<input.grid_x+2; j++){
                printWriter.print(velocity6[i][j].y + " ");
            }
            printWriter.println();
        }
        printWriter.println();

        printWriter.println("Koordinat (x): ");
        for (int i=input.grid_y+1; i>-1; i--){
            for (int j=0; j<input.grid_x+2; j++){
                printWriter.print(grid.getCoordinate_scalar()[i][j].getX() + " ");
            }
            printWriter.println();
        }
        printWriter.println();

        printWriter.println("Koordinat (y): ");
        for (int i=input.grid_y+1; i>-1; i--){
            for (int j=0; j<input.grid_x+2; j++){
                printWriter.print(grid.getCoordinate_scalar()[i][j].getY() + " ");
            }
            printWriter.println();
        }
        printWriter.println();
        printWriter.close();
    }
}
