package processor.Field;

import Utils.DecimalUtils;
import Utils.DeleteFiles;
import processor.GridGeneration.GridGeneration;
import processor.Input_User;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class MainField {

    public static void main(String[] args) throws IOException{
        Input_User input = new Input_User();
        GridGeneration Grid = new GridGeneration();
        DeleteFiles delete = new DeleteFiles();
        delete.Delete("U");
        delete.Delete("Pressure Correction");
        delete.Delete("output u face");
        delete.Delete("output v face");

        DecimalUtils round = new DecimalUtils();
        PrintWriter print = new PrintWriter(new FileWriter("output u face.txt", true));
        PrintWriter printv = new PrintWriter(new FileWriter("output v face.txt", true));
        PrintWriter printU = new PrintWriter(new FileWriter("U.txt", true));
        PrintWriter printWriter = new PrintWriter(new FileWriter("Pressure Correction.txt", true));
        double u_correction[][], u_temp[][], uFace[][], u_input[][];
        double v_correction[][], v_temp[][], vFace[][], v_input[][];
        double p_guess[][], p_correction[][], temp[][];
        double error_u, error_v, error_ref;
        double timestep, t_tot, t_ref;
        u_input = new double[input.grid_y][input.grid_x+1];
        error_ref = 0.0001;
        timestep = 0.001;
        t_tot = 0;
        t_ref = 0.002;

        TestCaseField testCase = new TestCaseField(input.grid_x, input.grid_y);
        testCase.Lid_driven();
        testCase.setVface();
        testCase.setP_guess();
        p_guess = testCase.getP_guess();
        uFace = testCase.getU_Face();
        vFace = testCase.getV_Face();
        Grid.initiation();
        Grid.PointDefinition(input.locations);
        Grid.Mesh_Point_Definition("Structured");
        Grid.Total_Grid();
        Grid.Scalar_grid();
        Grid.Staggered_Grid();
        Grid.getStaggerU();
        Grid.getArea_staggerU();
        Grid.getStaggerV();
        Grid.getArea_staggerV();

        SteadySIMPLE Simple = new SteadySIMPLE(testCase, Grid);
        Simple.Constant_Initiation();

        int number = 1;
        while(t_tot < t_ref) {
            error_u = 1;
            error_v = 1;
            printU.println("Time Step : " + String.valueOf(t_tot + timestep));
            printWriter.println("Time Step : " + String.valueOf(t_tot + timestep));
            while (error_u > error_ref || error_v > error_ref) {
                error_u = 0;
                error_v = 0;
                for (int i=0; i<input.grid_y; i++){
                    u_input[i] = Arrays.copyOf(uFace[i], uFace[i].length);
                }

                Simple.Umomentum(p_guess, vFace, uFace);
                uFace = Simple.getuFace();
                u_temp = uFace;

                Simple.Vmomentum(p_guess, vFace, uFace);
                vFace = Simple.getvFace();
                v_temp = vFace;

                printU.println("U* ke-"+String.valueOf(number)+ ": ");
                for (int i=input.grid_y+1; i>-1; i--){
                    for (int j=0; j<input.grid_x+1; j++){
                        printU.print(uFace[i][j] + " ");
                    }
                    printU.println();
                }
                printU.println();
                Simple.setP_Correction(vFace, uFace);
                p_correction = Simple.getP_Correction();

                Simple.setP_Corrected(p_guess, p_correction);
                p_guess = Simple.getP_Corrected();
                Simple.setU_Correction(p_correction);
                Simple.setV_Correction(p_correction);
                v_correction = Simple.getV_Correction();
                u_correction = Simple.getU_Correction();

                Simple.setU_Corrected(uFace, u_correction);
                Simple.setV_Corrected(vFace, v_correction);
                uFace = Simple.getU_Corrected();
                vFace = Simple.getV_Corrected();
                printU.println("Corrected U :");
                for (int i=input.grid_y+1; i>-1; i--){
                    for (int j=0; j<input.grid_x+1; j++){
                        printU.print(uFace[i][j] + " ");
                    }
                    printU.println();
                }
                printU.println();
                for (int i = 0; i < input.grid_y; i++) {
                    for (int j = 0; j < input.grid_x + 1; j++) {
                        error_u = error_u + Math.abs(u_temp[i][j] - uFace[i][j]);
                        error_v = error_v + Math.abs(v_temp[j][i] - vFace[j][i]);
                    }
                }
                System.out.println(error_u);
                System.out.println(error_v);
                printv.println();
                printWriter.close();
                printU.close();
            }
            System.out.println("waktu : " + t_tot);
            t_tot = round.round(t_tot + timestep, 3);

            System.out.println();
            printWriter.println("Pressure ke-"+String.valueOf(number)+ ": ");
            for (int i=input.grid_y+1; i>-1; i--){
                for (int j=0; j<input.grid_x+2; j++){
                    //printWriter.print(p_correction[i][j] + " ");
                }
                printWriter.println();
            }

            printWriter.println();
            number++;
            temp = Simple.getuFace();
            print.println("time : " + t_tot);
            print.println("u Face : ");
            for (int i=input.grid_y+1; i>-1; i--){
                for (int j=0; j<input.grid_x+1; j++) {
                    print.print(temp[i][j] + " ");
                }
                print.println();
            }
            print.println();

            printv.println("time : " + t_tot);
            printv.println("v Face: ");
            temp = Simple.getvFace();
            for (int i=input.grid_y; i>-1; i--){
                for (int j=0; j<input.grid_x; j++){
                    printv.print(round.round(temp[i][j],2) + " ");
                }
                printv.println();
            }
        }
        printv.close();
        print.close();
    }
}