//package processor;
//
//import processor.objectClasses.Area;
//import processor.objectClasses.Pressure;
//import processor.objectClasses.Velocity;
//
//import java.awt.*;
//import java.awt.geom.Point2D;
//import java.util.ArrayList;
//
//public class VelocityFieldOperator extends FieldOperator {
//  private ArrayList<Point> boundaryConditionIdx;
//
//  public VelocityFieldOperator(Area area){
//    super(area);
//    //membuat grid
//    velocityField = new Velocity[length][width];
//    pressureField = new Pressure[length][width];
//    boundaryConditionIdx= new ArrayList<>();
//    initiate();
//  }
//
//  //menginisialisasi setiap elemen grid kecepatan dan tekanan dengan nilai 0
//  @Override
//  public void initiate() {
//    for(int x = 0; x< length; x++){
//      for(int y = 0; y< width; y++){
//        velocityField[x][y] = new Velocity();
//        pressureField[x][y] = new Pressure();
//      }
//    }
//  }
//
//
//  //menambahkan data kondisi angin dan tekanan pada grid
//  public void addBoundaryCondition(Velocity velocity, Pressure pressure, Point2D.Double location){
//    Point idx = realLocationToGridIdx(location);
//    velocityField[idx.x][idx.y] = velocity;
//    pressureField[idx.x][idx.y] = pressure;
//    boundaryConditionIdx.add(idx);
//  }
//
//  public void solve(){
//      for(int x=0; x<length; x++){
//        for(int y=0; y<width;y++){
//          Point P1 = new Point(x,y);
//          for (Point idx:boundaryConditionIdx) {
//            velocityField[x][y].addVelocity(countVelocity(idx,P1));
//          }
//        }
//      }
//  }
//
//  //menghitung nilai kecepatan angin awal dari suatu titik dengan acuan satu source
//  public Velocity countVelocity(Point sourceIdx,Point destinationIdx){
//    int xRange = sourceIdx.x-destinationIdx.x;
//    int yRange = sourceIdx.y-destinationIdx.y;
//
//    Velocity sourceVelocity = velocityField[sourceIdx.x][sourceIdx.y];
//    float velocityX=0;
//    float velocityY=0;
//
//    if(sourceVelocity.x>0){
//      velocityX = sourceVelocity.x-xRange;
//    } else if(sourceVelocity.x<0){
//      velocityX = sourceVelocity.x+xRange;
//    }
//
//    if(sourceVelocity.y>0){
//      velocityY = sourceVelocity.y-yRange;
//    } else if(sourceVelocity.y<0){
//      velocityY = sourceVelocity.y+yRange;
//    }
//
//    return new Velocity(velocityX,velocityY);
//  }
//
//
//}
