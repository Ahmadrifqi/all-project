package processor.objectClasses;

public class Pressure extends CBRNObject {
  public Pressure(){
    super();
  }

  public Pressure(float value){
    super(value);
  }
}
