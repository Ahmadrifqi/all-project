package processor.objectClasses;

public class CBRNObject {
    protected float value;

    protected CBRNObject(){
        value = 0;
    }

    protected CBRNObject(float value){
        this.value = value;
    }

    public void setValue(float value){
        this.value = value;
    }

    public float getValue(){
        return value;
    }

}
