package processor.objectClasses;

public class Toxic extends CBRNObject {
  //atribut
  private String name;

  //default constructor
  public Toxic(){
    super();
    this.name = "";
  }

  //constructor dg parameter
  public Toxic(String name,int concentration){
    super(concentration);
    this.name = name;
  }

  //setter
  public void setName(String name){
    this.name = name;
  }

  //getter
  public String getName(){
    return name;
  }

  public float getConcentration(){
    return value;
  }

}
