package processor.objectClasses;

//kecepatan dalam m/s
public class Velocity{
  public double x;
  public double y;

  public Velocity(double x, double y){
    this.x = x;
    this.y = y;
  }

  public Velocity(){
    this.x = 0;
    this.y = 0;
  }

  public void setX(double x){
    this.x = x;
  }

  public void setY(double y){
    this.y = y;
  }

  public double getX(){
    return this.x;
  }

  public double getY(){
    return this.y;
  }

  public void addVelocity(Velocity velocity){
    x =(x+velocity.x)/2;
    y =(y+velocity.y)/2;
  }
}
