package processor.objectClasses;

import java.awt.geom.Point2D;

public class Area {
    public Point2D.Double origin;
    public double length;
    public double width;

    public Area(){
        origin = new Point2D.Double();
        length =0;
        width =0;
    }

    public Area(Point2D.Double origin, double length, double width){
        this.origin = origin;
        this.length = length;
        this.width = width;
    }
}
