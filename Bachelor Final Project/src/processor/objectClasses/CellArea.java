package processor.objectClasses;

public class CellArea {
    private double Area;
    private double dn, ds, de, dw;

    public CellArea(){
        this.Area = 0;
        this.dn = 0;
        this.ds = 0;
        this.de = 0;
        this.dw = 0;
    }

    public CellArea(double Area, double dn, double ds, double de, double dw){
        this.Area = Area;
        this.dn = dn;
        this.ds = ds;
        this.de = de;
        this.dw = dw;
    }

    public void setArea(double area) {
        this.Area = area;
    }

    public void setDe(double de) {
        this.de = de;
    }

    public void setDn(double dn) {
        this.dn = dn;
    }

    public void setDs(double ds) {
        this.ds = ds;
    }

    public void setDw(double dw) {
        this.dw = dw;
    }

    public double getArea() {
        return Area;
    }

    public double getDe() {
        return de;
    }

    public double getDw() {
        return dw;
    }

    public double getDn() {
        return dn;
    }

    public double getDs() {
        return ds;
    }
}
