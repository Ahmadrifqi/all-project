package processor.MatrixOperator;
public class GaussSeidel {
    private int dimensi;
    private double[] RHS, akhir;
    private double error;
    private double tol;
    private int i, N;

    public GaussSeidel(int dimension){
        dimensi = dimension;
        akhir = new double[dimensi];
        RHS = new double[dimensi];
        for (int j=0; j<dimensi; j++){
            akhir[j] = 0;
        }
        error = 1;
        tol = Math.pow(10, -4);
    }

    public void inisiasi(){
        i=0;
        N=1;
    }

//    public void SortMatrix(double oldMatrix[][], double oldVector[]){
//        double CombineMatrix[][] = new double[dimensi][dimensi+1];
//        double newMatrix[][] = new double[dimensi][dimensi];
//        double newVector[] = new double[dimensi];
//
//        for (int i=0; i<dimensi; i++){
//            for (int j=0; j<dimensi+1; j++){
//                if (j < dimensi){
//                    CombineMatrix[i][j] = oldMatrix[i][j];
//                }
//                else {
//                    CombineMatrix[i][j] = oldVector[i];
//                }
//            }
//        }
//
//        for (int i=0; i<dimensi; i++){
//            for (int j=0; j<dimensi; j++){
//                if (CombineMatrix[j][i] > CombineMatrix[i][i]){
//                    for (int k=0; k<dimensi; k++) {
//                        newMatrix[i][k] = CombineMatrix[j][k];
//                    }
//                    newVector[i] = CombineMatrix[j][dimensi];
//                }
//                else{
//                    for (int k=0; k<dimensi; k++) {
//                        newMatrix[i][k] = CombineMatrix[i][k];
//                    }
//                    newVector[i] = CombineMatrix[i][dimensi];
//                }
//            }
//            int a = 1;
//        }
//
//        for (int i=0; i<dimensi; i++) {
//            for (int j=0; j<dimensi; j++) {
//                matrix[i][j] = newMatrix[i][j];
//            }
//            konstan[i] = newVector[i];
//        }
//    }

    public void utama(double[][] matrix, double[] konstan){
        while (error>tol){
            RHS[i] = konstan [i];
            for (int j=0; j<dimensi; j++){
                if (j != i) {
                    RHS[i] = (RHS[i] - (matrix[i][j] * RHS[j]));
                }
            }
            RHS[i] = RHS[i]/ matrix[i][i];
            i++;
            if (i == dimensi){
                i = 0;
                error = 0;
                for (int j=0; j<dimensi; j++){
                    error = error + Math.abs(akhir[j] - RHS[j]);
                    akhir [j] = RHS[j];
                }
                N++;
            }
        }
    }

    public double[] getAkhir() {
        return akhir;
    }
}