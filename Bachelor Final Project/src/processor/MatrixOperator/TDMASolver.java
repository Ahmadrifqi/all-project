package processor.MatrixOperator;
/*penyelesaian menggunakan Three-Diagonal Matrix Algorithm
* menggunakan buku Computational Fluid Dynamics
* Karangan John David Anderson
* menggunakan Appendix A*/

public class TDMASolver {

    private double matriksBaru[][];
    private double hasilBaru[];
    private double akhir[];

    public TDMASolver(int dim){
        matriksBaru = new double[dim][dim];
        hasilBaru = new double[dim];
        akhir = new double[dim];
    }

    public void DefinisiMatriksBaru(int dim, double hasil[],double matriks[][]){
        matriksBaru[0][0]=matriks[0][0];
        matriksBaru[0][1]=matriks[0][1];
        hasilBaru[0]=hasil[0];

        for (int i=1; i<dim-1; i++){
            matriksBaru [i][i] = matriks[i][i]-(matriks[i][i-1]*matriks[i-1][i]/matriksBaru[i-1][i-1]);
            matriksBaru[i][i+1] = matriks [i][i+1];
            hasilBaru [i] = hasil[i] - (hasilBaru[i-1]*matriks[i][i-1]/matriksBaru[i-1][i-1]);
        }

        matriksBaru[dim-1][dim-1]=matriks[dim-1][dim-1]-matriks[dim-1][dim-2]*matriks[dim-2][dim-1]/matriksBaru[dim-2][dim-2];
        hasilBaru[dim-1]= hasil[dim-1] - (hasilBaru[dim-2]*matriks[dim-1][dim-2]/matriksBaru[dim-2][dim-2]);
    }

    public void penyelesaian(int dim){
        akhir[dim-1] = hasilBaru[dim-1]/matriksBaru[dim-1][dim-1];
        for (int i=dim-2; i>=0; i--){
            akhir[i]=(hasilBaru[i]-akhir[i+1]*matriksBaru[i][i+1])/matriksBaru[i][i];
        }
    }

    public double[] getAkhir() {
        return akhir;
    }

}
