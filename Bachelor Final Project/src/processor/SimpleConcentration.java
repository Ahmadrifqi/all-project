package processor;

import org.jzy3d.analysis.AnalysisLauncher;
import processor.Field.PlotVelocityField;
import processor.GridGeneration.GridGeneration;
import processor.flowDispersion.PlotConcentration;
import processor.flowDispersion.StructuredUpwind;
import processor.flowDispersion.TestCase;
import processor.objectClasses.Coordinate;
import processor.objectClasses.Velocity;

import java.io.File;
import java.util.Scanner;

public class SimpleConcentration {

    public static void main(String[] Args) throws Exception {
        double dump1, dump2, dump3, dump4;
        int totalconcentration = 0, number = 0;
        double[][] Temp;
        Input_User input_user = new Input_User();
        Scanner Scan1 = new Scanner(new File("D:/soft/FluPy/cavity-steady"));
        float totaldt = Constant.timeStep;
        Velocity velocity[][] = new Velocity[52][52];
        Coordinate coordinate[][] = new Coordinate[52][52];

        for (int i=0; i<52; i++){
            for (int j=0; j<52; j++){
                velocity[51-i][51-j] = new Velocity();
                coordinate[51-i][51-j] = new Coordinate();
                if (i > 1 && i < 51 && j > 1 && j < 51) {
                    coordinate[51-i][51-j].setX(Scan1.nextDouble());
                    coordinate[51-i][51-j].setY(Scan1.nextDouble());
                    velocity[51-i][51-j].setX(10.0*Scan1.nextDouble());
                    velocity[51-i][51-j].setY(10.0*Scan1.nextDouble());
                    dump3 = Scan1.nextDouble();
                    dump4 = Scan1.nextDouble();
                }
            }
        }

        GridGeneration Grid = new GridGeneration();
        Grid.initiation();
        Grid.PointDefinition(input_user.locations);
        Grid.Mesh_Point_Definition("Structured");
        Grid.Total_Grid();
        Grid.Scalar_grid();

        TestCase CaseConcentration = new TestCase();
        CaseConcentration.setConcentration1(Grid.getCoordinate_scalar());
        double[][] Concentrationt, Concentrationmint, Concentrationmin2t;
        Concentrationt = new double[input_user.grid_x][input_user.grid_y];
        Concentrationmint = new double[input_user.grid_x][input_user.grid_y];
        Concentrationmin2t = new double[input_user.grid_x][input_user.grid_y];
        Temp = CaseConcentration.getConcentration();

        for (int i=0; i<50; i++){
            for (int j=0; j<50; j++){
                Concentrationmint[i][j] = Temp[i][j];
                Concentrationmin2t[i][j] = Temp[i][j];
                if (Temp[i][j] != 0){
                    totalconcentration++;
                    System.out.println(i + " " + j);
                }
            }
        }

        double seperateConcentration[][][] = new double[totalconcentration][input_user.grid_y][input_user.grid_x];
        
        for (int i=0; i<50; i++){
            for (int j=0; j<50; j++) {
                Concentrationmint[i][j] = Temp[i][j];
                Concentrationmin2t[i][j] = Temp[i][j];
                if (Temp[i][j] != 0) {
                    seperateConcentration[number][i][j] = Temp[i][j];
                    number++;
                }
            }
        }
        number = 0;

        while (number < totalconcentration) {
            StructuredUpwind Upwind = new StructuredUpwind(50, 50, seperateConcentration[number]);
            for (int i=0; i<50; i++){
                for (int j=0; j<50; j++){
                    Concentrationmint[i][j] = seperateConcentration[number][i][j];
                    Concentrationmin2t[i][j] = seperateConcentration[number][i][j];
                }
            }
            while (totaldt < 10) {
                Upwind.Swipe_X(Concentrationmint, Concentrationmin2t, velocity, Grid, totaldt);
                for (int i=0; i<50; i++){
                    for (int j=0; j<50; j++){
                        Concentrationt[i][j] = Upwind.getConcentration()[i][j];
                    }
                }
                for (int i = 0; i < 50; i++) {
                    for (int j = 0; j < 50; j++) {
                        Concentrationmin2t[i][j] = Concentrationmint[i][j];
                        Concentrationmint[i][j] = Concentrationt[i][j];
                    }
                }

                totaldt = totaldt + Constant.timeStep;
            }
            for (int i = 0; i < 50; i++) {
                for (int j = 0; j < 50; j++) {
                    seperateConcentration[number][i][j] = Concentrationt[i][j];
                }
            }
            totaldt = Constant.timeStep;
            number++;
        }

        for (int i = 0; i < 50; i++) {
            for (int j = 0; j < 50; j++) {
                double sum = 0;
                for (int k=0; k<totalconcentration; k++){
                    sum += seperateConcentration[k][i][j];
                }
                Concentrationt[i][j] = sum;
            }
        }

        int  a = 0;
        PlotConcentration plot = new PlotConcentration();
        plot.input(Concentrationt);
        AnalysisLauncher.open(plot);

        PlotVelocityField plotVelocityField = new PlotVelocityField(velocity, coordinate);
    }
}
