package processor.Boundary;

public class Boundary_Condition {
    private double boundary[], coefficient[];
    int dimensi;

    public Boundary_Condition(int dim){
        boundary = new double[dim];
        coefficient = new double[3];
        dimensi = dim;
    }

    public void Boundary(String Boundary, String position, double input){
        if (Boundary == "Wall") {
            for (int i=0; i<dimensi; i++){ //Baca lagi efek wall pada aliran
                boundary[i] = 0;
            }
            coefficient[0] = 0;
            coefficient[1] = 0;
            coefficient[2] = 0;
        }

        if (Boundary == "Opening"){
            //digunakan ekspansi taylor orde dua
            coefficient[0] = -3;
            coefficient[1] = 4;
            coefficient[2] = -1;
            if (position == "End"){
                for (int i = 0; i<3; i++){
                    coefficient[i] = -coefficient[i];
                }
            }
        }

        if ( Boundary == "Constant"){
            for (int i=0; i<dimensi; i++){
                boundary[i] = input;
            }
            coefficient[0] = 1.0;
            coefficient[1] = 0;
            coefficient[2] = 0;
        }
    }
    
//    public void Opening(String Boundary, String position){
//        //kondisi ini bisa langsung pake tabel persamaan turunan dari buku klauss
//        //kondisi dibatasi untuk d(variable)/d(arah) = 0
//        if (Boundary == "Opening"){
//            if (position == "Beggining") {
//                for (int i = 0; i < dimensi; i++) {
//                    boundary[i] = - 3*boundary[i] + 4*boundary[i+1] - boundary[i+2];
//                }
//            }
//            if (position == "End"){
//                for (int i = 0; i<dimensi; i++){
//                    boundary[i] = 3*boundary[i] - 4*boundary[i-1] + boundary[i-2];
//                }
//            }
//        }
//    }
//
//    public void Steady(String Boundary, double input){
//        if ( Boundary == "Constant"){
//            for (int i=0; i<dimensi; i++){
//                boundary[i] = input;
//            }
//        }
//    }

    public double[] getCoefficient() {
        return coefficient;
    }

    public double[] getBoundary() {
        return boundary;
    }
}
