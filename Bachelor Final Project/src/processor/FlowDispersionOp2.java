//package processor;
//
////import processor.flowDispersion.PecletNumber;
//import processor.objectClasses.Pressure;
//import processor.objectClasses.Toxic;
//import processor.objectClasses.Velocity;
//
//import java.awt.*;
//import java.awt.geom.Point2D;
//import java.util.ArrayList;
//
//public class FlowDispersionOp2 extends FieldOperator {
//    private Toxic[][] toxicField;
//    private ArrayList<Point> toxicSourceIdx;
//    private ArrayList<Point> toxicResultIdx;
//
//    public FlowDispersionOp2(VelocityFieldOperator SVFO){
//        super(SVFO.area);
//        this.velocityField = SVFO.getVelocityField();
//        this.pressureField = SVFO.getPressureField();
//        toxicField = new Toxic[SVFO.getLength()][SVFO.getWidth()];
//        toxicSourceIdx = new ArrayList<>();
//        toxicResultIdx = new ArrayList<>();
//    }
//
//    //menginisialisasi setiap elemen grid kecepatan dan tekanan dengan nilai 0
//    @Override
//    public void initiate() {
//        for(int x = 0; x< length; x++){
//            for(int y = 0; y< width; y++){
//                toxicField[x][y] = new Toxic();
//            }
//        }
//    }
//
//    public void addToxic(Toxic toxic, Point2D.Double location){
//        Point idx = realLocationToGridIdx(location);
//        toxicField[idx.x][idx.y] = toxic;
//        toxicSourceIdx.add(idx);
//        toxicResultIdx.add(idx);
//    }
//
//    public Toxic[][] getToxicField() {
//        return toxicField;
//    }
//
//    public void solve(double time){
//    //menentukan pecletNumber
//        //PecletNumber pecletNumber = new PecletNumber();
//       // pecletNumber.getPecletNumber(Constant.rho, velocityField,Constant.eddyK, length, width);
//
//    }
//
//    public ArrayList<Point> getToxicResultIdx() {
//        return toxicResultIdx;
//    }
//
//    public ArrayList<Point> getToxicSourceIdx() {
//        return toxicSourceIdx;
//    }
//}
