//package processor;
//
//import processor.objectClasses.Area;
//import processor.objectClasses.Pressure;
//import processor.objectClasses.Velocity;
//
//import java.awt.*;
//import java.awt.geom.Point2D;
//
//public abstract class FieldOperator {
//    protected Area area;
//    protected int length;
//    protected int width;
//    protected Velocity[][] velocityField;
//    protected Pressure[][] pressureField;
//
//    FieldOperator(Area area){
//        this.area = area;
//        length = (int) area.length/ Constant.gridSize;
//        width = (int) area.width/Constant.gridSize;
//    }
//
//    public int getLength() {
//        return length;
//    }
//
//    public int getWidth() {
//        return width;
//    }
//
//    public Area getArea(){
//        return area;
//    }
//
//    public Point2D.Double getOrigin(){
//        return area.origin;
//    }
//
//    public double getAreaLength(){
//        return area.length;
//    }
//
//    public double getAreaWidth(){
//        return area.width;
//    }
//
//    public Velocity[][] getVelocityField() {
//        return velocityField;
//    }
//
//    public Pressure[][] getPressureField() {
//        return pressureField;
//    }
//
//    //konversi lokasi asli ke indeks grid
//    public Point realLocationToGridIdx(Point2D.Double location){
//        int x = (int) Math.abs(location.x-area.origin.x)/ Constant.gridSize;
//        int y = (int) Math.abs(location.y-area.origin.y)/ Constant.gridSize;
//        return new Point(x,y);
//    }
//
//    public Point2D.Double gridIdxToRealLocation(Point idx){
//        double x = idx.x * Constant.gridSize + area.origin.x;
//        double y = idx.y * Constant.gridSize + area.origin.y;
//
//        return new Point2D.Double(x,y);
//    }
//
//    public abstract void initiate();
//
//}
