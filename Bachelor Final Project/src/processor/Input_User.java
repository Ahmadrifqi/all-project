package processor;

import processor.objectClasses.Coordinate;

public class Input_User {
    public double locations[][][];
    public Coordinate[] Lokasi_Racun1, Lokasi_Racun2;
    public int grid_x, grid_y;
    public String Type_Mesh;
    public Coordinate centre;
    private double MaxDomain;

    public Input_User(){
        MaxDomain = 1000;
        locations = new double[1][2][4];
        Lokasi_Racun1 = new Coordinate[4];
        Lokasi_Racun2 = new Coordinate[4];

        locations[0][0][0] = 0;
        locations[0][1][0] = 0;
        locations[0][0][1] = MaxDomain;
        locations[0][1][1] = 0;
        locations[0][0][2] = MaxDomain;
        locations[0][1][2] = MaxDomain;
        locations[0][0][3] = 0;
        locations[0][1][3] = MaxDomain;

        for (int i=0; i<4; i++){
            Lokasi_Racun1[i] = new Coordinate();
            Lokasi_Racun2[i] = new Coordinate();
        }

        Lokasi_Racun1[0].setX(200);
        Lokasi_Racun1[1].setX(500);
        Lokasi_Racun1[2].setX(800);
        Lokasi_Racun1[3].setX(500);

        Lokasi_Racun1[0].setY(500);
        Lokasi_Racun1[1].setY(800);
        Lokasi_Racun1[2].setY(500);
        Lokasi_Racun1[3].setY(200);

        Lokasi_Racun2[0].setX(200);
        Lokasi_Racun2[1].setX(800);
        Lokasi_Racun2[2].setX(800);
        Lokasi_Racun2[3].setX(200);

        Lokasi_Racun2[0].setY(800);
        Lokasi_Racun2[1].setY(200);
        Lokasi_Racun2[2].setY(800);
        Lokasi_Racun2[3].setY(200);

        grid_x = 31;
        grid_y = 31;

        Type_Mesh = "Normal";

        centre = new Coordinate();

        centre.setX(MaxDomain/2);
        centre.setY(MaxDomain/2);
    }
}
