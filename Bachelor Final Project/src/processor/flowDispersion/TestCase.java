package processor.flowDispersion;

import processor.Input_User;
import processor.objectClasses.Coordinate;

public class TestCase {
    Input_User input = new Input_User();
    public int lokasiplotx, lokasiploty;
    private int length, width;
    private double Concentration[][], Source;

    public TestCase(){
        length = input.grid_x;
        width = input.grid_y;

        Source = 100;

        Concentration = new double[width][length];
    }

    public void setConcentration1(Coordinate ScalarGrid[][]) {
        double k1, k2, grid1, grid2;
        boolean Condition = false;
        int number = 0;
        while (number < 4) {
            for (int i = 1; i < input.grid_y + 1; i++) {
                for (int j = 1; j < input.grid_x + 1; j++) {
                    k1 = input.Lokasi_Racun1[number].getX() - ScalarGrid[i][j].getX();
                    k2 = input.Lokasi_Racun1[number].getY() - ScalarGrid[i][j].getY();

                    grid1 = (ScalarGrid[i][j+1].getX() - ScalarGrid[i][j].getX())/2.0;
                    grid2 = (ScalarGrid[i+1][j].getY() - ScalarGrid[i][j].getY())/2.0;

                    if (grid1 > k1 && grid2 > k2){
                        Concentration[i-1][j-1] = Source;
                        Condition = true;
                        lokasiplotx = j-1;
                        lokasiploty = i-1;
                        break;
                    }
                }
                if (Condition){
                    break;
                }
            }
            number ++;
            Condition = false;
        }
    }

    public void setConcentration2(Coordinate ScalarGrid[][]){
        double k1, k2, grid1, grid2;
        boolean Condition = false;
        int number = 0;
        while (number < 4) {
            for (int i = 1; i < input.grid_y + 1; i++) {
                for (int j = 1; j < input.grid_x + 1; j++) {
                    k1 = input.Lokasi_Racun2[number].getX() - ScalarGrid[i][j].getX();
                    k2 = input.Lokasi_Racun2[number].getY() - ScalarGrid[i][j].getY();

                    grid1 = (ScalarGrid[i][j+1].getX() - ScalarGrid[i][j].getX())/2.0;
                    grid2 = (ScalarGrid[i+1][j].getY() - ScalarGrid[i][j].getY())/2.0;

                    if (grid1 > k1 && grid2 > k2){
                        Concentration[i-1][j-1] = Source;
                        Condition = true;
                        lokasiplotx = j-1;
                        lokasiploty = i-1;
                        break;
                    }
                }
                if (Condition){
                    break;
                }
            }
            number ++;
            Condition = false;
        }
    }

    public void setConcentration3(){
        for (int i=0; i<input.grid_x; i++){
            Concentration[input.grid_y-1][i] = Source;
        }
        for (int i=0; i<input.grid_y; i++){
            Concentration[i][0] = Source;
        }
    }

    public double[][] getConcentration() {
        return Concentration;
    }
}
