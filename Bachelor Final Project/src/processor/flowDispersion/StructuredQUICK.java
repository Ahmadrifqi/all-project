package processor.flowDispersion;

import processor.GridGeneration.GridGeneration;
import processor.MatrixOperator.GaussJordanElimination;
import processor.objectClasses.CellArea;
import processor.objectClasses.Coordinate;
import processor.objectClasses.Velocity;
import processor.MatrixOperator.GaussSeidel;
import processor.Constant;

public class StructuredQUICK {
   private double matriks [][];
   private double Concentration[][], base[][];
   private double vektor [];
   private int panjang;
   private int lebar;
   private double De, Dw, Ds, Dn;
   private double Fe, Fw, Fs, Fn;

   public StructuredQUICK(int length, int width, double[][] Base){
       panjang = length;
       lebar = width;
       Concentration = new double[width][length];
       matriks = new double[length][length];
       vektor = new double[length];
       base = Base;
   }

   //digunakan skema QUICK untuk menghindari trunctuation error
   //digunakan skema eksplisit, karena time step yang digunakan masih kecil
   public void setConcentration_SwipeX(double concentration[][], Velocity velocities[][], GridGeneration Grid, float totaldt) {
       double error, temp[][], temp1[];
       double dxe, dxw, dyn, dys;
       error = 1;
       temp = new double[lebar][panjang];
       double x, y;
       double ku, kc, kd;
       boolean bool = false;

       for (int i = 0; i < lebar; i++) {
           for (int j = 0; j < panjang; j++) {
               Concentration[i][j] = concentration[i][j];
               temp[i][j] = concentration[i][j];
           }
       }

       while (error > Constant.error_reference) {
           error = 0;

           for (int i = 0; i < lebar; i++) {
               for (int j = 0; j < panjang; j++) {
                   CellArea[][] GridSize = Grid.getArea_Scalar();
                   Coordinate[][] coordinates = Grid.getCoordinate_scalar();
                   Coordinate[][] FaceLocation = Grid.getPoint();

                   dxe = coordinates[i + 1][j + 2].getX() - coordinates[i + 1][j + 1].getX();
                   dxw = coordinates[i + 1][j + 1].getX() - coordinates[i + 1][j].getX();
                   dyn = coordinates[i + 2][j + 1].getY() - coordinates[i + 1][j + 1].getY();
                   dys = coordinates[i + 1][j + 1].getY() - coordinates[i][j + 1].getY();

                   Fe = Constant.rho * velocities[i + 1][j + 1].x * GridSize[i + 1][j + 1].getDe();
                   Fw = Constant.rho * velocities[i + 1][j].x * GridSize[i + 1][j + 1].getDw();
                   Fn = Constant.rho * velocities[i + 1][j + 1].y * GridSize[i + 1][j + 1].getDn();
                   Fs = Constant.rho * velocities[i][j + 1].y * GridSize[i + 1][j + 1].getDs();

                   De = GridSize[i + 1][j + 1].getDe() * Constant.eddyK / dxe;
                   Dw = GridSize[i + 1][j + 1].getDw() * Constant.eddyK / dxw;
                   Dn = GridSize[i + 1][j + 1].getDn() * Constant.eddyK / dyn;
                   Ds = GridSize[i + 1][j + 1].getDs() * Constant.eddyK / dys;

                   if (Fw < 0) {
                       if (j == 0) {
                           matriks[j][j] += Fw; //4.0/3.0*Fw;
                           //matriks[j][j + 1] -= 1.0/3.0*Fw;
                       }
                       else {
                           x = (FaceLocation[i + 1][j + 1].getX() + FaceLocation[i + 2][j + 1].getX()) / 2.0;

                           kc = (x - coordinates[i + 1][j - 1].getX()) * (x - coordinates[i + 1][j + 1].getX()) /
                                   ((coordinates[i + 1][j].getX() - coordinates[i + 1][j - 1].getX()) * (coordinates[i + 1][j].getX() - coordinates[i + 1][j + 1].getX()));

                           kd = (x - coordinates[i + 1][j - 1].getX()) * (x - coordinates[i + 1][j].getX()) /
                                   ((coordinates[i + 1][j + 1].getX() - coordinates[i + 1][j - 1].getX()) * (coordinates[i + 1][j + 1].getX() - coordinates[i + 1][j].getX()));

                           ku = 1 - kc - kd;

                           if (j == 1) {
                               matriks[j][j - 1] += kc * Fw;
                               matriks[j][j] += kd * Fw;

                               //efek boundary opening
                               matriks[j][j - 1] += ku * Fw; //4.0/3.0*ku*Fw;
                               //matriks[j][j] = matriks[j][j] - 1.0/3.0*ku*Fw;
                           }
                           else {
                               matriks[j][j - 2] += ku * Fw;
                               matriks[j][j] += kd * Fw;
                               matriks[j][j - 1] += kc * Fw;
                           }
                           if (j > 1 && base[i][j - 2] != 0){
                               vektor[j] -= 2*ku*Fw*base[i][j-2];
                               matriks[j][j - 1] -= ku*Fw;
                               matriks[j][j - 2] -= ku*Fw;
                           }
                           if (j > 0 && base[i][j - 1] != 0){
                               if (j > 1) {
                                   matriks[j][j - 2] -= ku * Fw;
                               }
                               matriks[j][j - 1] -= kc*Fw;
                               matriks[j][j] -= kd*Fw;
                               vektor[j] -= Fw*base[i][j - 1];
                           }
                       }
                   }

                   if (Fw > 0) {
                       x = (FaceLocation[i + 1][j + 1].getX() + FaceLocation[i + 2][j + 1].getX()) / 2.0;
                       kc = (x - coordinates[i + 1][j + 2].getX()) * (x - coordinates[i + 1][j].getX()) /
                               ((coordinates[i + 1][j + 1].getX() - coordinates[i + 1][j + 2].getX()) * (coordinates[i + 1][j + 1].getX() - coordinates[i + 1][j].getX()));

                       kd = (x - coordinates[i + 1][j + 2].getX()) * (x - coordinates[i + 1][j + 1].getX()) /
                               ((coordinates[i + 1][j].getX() - coordinates[i + 1][j + 1].getX()) * (coordinates[i + 1][j].getX() - coordinates[i + 1][j + 2].getX()));

                       ku = 1 - kc - kd;

                       if (j == 0) {
                           matriks[j][j] += kc * Fw;
                           matriks[j][j + 1] += kd * Fw;

                           //Boundary effect
                           matriks[j][j] += ku * Fw;
                       }
                       else {
                           if (j == panjang - 1) {
                               matriks[j][j - 1] += kd * Fw;
                               matriks[j][j] += kc * Fw;

                               //Boundary effect
                               matriks[j][j] += ku * Fw;
                           }
                           else {
                               matriks[j][j - 1] += ku * Fw;
                               matriks[j][j] += kc * Fw;
                               matriks[j][j + 1] += kd * Fw;
                           }
                           if (j < panjang - 2 && base[i][j + 1] != 0) {
                               vektor[j] -= 2*ku*Fw*base[i][j + 1];
                               matriks[j][j] -= ku*Fw;

                               matriks[j][j + 1] -= kd*Fw;
                           }
                       }
                   }

                   if (Fe < 0) {
                       if (j == panjang - 1) {
                           matriks[j][j] += Fe; //4.0/3.0*Fe;
                           //matriks[j][j - 1] = matriks[j][j - 1] - 1.0/3.0*Fe;
                       }
                       else {
                           x = (FaceLocation[i + 1][j + 2].getX() + FaceLocation[i + 2][j + 2].getX()) / 2.0;

                           kc = (x - coordinates[i + 1][j + 1].getX()) * (x - coordinates[i + 1][j + 3].getX()) /
                                   ((coordinates[i + 1][j + 2].getX() - coordinates[i + 1][j + 1].getX()) * (coordinates[i + 1][j + 2].getX() - coordinates[i + 1][j + 3].getX()));

                           kd = (x - coordinates[i + 1][j + 1].getX()) * (x - coordinates[i + 1][j + 2].getX()) /
                                   ((coordinates[i + 1][j + 3].getX() - coordinates[i + 1][j + 1].getX()) * (coordinates[i + 1][j + 3].getX() - coordinates[i + 1][j + 2].getX()));

                           ku = 1 - kc - kd;

                           if (j == panjang - 2) {
                               matriks[j][j] += kd * Fe;
                               matriks[j][j + 1] += kc * Fe;

                               //matriks[j][j] = matriks[j][j] - 1.0/3.0*kd*Fe;
                               matriks[j][j + 1] += ku * Fe; //4.0/3.0*ku*Fe;
                           }
                           else {
                               matriks[j][j] += kd * Fe;
                               matriks[j][j + 1] += kc * Fe;
                               matriks[j][j + 2] += ku * Fe;
                           }
                           if (j < panjang-2 && base[i][j + 2] != 0){
                               vektor[j] -= ku*Fe*base[i][j + 2];
                               matriks[j][j + 1] -= ku*Fe;

                               matriks[j][j + 2] -= ku*Fe;
                           }
                       }
                   }

                   if (Fe > 0) {
                       x = (FaceLocation[i + 1][j + 2].getX() + FaceLocation[i + 2][j + 2].getX()) / 2.0;
                       kc = (x - coordinates[i + 1][j].getX()) * (x - coordinates[i + 1][j + 2].getX()) /
                               ((coordinates[i + 1][j + 1].getX() - coordinates[i + 1][j].getX()) * (coordinates[i + 1][j + 1].getX() - coordinates[i + 1][j + 2].getX()));

                       kd = (x - coordinates[i + 1][j].getX()) * (x - coordinates[i + 1][j + 1].getX()) /
                               ((coordinates[i + 1][j + 2].getX() - coordinates[i + 1][j].getX()) * (coordinates[i + 1][j + 2].getX() - coordinates[i + 1][j + 1].getX()));

                       ku = 1 - kc - kd;
                       if (j == 0) {
                           matriks[j][j] += kc * Fe;
                           matriks[j][j + 1] = matriks[j][j + 1] + kd * Fe;

                           //Boundary Effect
                           matriks[j][j] += ku * Fe;
                       }
                       else {
                           if (j == panjang - 1) {
                               matriks[j][j - 1] += ku * Fe;
                               matriks[j][j] += kc * Fe;

                               //Boundary effect
                               matriks[j][j] += kd * Fe;
                           }
                           else {
                               matriks[j][j - 1] += ku * Fe;
                               matriks[j][j] += kc * Fe;
                               matriks[j][j + 1] += kd * Fe;
                           }
                           if (j < panjang-1 && base[i][j - 1] != 0){
                               vektor[j] -= 2*ku*Fe*base[i][j - 1];
                               matriks[j][j] -= ku*Fe;

                               matriks[j][j - 1] -= ku*Fe;
                           }
                           if (j == panjang-1 && base[i][j - 1] != 0){
                               vektor[j] -= 2*ku*Fe*base[i][j - 1];
                               matriks[j][j] += kd*Fe - ku*Fe;
                           }
                       }
                       int a = 1;
                   }


                   if (Fs < 0){
                       if (i == 0){
                           matriks[j][j] += Fs; //4.0/3.0*Fs;
                           //vektor[j] += 1.0/3.0*Fs*concentration[i+1][j];
                       }

                       else{
                           y = (FaceLocation[i+1][j+1].getY() + FaceLocation[i+1][j+2].getY())/2.0;

                           kc = (y - coordinates[i+1][j+1].getY())*(y - coordinates[i-1][j+1].getY())/
                               ((coordinates[i][j+1].getY() - coordinates[i+1][j+1].getY()) * (coordinates[i][j+1].getY() - coordinates[i-1][j+1].getY()));

                           kd = (y - coordinates[i][j+1].getY()) * (y - coordinates[i-1][j+1].getY())/
                               ((coordinates[i+1][j+1].getY() - coordinates[i][j+1].getY()) * (coordinates[i+1][j+1].getY() - coordinates[i-1][j+1].getY()));

                           ku = 1 - kc - kd;

                           if (i == 1){
                               matriks[j][j] += kd*Fs;
                               vektor[j] -= kc*Fs*Concentration[i-1][j];

                               //matriks[j][j] = matriks[j][j] - 1.0/3.0*Fs;
                               vektor[j] -= Fs*Concentration[i-1][j]; //4.0/3.0*Fs*concentration[i-1][j];
                           }

                           else{
                               matriks[j][j] += kd*Fs;
                               vektor[j] -= kc*Concentration[i - 1][j]*Fs;
                               vektor[j] -= ku*Concentration[i - 2][j]*Fs;
                           }
                       }
                   }

                   if (Fs > 0){
                       y = (FaceLocation[i+1][j+1].getY() + FaceLocation[i+1][j+2].getY())/2.0;

                       kc = (y - coordinates[i+2][j+1].getY()) * (y - coordinates[i][j+1].getY())/
                               ((coordinates[i+1][j+1].getY() - coordinates[i+2][j+1].getY()) * (coordinates[i+1][j+1].getY() - coordinates[i][j+1].getY()));

                       kd = (y - coordinates[i+2][j+1].getY()) * (y - coordinates[i+1][j+1].getY())/
                               ((coordinates[i][j+1].getY() - coordinates[i+2][j+1].getY()) * (coordinates[i][j+1].getY() - coordinates[i+1][j+1].getY()));

                       ku = 1 - kc - kd;

                       matriks[j][j] += kc*Fs;

                       if (i == 0){
                           vektor[j] -= ku*Concentration[i+1][j]*Fs;

                           matriks[j][j] += kd*Fs; //4.0/3.0*kd*Fs;
                           //vektor[j] += 1.0/3.0*concentration[i+1][j]*kd*Fs;
                       }
                       else {
                           if (i == lebar - 1) {
                               vektor[j] -= kd * Concentration[i - 1][j] * Fs;

                               matriks[j][j] += ku * Fs;//4.0/3.0*ku*Fs;
                               //vektor[j] +=1.0/3.0*concentration[i-1][j]*ku*Fs;
                           } else {
                               vektor[j] -= kd * Concentration[i - 1][j] * Fs;
                               vektor[j] -= ku * Concentration[i + 1][j] * Fs;
                           }
                       }
                   }

                   if (Fn < 0){
                       if (i == lebar-1){
                           matriks[j][j] += Fn; //4.0/3.0*Fn;
                           //vektor[j] += 1.0/3.0*concentration[i-1][j];
                       }

                       else{
                           y = (FaceLocation[i+2][j+1].getY() + FaceLocation[i+2][j+2].getY())/2.0;

                           kc = (y - coordinates[i+3][j+1].getY()) * (y - coordinates[i+1][j+1].getY())/
                                   ((coordinates[i+2][j+1].getY() - coordinates[i+3][j+1].getY()) * (coordinates[i+2][j+1].getY() - coordinates[i+1][j+1].getY()));

                           kd = (y - coordinates[i+3][j+1].getY()) * (y - coordinates[i+2][j+1].getY())/
                                   ((coordinates[i+1][j+1].getY() - coordinates[i+3][j+1].getY()) * (coordinates[i+1][j+1].getY() - coordinates[i+2][j+1].getY()));

                           ku = 1 - kc - kd;

                           matriks[j][j] += kd*Fn;

                           if (i == lebar-2){
                               vektor[j] -= kc*Concentration[i+1][j]*Fn;

                               //matriks[j][j] = matriks[j][j] - 1.0/3.0*ku*Fn;
                               vektor[j] -= ku*Concentration[i+1][j]*Fn; //4.0/3.0*ku*concentration[i+1][j]*Fn;
                           }

                           else{
                               vektor[j] -= kc*Concentration[i+1][j]*Fn;
                               vektor[j] -= ku*Concentration[i+2][j]*Fn;
                           }
                       }
                   }

                   if (Fn > 0){
                       y = (FaceLocation[i+2][j+1].getY() + FaceLocation[i+2][j+2].getY())/2.0;

                       kc = (y - coordinates[i][j+1].getY()) * (y - coordinates[i+2][j+1].getY())/
                               ((coordinates[i+1][j+1].getY() - coordinates[i][j+1].getY()) * (coordinates[i+1][j+1].getY() - coordinates[i+2][j+1].getY()));

                       kd = (y - coordinates[i+2][j+1].getY()) * (y - coordinates[i+1][j+1].getY())/
                               ((coordinates[i][j+1].getY() - coordinates[i+2][j+1].getY()) * (coordinates[i][j+1].getY() - coordinates[i+1][j+1].getY()));

                       ku = 1 - kc - kd;

                       matriks[j][j] += kc*Fn;

                       if (i == 0){
                           vektor[j] -= kd*Concentration[i+1][j]*Fn;

                           matriks[j][j] += ku*Fn; //4.0/3.0*ku*Fn;
                           //vektor[j] += 1.0/3.0*ku*concentration[i+1][j]*Fn;
                       }

                       else {
                           if (i == lebar - 1) {
                               vektor[j] -= ku * Concentration[i - 1][j] * Fn;

                               matriks[j][j] += kd * Fn; //4.0/3.0*kd*Fn;
                               //vektor[j] += 1.0/3.0*kd*concentration[i-1][j]*Fn;
                           }
                           else {
                               vektor[j] -= ku * Concentration[i - 1][j] * Fn;
                               vektor[j] -= kd * Concentration[i + 1][j] * Fn;
                           }
                       }
                       int a = 1;
                   }

                   //diffusive term
                   if (j == 0){
                       matriks[j][j] += De;
                       matriks[j][j + 1] -= De;
                   }

                   if (j > 0 && j < panjang-1){
                       matriks[j][j - 1] += Dw;
                       matriks[j][j] += De - Dw;
                       matriks[j][j + 1] -= De;
                   }

                   if (j == panjang-1){
                       matriks[j][j - 1] += Dw;
                       matriks[j][j] -= Dw;
                   }

                   //Semi-Implicit Term
                   if (i == 0){
                       matriks[j][j] += Dn;
                       vektor[j] += Dn*Concentration[i+1][j];
                   }

                   if (i > 0 && i < lebar-1){
                       matriks[j][j] += Dn - Ds;
                       vektor[j] += Dn*Concentration[i+1][j] - Ds*Concentration[i-1][j];
                   }

                   if (i == lebar-1){
                       matriks[j][j] -= Ds;
                       vektor[j] -= Ds*Concentration[i-1][j];
                   }
                  //non-zero cell value term
                   if (j > 0 && j < panjang - 1) {
                       //Diffusive term corrector due to non-zero cell value
                       if (concentration[i][j - 1] != 0) {
                           matriks[j][j] -= 2.0 * Dw;
                           matriks[j][j + 1] += 1.0 / 3.0 * Dw;

                           vektor[j] -= 8.0 / 3.0 * Dw * concentration[i][j - 1];

                           vektor[j] -= Fw * concentration[i][j - 1];
                           matriks[j][j - 1] = 0;
                       }

                       if (concentration[i][j + 1] != 0) {
                           matriks[j][j - 1] -= 1.0 / 3.0 * De;
                           matriks[j][j] += 2.0 * De;

                           vektor[j] += 8.0 / 3.0 * De * concentration[i][j + 1];

                           vektor[j] -= Fe * concentration[i][j + 1];
                           matriks[j][j + 1] = 0;
                       }
                   }

                   if (j == 0){
                       if (concentration[i][j + 1] != 0){
                           matriks[j][j + 1] = 0;
                           matriks[j][j] += 2.0 * De + De/3.0;
                           vektor[j] += 8.0 / 3.0 * De * concentration[i][j + 1];}
                   }

                   if (j == panjang-1){
                       if (concentration[i][j - 1] != 0){
                           matriks[j][j - 1] = 0;
                           matriks[j][j] -= 2.0 * Dw + Dw/3.0;
                           vektor[j] -= 8.0 / 3.0 * Dw * concentration[i][j - 1];
                       }
                   }

                   if (base[i][j] != 0){
                       for (int k=0; k < panjang; k++) {
                           matriks[j][k] = 0;
                       }
                       matriks[j][j] = 1;
                       vektor[j] = base[i][j];
                   }
                    //transient term
                   /*if (totaldt == Constant.timeStep) {
                           matriks[j][j] += Constant.rho / Constant.timeStep;

                           hasil[j] = hasil[j] + Constant.rho / Constant.timeStep * concentrationmin2t[i][j];
                           }
                     else {
                           matriks[j][j] += 3 * Constant.rho / (2 * Constant.timeStep);

                           hasil[j] = hasil[j] + 4 * Constant.rho / (2 * Constant.timeStep) * concentration[i][j];
                           hasil[j] = hasil[j] - Constant.rho / (2 * Constant.timeStep) * concentrationmin2t[i][j];
                           }*/
                   if (base[i][j] != 0){
                       bool = true;
                   }
               }/*
               GaussJordanElimination gaussJordanElimination = new GaussJordanElimination(matriks, vektor);
               gaussJordanElimination.solve();
               if (bool){
                   int a= 0;
               }
               temp1 = gaussJordanElimination.primal();*/
               GaussSeidel gaussSeidel = new GaussSeidel(panjang);
               gaussSeidel.inisiasi();
               gaussSeidel.utama(matriks, vektor);
               temp1 = gaussSeidel.getAkhir();
               for (int j=0; j<panjang; j++){
                   Concentration[i][j] = temp1[j];
               }
               for (int j=0; j<panjang; j++){
                   vektor[j] = 0;
                   for (int k=0; k<panjang; k++) {
                       matriks[j][k] = 0;
                   }
               }
           }

           for (int i = 0; i < lebar; i++) {
               for (int j = 0; j < panjang; j++) {
                   double error_temp;
                   error_temp = Math.abs(Concentration[i][j] - temp[i][j]);
                   error = Math.max(error, error_temp);
                   temp[i][j] = Concentration[i][j];
               }
           }
           System.out.println(error);
       }
   }

   public double[][] getConcentration() {
       return Concentration;
   }
}
