package processor.flowDispersion;

import processor.Constant;
import processor.GridGeneration.GridGeneration;
import processor.MatrixOperator.TDMASolver;
import processor.objectClasses.CellArea;
import processor.objectClasses.Coordinate;
import processor.objectClasses.Velocity;

public class StructuredCentralDifference {
    private double matriks [][], hasil [], error, alpha;
    private int panjang, lebar;
    private double Fe, Fw, Fn, Fs;
    private double De, Dw, Dn, Ds;
    private double Concentration[][];
    private double base[][];

    public StructuredCentralDifference(int length, int width, double Base[][]){
        panjang = length;
        lebar = width;
        Concentration = new double[width][length];
        base = new double[width][length];
        base = Base;
        alpha = 0.8;
    }

    //digunakan skema Central Difference
    public void setConcentration(double concentration[][], double concentrationmin2t[][], Velocity velocities[][],
                                 GridGeneration Grid, float totaldt) {
        //didefinisikan kecepatan aliran (u) positif sebagai kecepatan aliran dari barat menuju timur
        //kecepatan a;iran (v) positif sebagai kecepatan aliran dari selatan menuju utara
        // untuk u positif ----> C face w = C cell W && C face e = C cell P
        // untuk u negatif ----> C face w = C cell P && C face e = C cell E
        // untuk v positif ----> C face n = C cell P && C face s = C cell S
        // untuk v negatif ----> C face n = C cell N && C face s = C cell P

        double temp1[], temp[][];
        double dxe, dxw, dyn, dys;
        temp = new double[lebar][panjang];
        matriks = new double[panjang][panjang];
        hasil = new double[panjang];
        error = 1;

        for (int i = 0; i < lebar; i++) {
            for (int j = 0; j < panjang; j++) {
                Concentration[i][j] = concentration[i][j];
                temp[i][j] = concentration[i][j];
            }
        }

        while (error > Constant.error_reference) {
            error = 0;
            for (int i = 0; i < lebar; i++) {
                for (int j = 0; j < panjang; j++) {
                    hasil[j] = 0;
                    CellArea[][] GridSize = Grid.getArea_Scalar();
                    Coordinate[][] coordinates = Grid.getCoordinate_scalar();

                    dxe = coordinates[i + 1][j + 2].getX() - coordinates[i + 1][j + 1].getX();
                    dxw = coordinates[i + 1][j + 1].getX() - coordinates[i + 1][j].getX();
                    dyn = coordinates[i + 2][j + 1].getY() - coordinates[i + 1][j + 1].getY();
                    dys = coordinates[i + 1][j + 1].getY() - coordinates[i][j + 1].getY();

                    Fe = Constant.rho * velocities[i + 1][j + 1].x * GridSize[i + 1][j + 1].getDe();
                    Fw = Constant.rho * velocities[i + 1][j].x * GridSize[i + 1][j + 1].getDw();
                    Fn = Constant.rho * velocities[i + 1][j + 1].y * GridSize[i + 1][j + 1].getDn();
                    Fs = Constant.rho * velocities[i][j + 1].y * GridSize[i + 1][j + 1].getDs();

                    De = GridSize[i + 1][j + 1].getDe() * Constant.eddyK / dxe;
                    Dw = GridSize[i + 1][j + 1].getDw() * Constant.eddyK / dxw;
                    Dn = GridSize[i + 1][j + 1].getDn() * Constant.eddyK / dyn;
                    Ds = GridSize[i + 1][j + 1].getDs() * Constant.eddyK / dys;

                    if (j == 0){
                        //West Boundary
                        //Kondisi Batas dc/dx = 0

                        //Diffusion Term
                        matriks[j][j] += De;
                        matriks[j][j + 1] -= De;

                        //Advection Term
                        matriks[j][j] += Fe/2.0;
                        matriks[j][j + 1] += Fe/2.0;

                        //Source Term

                        //Boundary Term
                        matriks[j][j] += Fw;

                        int cok = 1;
                    }

                    else if (j == panjang-1){
                        //East Boundary
                        //Kondisi Batas dc/dx = 0

                        //Diffusion term
                        matriks[j][j] -= Dw;
                        matriks[j][j - 1] += Dw;

                        //Advection Term
                        matriks[j][j] += Fw/2.0;
                        matriks[j][j - 1] += Fw/2.0;

                        //Source Term

                        //Boundary Term
                        matriks[j][j] += Fe;
                    }

                    else{
                        //Diffusion Term
                        matriks[j][j] += De - Dw;
                        matriks[j][j - 1] += Dw;
                        matriks[j][j + 1] -= De;

                        //Advection Term
                        matriks[j][j] += Fe/2.0 + Fw/2.0;
                        matriks[j][j - 1] += Fw/2.0;
                        matriks[j][j + 1] += Fe/2.0;

                        int a = 1;

                        //non-zero Neighbour cell term
                        if (concentration[i][j + 1] != 0 && j < panjang) { //east cell
                            matriks[j][j] -= Fe/2.0;
                            matriks[j][j] = matriks[j][j] + De;
                            matriks[j][j + 1] -= Fe/2.0;

                            hasil[j] = hasil[j] - Math.min(Fe, 0) * concentration[i][j + 1];
                            hasil[j] = hasil[j] + 2.0 * De * concentration[i][j + 1];
                        }

                        if (concentration[i][j - 1] != 0) { //west cell
                            matriks[j][j - 1] -= Fw/2.0;
                            matriks[j][j] -= Fw/2.0;
                            matriks[j][j] = matriks[j][j] - Dw;

                            hasil[j] = hasil[j] - 2.0 * Dw * concentration[i][j - 1];
                            hasil[j] = hasil[j] - Math.min(Fw, 0) * concentration[i][j - 1];
                        }
                    }

                    if (i == 0){
                        //South Boundary
                        //Kondisi Batas dc/dy = 0

                        //Diffusion Term
                        matriks[j][j] += Dn;

                        //Advection Term
                        matriks[j][j] += Fn/2.0;

                        //Source Term

                        //Boundary Term
                        matriks[j][j] += Fs;

                        //Semi-Implicit Term
                        hasil[j] -= Fn/2.0*Concentration[i+1][j];
                        hasil[j] += Dn*Concentration[i+1][j];

                        int a = 1;
                    }

                    else if (i == lebar-1){
                        //North Boundary
                        //Kondisi Batas dc/dy = 0

                        //Diffusion Term
                        matriks[j][j] -= Ds;

                        //Advection Term
                        matriks[j][j] += Fs/2.0;

                        //Source Term

                        //Boundary Term
                        matriks[j][j] += Fn;

                        //Semi-Implicit Term
                        hasil[j] -= Fs/2.0*Concentration[i-1][j];
                        hasil[j] += Ds*Concentration[i-1][j];
                    }

                    else {
                        //Diffusion Term
                        matriks[j][j] += Dn - Ds;

                        //Advection Term
                        matriks[j][j] += Fn/2.0 + Fs/2.0;

                        //Source Term

                        //Boundary Term

                        //Semi-Implicit Term
                        hasil[j] -= Fn/2.0*Concentration[i+1][j];
                        hasil[j] += Dn*Concentration[i+1][j];
                        hasil[j] -= Fs/2.0*Concentration[i-1][j];
                        hasil[j] += Ds*Concentration[i-1][j];
                    }

                    if (totaldt == Constant.timeStep) {
                        matriks[j][j] = matriks[j][j] + Constant.rho / Constant.timeStep;

                        hasil[j] = hasil[j] + Constant.rho / Constant.timeStep * concentrationmin2t[i][j];
                    }

                    else {
                        matriks[j][j] = matriks[j][j] + 3 * Constant.rho / (2.0 * Constant.timeStep);

                        hasil[j] = hasil[j] + 4.0 * Constant.rho / (2.0 * Constant.timeStep) * concentration[i][j];
                        hasil[j] = hasil[j] - Constant.rho / (2.0 * Constant.timeStep) * concentrationmin2t[i][j];
                    }

                    if (base[i][j] != 0) {
                        for (int k = 0; k < panjang; k++) {
                            matriks[j][k] = 0;
                        }
                        matriks[j][j] = 1;

                        hasil[j] = concentration[i][j];
                    }
                }
                TDMASolver tdma = new TDMASolver(panjang);
                tdma.DefinisiMatriksBaru(panjang, hasil, matriks);
                tdma.penyelesaian(panjang);
                temp1 = tdma.getAkhir();

                for (int j = 0; j < panjang; j++) {
                    Concentration[i][j] = Concentration[i][j] + alpha * (temp1[j] - Concentration[i][j]);
                }
                for (int j = 0; j < panjang; j++) {
                    for (int k = 0; k < panjang; k++) {
                        matriks[j][k] = 0;
                    }
                    hasil[j] = 0;
                }
            }
            for (int i = 0; i < lebar; i++) {
                for (int j = 0; j < panjang; j++) {
                    double error1;
                    error1 = Math.abs(Concentration[i][j] - temp[i][j]);
                    error = Math.max(error, error1);
                    temp[i][j] = Concentration[i][j];
                }
            }
            System.out.println(error);
        }
    }


    public double[][] getConcentration() {
        return Concentration;
    }
}
