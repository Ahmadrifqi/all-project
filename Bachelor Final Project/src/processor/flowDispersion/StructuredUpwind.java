package processor.flowDispersion;

import processor.Constant;
import processor.GridGeneration.GridGeneration;
import processor.MatrixOperator.TDMASolver;
import processor.objectClasses.CellArea;
import processor.objectClasses.Coordinate;
import processor.objectClasses.Velocity;

public class StructuredUpwind {
    private double matriks [][], hasil [], error, alpha;
    private int panjang, lebar;
    private double Fe, Fw, Fn, Fs;
    private double De, Dw, Dn, Ds;
    private double Concentration[][];
    private double base[][];

    public StructuredUpwind(int length, int width, double Base[][]){
        panjang = length;
        lebar = width;
        Concentration = new double[width][length];
        base = new double[width][length];
        base = Base;
        alpha = 0.8;
    }

    //digunakan skema Upwind
    //digunakan skema semi-implisit, karena time step yang digunakan masih kecil
    public void Swipe_X(double concentration[][], double concentrationmin2t[][], Velocity velocities[][],
                        GridGeneration Grid, float totaldt) {
        //didefinisikan kecepatan aliran (u) positif sebagai kecepatan aliran dari barat menuju timur
        //kecepatan a;iran (v) positif sebagai kecepatan aliran dari selatan menuju utara
        // untuk u positif ----> C face w = C cell W && C face e = C cell P
        // untuk u negatif ----> C face w = C cell P && C face e = C cell E
        // untuk v positif ----> C face n = C cell P && C face s = C cell S
        // untuk v negatif ----> C face n = C cell N && C face s = C cell P

        double temp1[], temp[][];
        double dxe, dxw, dyn, dys;
        temp = new double[lebar][panjang];
        matriks = new double[panjang][panjang];
        hasil = new double[panjang];
        error = 1;

        for (int i = 0; i < lebar; i++) {
            for (int j = 0; j < panjang; j++) {
                Concentration[i][j] = concentration[i][j];
                temp[i][j] = concentration[i][j];
            }
        }

        while (error > Constant.error_reference) {
            error = 0;
            for (int i = 0; i < lebar; i++) {
                for (int j = 0; j < panjang; j++) {
                    hasil[j] = 0;
                    CellArea[][] GridSize = Grid.getArea_Scalar();
                    Coordinate[][] coordinates = Grid.getCoordinate_scalar();

                    dxe = coordinates[i + 1][j + 2].getX() - coordinates[i + 1][j + 1].getX();
                    dxw = coordinates[i + 1][j + 1].getX() - coordinates[i + 1][j].getX();
                    dyn = coordinates[i + 2][j + 1].getY() - coordinates[i + 1][j + 1].getY();
                    dys = coordinates[i + 1][j + 1].getY() - coordinates[i][j + 1].getY();

                    Fe = Constant.rho * velocities[i + 1][j + 1].x * GridSize[i + 1][j + 1].getDe();
                    Fw = Constant.rho * velocities[i + 1][j].x * GridSize[i + 1][j + 1].getDw();
                    Fn = Constant.rho * velocities[i + 1][j + 1].y * GridSize[i + 1][j + 1].getDn();
                    Fs = Constant.rho * velocities[i][j + 1].y * GridSize[i + 1][j + 1].getDs();

                    De = GridSize[i + 1][j + 1].getDe() * Constant.eddyK / dxe;
                    Dw = GridSize[i + 1][j + 1].getDw() * Constant.eddyK / dxw;
                    Dn = GridSize[i + 1][j + 1].getDn() * Constant.eddyK / dyn;
                    Ds = GridSize[i + 1][j + 1].getDs() * Constant.eddyK / dys;

                    //kondisi batas lebih masuk akal dibuat outlet
                    if (j == 0) {
                        matriks[j][j] = matriks[j][j] + De;
                        matriks[j][j + 1] = matriks[j][j + 1] - De;

                        matriks[j][j] = matriks[j][j] + Math.max(0, Fw);
                        matriks[j][j] = matriks[j][j] + Math.max(0, Fe);
                        matriks[j][j + 1] = matriks[j][j + 1] + Math.min(Fe, 0);

                        //Boundary Effect
                        matriks[j][j] = matriks[j][j] + Math.min(0, Fw); //4.0*Math.min(0, Fw)/3.0;
                        matriks[j][j + 1] = matriks[j][j + 1] - 0; //Math.min(0, Fw)/3.0;
                    }

                    if (j == panjang - 1) {
                        matriks[j][j - 1] = matriks[j][j - 1] + Dw;
                        matriks[j][j] = matriks[j][j] - Dw;

                        matriks[j][j] = matriks[j][j] + Math.max(0, Fw) + Math.max(0, Fe);
                        matriks[j][j - 1] = matriks[j][j - 1] + Math.min(Fw, 0);

                        //Boundary Effect
                        matriks[j][j] = matriks[j][j] + Math.min(0, Fe); //4.0*Math.min(0, Fe)/3.0;
                        matriks[j][j - 1] = matriks[j][j - 1] - 0; //Math.min(0, Fe)/3.0;
                    }

                    if (j > 0 && j < panjang - 1) {
                        matriks[j][j - 1] = matriks[j][j - 1] + Dw;
                        matriks[j][j] = matriks[j][j] + De - Dw;
                        matriks[j][j + 1] = matriks[j][j + 1] - De;

                        matriks[j][j - 1] = matriks[j][j - 1] + Math.min(0, Fw);
                        matriks[j][j] = matriks[j][j] + Math.max(0, Fw) + Math.max(0, Fe);
                        matriks[j][j + 1] = matriks[j][j + 1] + Math.min(0, Fe);

                        if (concentration[i][j + 1] != 0 && j < panjang) { //east cell
                            matriks[j][j] = matriks[j][j] + De;
                            matriks[j][j + 1] = 0;

                            hasil[j] = hasil[j] - Math.min(Fe, 0) * concentration[i][j + 1];
                            hasil[j] = hasil[j] + 2.0 * De * concentration[i][j + 1];
                        }

                        if (concentration[i][j - 1] != 0) { //west cell
                            matriks[j][j - 1] = 0;
                            matriks[j][j] = matriks[j][j] - Dw;

                            hasil[j] = hasil[j] - 2.0 * Dw * concentration[i][j - 1];
                            hasil[j] = hasil[j] - Math.min(Fw, 0) * concentration[i][j - 1];
                        }
                    }

                    if (i == 0) {
                        matriks[j][j] = matriks[j][j] + Dn;
                        matriks[j][j] = matriks[j][j] + Math.max(0, Fs);
                        matriks[j][j] = matriks[j][j] + Math.max(0, Fn);

                        //Semi-Implicit
                        hasil[j] = hasil[j] + Dn * Concentration[i + 1][j];
                        hasil[j] = hasil[j] - Math.min(0, Fn) * Concentration[i + 1][j];

                        //Boundary effect
                        matriks[j][j] = matriks[j][j] + Math.min(0, Fs); //4.0*Math.min(0, Fs)/3.0;
                        hasil[j] = hasil[j] + 0; //Math.min(0, Fs)/3.0*Concentration[i + 1][j];
                    }

                    if (i > 0 && i < lebar - 1) {
                        matriks[j][j] = matriks[j][j] + Dn - Ds;
                        matriks[j][j] = matriks[j][j] + Math.max(0, Fn) + Math.max(0, Fs);

                        //Semi-Implicit
                        hasil[j] = hasil[j] + Dn * Concentration[i + 1][j] - Ds * Concentration[i - 1][j];
                        hasil[j] = hasil[j] - Math.min(0, Fn) * Concentration[i + 1][j];
                        hasil[j] = hasil[j] - Math.min(0, Fs) * Concentration[i - 1][j];
                    }

                    if (i == lebar - 1) {
                        int b = 1;
                        matriks[j][j] = matriks[j][j] - Ds;
                        matriks[j][j] = matriks[j][j] + Math.max(0, Fs) + Math.max(0, Fn);

                        //Semi-Implicit
                        hasil[j] = hasil[j] - Ds * Concentration[i - 1][j];
                        hasil[j] = hasil[j] - Math.min(0, Fs) * Concentration[i - 1][j];

                        //efek Boundary
                        matriks[j][j] = matriks[j][j] + Math.min(0, Fn); //4.0*Math.min(0, Fn)/3.0;
                        hasil[j] = hasil[j] - 0; //Math.min(0, Fn)*Concentration[i - 1][j]/3.0;
                    }

                    //transient term
                    if (totaldt == Constant.timeStep) {
                        matriks[j][j] = matriks[j][j] + Constant.rho / Constant.timeStep;

                        hasil[j] = hasil[j] + Constant.rho / Constant.timeStep * concentrationmin2t[i][j];
                    } else {
                        matriks[j][j] = matriks[j][j] + 3.0 * Constant.rho / (2.0 * Constant.timeStep);

                        hasil[j] = hasil[j] + 4.0 * Constant.rho / (2.0 * Constant.timeStep) * concentration[i][j];
                        hasil[j] = hasil[j] - Constant.rho / (2.0 * Constant.timeStep) * concentrationmin2t[i][j];
                    }

                    if (base[i][j] != 0) {
                        for (int k = 0; k < panjang; k++) {
                            matriks[j][k] = 0;
                        }
                        matriks[j][j] = 1;

                        hasil[j] = base[i][j];
                    }
                }
                TDMASolver tdmaSolver = new TDMASolver(panjang);
                tdmaSolver.DefinisiMatriksBaru(panjang, hasil, matriks);
                tdmaSolver.penyelesaian(panjang);
                temp1 = tdmaSolver.getAkhir();

                for (int j = 0; j < panjang; j++) {
                    Concentration[i][j] = Concentration[i][j] + alpha * (temp1[j] - Concentration[i][j]);
                }
                for (int j = 0; j < panjang; j++) {
                    for (int k = 0; k < panjang; k++) {
                        matriks[j][k] = 0;
                    }
                    hasil[j] = 0;
                }
            }
            for (int i = 0; i < lebar; i++) {
                for (int j = 0; j < panjang; j++) {
                    double error1;
                    error1 = Math.abs(Concentration[i][j] - temp[i][j]);
                    error = Math.max(error, error1);
                    temp[i][j] = Concentration[i][j];
                }
            }
            //System.out.println(error);
        }
    }


    public void Swipe_Y(double concentrationmint[][], double concentrationmin2t[][], Velocity velocities[][], GridGeneration Grid, float totaldt) {
        //didefinisikan kecepatan aliran (u) positif sebagai kecepatan aliran dari barat menuju timur
        //kecepatan a;iran (v) positif sebagai kecepatan aliran dari selatan menuju utara
        // untuk u positif ----> C face w = C cell W && C face e = C cell P
        // untuk u negatif ----> C face w = C cell P && C face e = C cell E
        // untuk v positif ----> C face n = C cell P && C face s = C cell S
        // untuk v negatif ----> C face n = C cell N && C face s = C cell P

        double temp1[], temp[][];
        double dxe, dxw, dyn, dys;
        temp = new double[lebar][panjang];
        matriks = new double[lebar][lebar];
        hasil = new double[lebar];
        error = 1;

        for (int i = 0; i < lebar; i++) {
            for (int j = 0; j < panjang; j++) {
                Concentration[i][j] = concentrationmint[i][j];
                temp[i][j] = concentrationmint[i][j];
            }
        }

        while (error > Constant.error_reference) {
            error = 0;
            for (int i = 0; i < panjang; i++) {
                for (int j = 0; j < lebar; j++) {
                    hasil[j] = 0;
                    CellArea[][] GridSize = Grid.getArea_Scalar();
                    Coordinate[][] coordinates = Grid.getCoordinate_scalar();

                    dxe = coordinates[j + 1][i + 2].getX() - coordinates[j + 1][i + 1].getX();
                    dxw = coordinates[j + 1][i + 1].getX() - coordinates[j + 1][i].getX();
                    dyn = coordinates[j + 2][i + 1].getY() - coordinates[j + 1][i + 1].getY();
                    dys = coordinates[j + 1][i + 1].getY() - coordinates[j][i + 1].getY();

                    Fe = Constant.rho * velocities[j + 1][i + 1].x * GridSize[j + 1][i + 1].getDe();
                    Fw = Constant.rho * velocities[j + 1][i].x * GridSize[j + 1][i + 1].getDw();
                    Fn = Constant.rho * velocities[j + 2][i + 1].y * GridSize[j + 1][i + 1].getDn();
                    Fs = Constant.rho * velocities[j + 1][i + 1].y * GridSize[j + 1][i + 1].getDs();

                    De = GridSize[j + 1][i + 1].getDe() * Constant.eddyK / dxe;
                    Dw = GridSize[j + 1][i + 1].getDw() * Constant.eddyK / dxw;
                    Dn = GridSize[j + 1][i + 1].getDn() * Constant.eddyK / dyn;
                    Ds = GridSize[j + 1][i + 1].getDs() * Constant.eddyK / dys;

                    //kondisi batas lebih masuk akal dibuat outlet
                    if (j == 0) { //South Boundary
                        matriks[j][j] = matriks[j][j] + Dn;
                        matriks[j][j + 1] = matriks[j][j + 1] - Dn;

                        matriks[j][j] = matriks[j][j] + Math.max(0, Fs) + Math.max(0, Fn);
                        matriks[j][j + 1] = matriks[j][j + 1] + Math.min(Fn, 0);

                        //Boundary Effect
                        matriks[j][j] = matriks[j][j] + Math.min(0, Fs);
                    }

                    if (j == lebar - 1) { //North Boundary
                        matriks[j][j - 1] = matriks[j][j - 1] + Ds;
                        matriks[j][j] = matriks[j][j] - Ds;

                        matriks[j][j - 1] = matriks[j][j - 1] + Math.min(Fs, 0);
                        matriks[j][j] = matriks[j][j] + Math.max(0, Fs) + Math.max(0, Fn);

                        //Boundary Effect
                        matriks[j][j] = matriks[j][j] + Math.min(0, Fn);
                    }

                    if (j > 0 && j < lebar - 1) {
                        matriks[j][j - 1] = matriks[j][j - 1] + Ds;
                        matriks[j][j] = matriks[j][j] + Dn - Ds;
                        matriks[j][j + 1] = matriks[j][j + 1] - Dn;

                        matriks[j][j - 1] = matriks[j][j - 1] + Math.min(0, Fs);
                        matriks[j][j] = matriks[j][j] + Math.max(0, Fs) + Math.max(0, Fn);
                        matriks[j][j + 1] = matriks[j][j + 1] + Math.min(0, Fn);

                        if (concentrationmint[j - 1][i] != 0) { //South Cell
                            matriks[j][j] = matriks[j][j] - Ds;
                            matriks[j][j - 1] = 0;

                            hasil[j] = hasil[j] - Math.min(Fs * concentrationmint[j - 1][i], 0);
                            hasil[j] = hasil[j] - 2.0 * Ds * concentrationmint[j - 1][i];
                        }

                        if (concentrationmint[j + 1][i] != 0 && j < lebar) { //North Cell
                            matriks[j][j] = matriks[j][j] + Dn;
                            matriks[j][j + 1] = 0;

                            hasil[j] = hasil[j] - Math.min(Fn * concentrationmint[j + 1][i], 0);
                            hasil[j] = hasil[j] + 2.0 * Dn * concentrationmint[j + 1][i];
                        }
                    }

                    if (i > 0 && i < panjang - 1) {
                        matriks[j][j] = matriks[j][j] + 2.0 * De - 2.0 * Dw;
                        matriks[j][j] = matriks[j][j] + Math.max(0, Fe) + Math.max(0, Fw);

                        //Semi-Implicit
                        hasil[j] = hasil[j] + 2.0 * De * Concentration[j][i + 1] - 2.0 * Dw * Concentration[j][i - 1];
                        hasil[j] = hasil[j] - Math.min(0, Fe) * Concentration[j][i + 1];
                        hasil[j] = hasil[j] - Math.min(0, Fw) * Concentration[j][i - 1];
                    }

                    if (i == 0) { //West Boundary
                        matriks[j][j] = matriks[j][j] + 2.0 * De;
                        matriks[j][j] = matriks[j][j] + Math.max(0, Fe) + Math.max(0, Fw);

                        //Semi-Implicit
                        hasil[j] = hasil[j] + 2.0 * De * Concentration[j][i + 1];
                        hasil[j] = hasil[j] - Math.min(0, Fe) * Concentration[j][i + 1];

                        //Boundary effect
                        matriks[j][j] = matriks[j][j] + Math.min(0, Fw);
                    }

                    if (i == panjang - 1) { //East Boundary
                        matriks[j][j] = matriks[j][j] - 2.0 * Dw;
                        matriks[j][j] = matriks[j][j] + Math.max(0, Fw) + Math.max(0, Fe);

                        //Semi-Implicit
                        hasil[j] = hasil[j] - 2.0 * Dw * Concentration[j][i - 1];
                        hasil[j] = hasil[j] - Math.min(0, Fw) * Concentration[j][i - 1];

                        //efek Boundary
                        matriks[j][j] = matriks[j][j] + Math.min(0, Fe);
                    }

                    //transient term
                    if (totaldt == Constant.timeStep){
                        matriks[j][j] = matriks[j][j] + Constant.rho/Constant.timeStep;

                        hasil[j] = hasil[j] + Constant.rho/Constant.timeStep*concentrationmin2t[i][j];
                    }
                    else{
                        matriks[j][j] = matriks[j][j] + 3*Constant.rho/(2*Constant.timeStep);

                        hasil[j] = hasil[j] + 4.0*Constant.rho/(2*Constant.timeStep)*concentrationmint[i][j];
                        hasil[j] = hasil[j] - Constant.rho/(2*Constant.timeStep)*concentrationmin2t[i][j];
                    }

                    if (base[j][i] != 0) {
                        for (int k = 0; k < lebar; k++) {
                            matriks[j][k] = 0;
                        }
                        matriks[j][j] = 1;

                        hasil[j] = concentrationmint[j][i];
                    }
                }
                TDMASolver tdmaSolver = new TDMASolver(lebar);
                tdmaSolver.DefinisiMatriksBaru(lebar, hasil, matriks);
                tdmaSolver.penyelesaian(lebar);
                temp1 = tdmaSolver.getAkhir();
                for (int k = 0; k < lebar; k++) {
                    Concentration[k][i] = Concentration[k][i] + alpha*(temp1[k] - Concentration[k][i]);
                }
                for (int j = 0; j < panjang; j++) {
                    for (int k = 0; k < panjang; k++) {
                        matriks[j][k] = 0;
                    }
                    hasil[j] = 0;
                }
            }

            for (int i = 0; i < lebar; i++) {
                for (int j = 0; j < panjang; j++) {
                    double error1;
                    error1 = Math.abs(Concentration[i][j] - temp[i][j]);
                    error = Math.max(error, error1);
                    temp[i][j] = Concentration[i][j];
                }
            }
            System.out.println(error);
        }
    }

    public double[][] getConcentration() {
        return Concentration;
    }

    public double getError() {
        return error;
    }
}
