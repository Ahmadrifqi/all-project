package processor.flowDispersion;

import org.jzy3d.analysis.AnalysisLauncher;
import processor.GridGeneration.GridGeneration;
import processor.Input_User;
import processor.Field.*;
import processor.objectClasses.Coordinate;
import processor.objectClasses.Velocity;
import java.lang.*;

public class TestQUICK {

    public static void main(String[] Args) throws Exception{
        float totaldt = 0.01f;
        double[][] Concentration;
        Input_User input = new Input_User();

        GridGeneration grid = new GridGeneration();
        grid.initiation();
        grid.PointDefinition(input.locations);
        grid.Mesh_Point_Definition("Structured");
        grid.Total_Grid();
        grid.Scalar_grid();
        Coordinate[][] ScalarGrid = grid.getCoordinate_scalar();

        processor.flowDispersion.TestCase Case = new processor.flowDispersion.TestCase();
        Case.setConcentration2(ScalarGrid);
        Concentration = Case.getConcentration();

        TestCaseField CaseField = new TestCaseField(input.grid_x, input.grid_y);
        CaseField.initiation();
        CaseField.ConstantXVelocity();
        CaseField.setVface();
        Velocity[][] Vel = CaseField.getVelocity();

        StructuredQUICK QUICK = new StructuredQUICK(input.grid_x, input.grid_y, Concentration);
        QUICK.setConcentration_SwipeX(Concentration, Vel, grid, totaldt);
        double[][] FinalConcentration = QUICK.getConcentration();

        PlotConcentration plot = new PlotConcentration();
        plot.input(FinalConcentration);
        AnalysisLauncher.open(plot);
    }
}
