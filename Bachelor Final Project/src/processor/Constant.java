package processor;

public class Constant {
    public static final float timeStep = 0.01f;
    public static final double rho = 1.225f;
    public static final double eddyK = 0.001;
    public static final double error_reference = 0.0001;
}
