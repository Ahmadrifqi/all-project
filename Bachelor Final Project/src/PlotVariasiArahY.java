import java.awt.Color;
import java.awt.BasicStroke;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.ui.ApplicationFrame;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

public class PlotVariasiArahY extends ApplicationFrame {

    public PlotVariasiArahY(String applicationTitle, String chartTitle, double[] dataCon, double[] dataY) {
        super(applicationTitle);
        double maxRange = 0, maxDomain = 0;
        JFreeChart xylineChart = ChartFactory.createXYLineChart(
                chartTitle,
                "Lokasi Y (m)",
                "Konsentrasi",
                createDataset(dataCon, dataY),
                PlotOrientation.VERTICAL,
                false, false, false);

        for (int i=0; i<dataCon.length; i++){
            maxRange = Math.max(maxRange, dataCon[i]);
            maxDomain = Math.max(maxDomain, dataY[i]);
        }

        XYPlot xyPlot = (XYPlot) xylineChart.getPlot();
        xyPlot.setDomainCrosshairVisible(true);
        xyPlot.setRangeCrosshairVisible(true);

        ChartPanel chartPanel = new ChartPanel(xylineChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 800));
        final XYPlot plot = xylineChart.getXYPlot();

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.RED);
        renderer.setSeriesStroke(0, new BasicStroke(4.0f));
        plot.setRenderer(renderer);
        setContentPane(chartPanel);

        NumberAxis domain = (NumberAxis) xyPlot.getDomainAxis();
        domain.setRange(-20, maxDomain+20);
        NumberAxis range = (NumberAxis) xyPlot.getRangeAxis();
        range.setRange(-10,maxRange+10);
    }

    private XYDataset createDataset(double Data[], double DataY[]) {
        final XYSeries DataCon = new XYSeries("");
        for (int i=0; i<Data.length; i++){
            DataCon.add(DataY[i], Data[i]);
        }

        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(DataCon);
        return dataset;
    }
}
