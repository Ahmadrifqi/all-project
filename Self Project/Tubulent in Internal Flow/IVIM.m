function [Cf, delta_star, teta] = IVIM(U0, U, x, point_tr, sep_point, nu, M)
fzeta = zeros(sep_point-point_tr,1); %banyaknya section antara x transisi dan separasi
kappa = 0.41;
B = 5.5;
error_Cf = 1;
error_Pe = 1;

for i=point_tr:sep_point
    Re_x = U0*U(i)*x(i)/nu;
    Re_L = U0*max(x(M), x(1))/nu;
    
    %menggunakan tebal boundary layer blasius
    delta(i) = 0.38*x(i)/(Re_x^0.2);
    
    %menggunakan Cf dari Blasius
    Cf(i) = 0.059/(Re_x^0.2);
    lambda = sqrt(2/Cf(i));
    
    %displacement thickness diasumsikan memenuhi hubungan BL blasius
    delta_star(i) = 0.048*x(i)/(Re_x^0.2);
    
    %menghitung wake strength parameter
    Pe(i) = (kappa*lambda*(delta_star(i)/delta(i)))-1;
    
    %definisi variabel yang akan digunakan untuk mencari zeta
    while error_Cf > 0.0001 || error_Pe > 0.0001
        V(i) = U(i)/U0;
        Tw(i) = 1/2*1.225*U0^2*Cf(i);
        v_star(i) = sqrt(Tw(i)/1.225);
        delta_plus(i) = delta(i)*v_star(i)/nu;
        zeta = v_star(i)/(U0*U(i));
        RHS(i) = U0*(U(i-1) + U(i+1))/(2*(x(i)-x(i-1)));
        RHS_2(i) = U0*(U(i-1)-2*U(i)+U(i+1))/(2*x(i)-x(i-1));
        
        %penyelesaian menggunakan runge kutta
        fzeta (point_tr) = 1; %nilai sebenarnya dari program viskos
        
        h = x(i+2) - x(i); %diasummsukian x(i+2) - x(i) = 2*(x(i+1) - x(i))
        
        if i == point_tr
            fzeta (i) = 1; %mengambil nilai dari Cf saat laminar dari program viskos
        else
            dzeta = -zeta/V(i)*RHS(i) + 1/V(i) * f(Pe(i), zeta, delta_plus(i), V(i), RHS(i), RHS_2(i), Re_L);
            k1 = h*dzeta;
            dzeta = -zeta/V(i+1)*RHS(i+1) + 1/V(i+1) * f(Pe(i+1), zeta+1/2*k1, delta_plus(i+1), V(i+1), RHS(i+1), RHS_2(i+1), Re_L);
            k2 = h*dzeta;
            dzeta = -zeta/V(i+1)*RHS(i+1) + 1/V(i+1) * f(Pe(i+1), zeta+1/2*k2, delta_plus(i+1), V(i+1), RHS(i+1), RHS_2(i+1), Re_L);
            k3 = h*dzeta;
            dzeta = -zeta/V(i+2)*RHS(i+2) + 1/V(i+2) * f(Pe(i+2), zeta+k3, delta_plus(i+2), V(i+2), RHS(i+2), RHS_2(i+2), Re_L);
            k4 = h*dzeta;
            fzeta (i+1) = fzeta(i) + 1/6*(k1+2*k2+2*k3+k4);
        end
        
        %mengoreksi nilai wake strength, Cf, serta displacement thickness
        %panjang sudah dinormalisasi, sehingga tidak diperlukan konversi
        %dari x* menjadi x
        Cf_2(i) = 2*fzeta(i)^2;
        temp = 1-(fzeta(i)*(1/kappa*log(delta_plus(i)) + B));
        Pe_2(i) = temp*kappa/(2*fzeta(i));
        error_Cf = abs(Cf(i) - Cf_2(i));
        error_Pe = abs(Pe(i) - Pe_2(i));
        Cf(i) = Cf_2(i);
        Pe(i) = Pe_2(i);
    end
    delta_star(i) = (Pe(i) + 1)*(delta_plus(i)*nu)/(V(i)*U0*kappa);
    teta(i) = ((Pe(i)+1) - fzeta(i)/kappa*(2+3.1667*Pe(i)+1.4857*Pe(i)^2))*(delta_plus(i)*nu)/(V(i)*U0*kappa);
end
end