function [point_tr ,x_tr] = OneStepMichel(U, teta, x, k1)
	%mendefinisikan variabel yang akan digunakan
	x_tr=0;
    
	for i=1:k1
		Re_teta = U(i)*teta(i)/1.4792e-05;
		Re_x = U(i)*x(i)/1.4792e-05;
		Temp = 2.9*(Re_x^0.4);
		if Temp-Re_teta < 10^-4
			x_tr = x(i);
            point_tr = i;
		end
	end
end