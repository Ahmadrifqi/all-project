%                         VISCOUS FLOW CODE
%==========================================================================
%Tugas Besar Mata Kuliah Aliran Viskos
%Ahmad Humaedi / 13614065
%Antonius Tyaswidyono / 23617017

%Code terdiri dari dua bagian: Vortex panel method & Thwaites laminar BL
%Persamaan yang digunakan tidak valid untuk sudut serang yang tinggi
clc;clear;

%_________________________________INPUT____________________________________
%Ketentuan Input :
%- titik-titik airfoil dimulai dari Trailing Edge memutar searah jarum jam
%sampai kembali ke Trailing Edge (TE-lower-LE-upper-TE)
%- jumlah input titik airfoil harus ganjil
%- titik-titik pada sumbu x dimasukkan kedalam file Xbody.txt
%- titik-titik pada sumbu y dimasukkan kedalam file Ybody.txt
%- file input (Xbody.txt dan Ybody.txt) diletakan pada direktori yang sama
%dengan file VISCOUS_CODE.m

%input kondisi terbang
AoA = 0;            %sudut serang (dalam degree)
U = 6;              %kecepatan free stream
miu = 0.0000181206; %Dynamics Viscosity
rho = 1.225;        %Density
nu = miu/rho;       %kinematics Viscosity


%__________________________Vortex Panel Method____________________________
%referensi : Kuthe-Chow
XbIn = fscanf(fopen('Xbody.txt'),'%f');
YbIn = fscanf(fopen('Ybody.txt'),'%f');
Xb = XbIn';
Yb = YbIn';
M = length(Xb)-1;
MP1 = M + 1;
Alpha = AoA * pi/180;

%definisi awal
for i=1:M
    ip1 = i + 1;
    X(i) = 0.5 * (Xb(i) + Xb(ip1));
    Y(i) = 0.5 * (Yb(i) + Yb(ip1));
    S(i) = sqrt ((Xb(ip1)-Xb(i))^2 + (Yb(ip1)-Yb(i))^2 );
    Theta(i) = atan2(Yb(ip1)-Yb(i), Xb(ip1)-Xb(i));
    Sine(i) = sin (Theta(i));
    Cosine(i) = cos (Theta(i));
    RHS(i) = sin (Theta(i) - Alpha);
    RHSC(i) = cos (Theta(i) - Alpha);
end
i=0;

%mencari solusi gamma
for i=1:M
    for j=1:M
        if (i==j)
            CN1(i,j) = -1;
            CN2(i,j) = 1;
            CT1(i,j) = 0.5*pi;
            CT2(i,j) = 0.5*pi;
        else
            A = -(X(i)-Xb(j))*Cosine(j) - (Y(i)-Yb(j))*Sine(j);
            B = (X(i)-Xb(j))^2 + (Y(i)-Yb(j))^2;
            C = sin(Theta(i)-Theta(j));
            D = cos(Theta(i)-Theta(j));
            E = (X(i)-Xb(j))*Sine(j) - (Y(i) - Yb(j))*Cosine(j);
            F = log (1 + S(j)*(S(j)+2*A)/B);
            G = atan2 (E*S(j), B+A*S(j));
            P = (X(i)-Xb(j))*sin(Theta(i)-2.*Theta(j))+(Y(i)-Yb(j))*cos(Theta(i)-2.*Theta(j));
            Q = (X(i)-Xb(j))*cos(Theta(i)-2.*Theta(j))-(Y(i)-Yb(j))*sin(Theta(i)-2.*Theta(j));
            CN2(i,j) = D + 0.5*Q*F/S(j) - (A*C+D*E)*G/S(j);
            CN1(i,j) = 0.5*D*F + C*G - CN2(i,j);
            CT2(i,j) = C + 0.5*P*F/S(j) +(A*D-C*E)*G/S(j);
            CT1(i,j) = 0.5*C*F - D*G -CT2(i,j);
        end
    end
end

for i=1:M
    AN(i,1) = CN1(i,1);
    AN(i,MP1) = CN2(i,M);
    AT(i,1) = CT1(i,1);
    AT(i,MP1)=CT2(i,M);
    for j=2:M
        AN(i,j) = CN1(i,j)+CN2(i,j-1);
        AT(i,j) = CT1(i,j)+CT2(i,j-1);
    end
    AN(MP1,1) = 1;
    AN(MP1,MP1) = 1;
    for j=2:M
        AN(MP1,j) = 0;
    end
    RHS(MP1) = 0;
end

Gam = inv(AN)*RHS';
Vtan = RHSC' + AT*Gam;
for i=1:M
    if Vtan(i)<0
       Vtan(i)=Vtan(i)*-1;
    end
end
Vplot = Vtan;
Cp = 1-Vtan.^2;
CpPlot = -Cp./max(Cp);



%________________________________THWAITES__________________________________
%Referensi : Dinamika Fluida (Lavi R Zuhal), Viscous Flow (Frank White) 

Uin = Vtan*U;
%.......................Calculate momentum thickness........................
teta = zeros(M,1);
%mencari titik stagnasi
for i=1:M
    if Uin(i) == min(Uin)
        sl = i;
    end
end
su = sl+1;

%upper airfoil
teta(su) = sqrt(0.075*nu/(abs((Uin((su)+1)-Uin(su))/(X((su)+1)-X((su))))));
for j=su+1:M
    integral1 = 0;
    for i=su+1:j
        integral1 = integral1 + (Uin(i)^5 + Uin(i-1)^5)*abs((X(i)-X(i-1)))/2 ;
    end
    teta(j) = sqrt(0.45*nu*integral1/(Uin(j)^6));
end
%lower airfoil
teta(sl) = sqrt(0.075*nu/(abs((Uin((sl)-1)-Uin(sl))/(X((sl)-1)-X((sl))))));
for j=sl-1:-1:1
    integral1 = 0;
    for i=sl-1:-1:j
        integral1 = integral1 + (Uin(i)^5 + Uin(i+1)^5)*abs((X(i)-X(i+1)))/2 ;
    end
    teta(j) = sqrt(0.45*nu*integral1/(Uin(j)^6));
end


%..............Calculate pressure gradient parameter (lamda)...............
lamda = zeros(M,1);
%upper
lamda(su) = ((teta(su)^2)/nu)*(Uin(su+1)-Uin(su))/abs((X(su+1)-X(su)));
for i=su+1:M
    lamda(i) = ((teta(i)^2)/nu)*(Uin(i)-Uin(i-1))/abs((X(i)-X(i-1)));
end
%lower
lamda(sl) = ((teta(sl)^2)/nu)*(Uin(sl-1)-Uin(sl))/abs((X(sl-1)-X(sl)));
for i=sl-1:-1:1
    lamda(i) = ((teta(i)^2)/nu)*(Uin(i)-Uin(i+1))/abs((X(i)-X(i+1)));
end

%............Calculate wall shear-stress and displacement thickness.........
L = zeros(M,1);
H = zeros(M,1);
Cf = zeros(M,1);
delta = zeros(M,1);
YBL = zeros(M,1);
%shear stress at wall
%upper
for i = su:M
    if lamda(i)<0.1 && lamda(i)>0
        L(i) = 0.22 + 1.57*lamda(i) -1.8*lamda(i)^2;
        H(i) = 2.61 - 3.75*lamda(i) + 5.24*lamda(i)^2;
    else if lamda(i)<=0 && lamda(i)>-0.1
        L(i) = 0.22 + 1.402*lamda(i) + 0.018*lamda(i)/(lamda(i)+0.107);
        H(i) = 2.088 + 0.0731/(lamda(i)+0.14);
        else
        L(i) = L(i-1);
        H(i) = H(i-1);
        end
    end
    if teta(i)==0
        Cf(i) = 0;
    else
        Cf(i) = 2*L(i)*nu/(Uin(i)*teta(i));
    end
    delta(i) = teta(i)*H(i);
end
%lower
for i = sl:-1:1
    if lamda(i)<0.1 && lamda(i)>0
        L(i) = 0.22 + 1.57*lamda(i) -1.8*lamda(i)^2;
        H(i) = 2.61 - 3.75*lamda(i) + 5.24*lamda(i)^2;
    else if lamda(i)<=0 && lamda(i)>-0.1
        L(i) = 0.22 + 1.402*lamda(i) + 0.018*lamda(i)/(lamda(i)+0.107);
        H(i) = 2.088 + 0.0731/(lamda(i)+0.14);
        else
        L(i) = L(i+1);
        H(i) = H(i+1);
        end
    end
    if teta(i)==0
        Cf(i) = 0;
    else
        Cf(i) = 2*L(i)*nu/(Uin(i)*teta(i));
    end
    delta(i) = teta(i)*H(i);
end
%evaluate separation point
%upper
for i=su:M
    if lamda(i)<=-0.09
        xsepu = X(i);
        k1 = i;
        break
    else
        xsepu = X(M);
        k1 = M;
    end
end
%lower
for i=sl:-1:1
    if lamda(i)<=-0.09
        xsepl = X(i);
        k2 = i;
        break
    else
        xsepl = X(1);
        k2 = 1;
    end
end

%boundary layer thickness
for i=1:M
   if Y(i)>0
       YBL(i) = Y(i) + delta(i);
   else
       YBL(i) = Y(i) - delta(i);
   end
end

%..................Calculate Coefficient of Lift and Drag..................
CP_u = 0;CP_l=0;
CF_u = 0;CF_l=0;
%Coefficient of lift
%upper
for i=su:M-1
    CP_u = CP_u + (Cp(i+1)+Cp(i))*(X(i+1)-X(i))/2;  
end
%lower
for i=sl:-1:2
    CP_l = CP_l + (Cp(i-1)+Cp(i))*(X(i-1)-X(i))/2;
end
Cl = (CP_l - CP_u)*cos(Alpha);
%Coefficient of drag
%upper
for i=su:k1-1
    CF_u = CF_u + (Cf(i+1)+Cf(i))*(X(i+1)-X(i))/2;
end
%lower
for i=sl:-1:k2+1
    CF_l = CF_l + (Cf(i-1)+Cf(i))*(X(i-1)-X(i))/2;
end
Cd = CF_u + CF_l ;
%.........................Plot and display result..........................
%plot
% figure(1)
% plot(X,Vplot)
% title('Velocity Distribution')
% xlabel('X/c')
% ylabel('v/V')
% grid on;
% figure(2)
% plot(X,CpPlot)
% title('Cp Distribution')
% xlabel('X/c')
% ylabel('Cp')
% grid on;
% figure(3)
% plot(X,Y,X,YBL)
% legend('airfoil','boundary layer')
% title('Boundary Layer')
% axis([0 1 -0.3 0.3]);
% xlabel('X')
% ylabel('Y')            
% grid on; 
% figure(4)
% plot(X,Cf)
% title('Shear Stress Distribution at Wall')
% axis([0 1 -0.02 0.02])
% xlabel('X')
% ylabel('Cf')
% grid on

%display
disp('     X          Y       Uin        Cp        Theta     Lamda      Cf      Delta')
disp([X',Y',Uin,Cp,teta,lamda,Cf,delta])
disp('Titik terjadinya separasi aliran')
disp('Bagian atas airfoil separasi aliran diperkirakan terjadi pada X=')
disp(xsepu)
disp('Bagian bawah airfoil separasi aliran diperkirakan terjadi pada X=')
disp(xsepl)
disp('Cl dan Cd airfoil yaitu:')
disp('Cl')
disp(Cl)
disp('Cd')
disp(Cd)