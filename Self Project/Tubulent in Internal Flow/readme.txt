TUGAS BESAR MATA KULIAH ALIRAN VISKOS
=====================================================================
Ahmad Humaedi / 13614065
Antonius Tyaswidyono / 23617017


Langkah-langkah menjalankan program:

1. Isi input pada file Xbody.txt dan Ybody.txt, input berupa titik-titik kurva airfoil.
   Pengisian titik dimulai dari Trailing edge memutar searah jarum jam ke bagian bawah airfoil sampai Leading edge hingga kembali ke Trailing edge
   Contohnya pada airfoil NACA 4412 sudah terdapat pada file Xbody.txt dan Ybody.txt

2. Buka file VISCOUS_CODE.m 

3. Isi input kondisi terbang pada program tersebut, input kondisi terbang berupa 
   kecepatan free stream, sudut serang, kerapatan udara, dynamics viscosity.

4. Jalankan program maka akan muncul 4 gambar dan tampilan hasil pada command window
   gambar yang akan muncul antara lain: 1.distribusi kecepatan, 2.Cp, 3.airfoil dan boundary layer, 4.Cf
   dan yang akan muncul pada command window yaitu hasil perhitungan antara lain
   posisi panel airfoil (X dan Y), kecepatan, Cp, momentum thickness, lamda, Cf, Displacement Thickness

5. Titik separasi serta Cl dan Cd akan muncul pada tampilan Command window. 

Catatan:
1. Jumlah input titik airfoil harus ganjil
2. Perhitungan integral dan diferensial menggunakan persamaan numerik orde 2 sehingga menghasilkan error yang cukup besar
3. Program mungkin menunjukan hasil yang tidak mungkin apabila menganalisis airfoil dengan sudut serang besar.
 

