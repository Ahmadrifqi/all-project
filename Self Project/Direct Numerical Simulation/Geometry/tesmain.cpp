#include <iostream>
#include <fstream>
#include <vector>

#include "cartesian.cpp"
#include "Mesh.cpp"
#include "ImportPoint.cpp"

using namespace std;

int main(){
	ofstream OutputFile;
	OutputFile.open("Out");

	vector<cartesian> InputPoint = ImportPoint("Geom");
	vector<vector<cartesian> > Coba = Mesh(InputPoint);

	for (int i=0; i<Coba.size(); i++){
		for (int j=0; j<Coba[i].size(); j++){
			OutputFile<<Coba[i][j].x<<" ";
		}
		OutputFile<<endl;
	}
	OutputFile<<endl;

	for (int i=0; i<Coba.size(); i++){
		for (int j=0; j<Coba[i].size(); j++){
			OutputFile<<Coba[i][j].y<<" ";
		}
		OutputFile<<endl;
	}
	OutputFile.close();

	return 0;
}