#include "cartesian.cpp"

char collision(cartesian p1, cartesian p2, cartesian p3, cartesian p4){
	//Define line 1 as p2 to p1
	//Define line 2 as p4 to p3
	
	char col = 'N';
	cartesian p5; 
	bool cond1 = false, cond2 = false;
	
	m1 = (p2.y - p1.y)/(p2.x - p1.x);
	c1 = p1.y - m1*p1.x;
	
	m2 = (p4.y - p3.y)/(p4.x - p3.x);
	c2 = p3.y - m2*p2.x;
	
	p5.x = (c2 - c1)/(m1 - m2);
	p5.y = m1*p5.x + c1;
	
	if (p2.x > p5.x && p5.x > p1.x){
		if (p2.y > p5.y && p5.y > p1.y){
			cond1 = true;
		}
	}
	
	if (p4.x > p5.x && p5.x > p3.x){
		if (p4.y > p5.y && p5.y > p3.y){
			cond2 = true;
		}
	}
	
	if (cond1 && cond2)
		col = 'Y';
	
	return col;
}