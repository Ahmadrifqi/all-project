#include <vector>
#include <cmath>

using namespace std;

vector<vector<cartesian> > Mesh(double Lengthx, double Lengthy, int meshx, int meshy){
	vector<vector<cartesian> > Results(meshy+2);
	double basex = 0.0;
	double basey = 0.0;
	double dx = Lengthx/meshx;
	double dy = Lengthy/meshy;

	for (int i = 0; i < Results.size(); ++i){
		Results[i] = vector<cartesian>(meshx+2);
		for (int j=0; j<Results[i].size(); j++){
			if (i == 0){
				Results[i][j].y = basey - dy/2.0;
			}
			else if (j == 0){
				Results[i][j].x = basex - dx/2.0;
			}
			else {
				Results[i][j].x = Results[i][j-1].x + dx;
				Results[i][j].y = Results[i-1][j].y + dy;
			}
		}
	}

	return Results;	
}
	