#include <vector>
#include <iostream>
using namespace std;

vector<vector<double> > Pivoting(vector<vector<double> > &Matrix, vector<double> &Vector){
	cout<<"Masuk pivoting"<<endl;
	int dim = Matrix.size();
	vector<vector<double> > AugMatrix(dim);
	vector<vector<double> > NewAugMatrix(dim);
	int PosMax[dim];
	double Max = 0;
	bool Pos[dim];
	bool Confirm = true;
	char COntinue;

	for (int i=0; i<dim; i++){
		AugMatrix[i] = vector<double>(dim+1);
		NewAugMatrix[i] = vector<double>(dim+1);
		for (int j=0; j<dim+1; j++){
			if (j<dim)
				AugMatrix[i][j] = Matrix[i][j];
			else
				AugMatrix[i][j] = Vector[i];
		}
		PosMax[i] = 0;
		Pos[i] = false;
	}

	for (int i=0; i<dim; ++i){
		for (int j=0; j<dim; j++){
			Max = AugMatrix[i][PosMax[i]];
			if (abs(AugMatrix[i][j]) > abs(Max)){
				PosMax[i] = j;
			}
		}
	}

	for (int i = 0; i < dim; ++i){
		if (Pos[PosMax[i]] == false)
			Pos[PosMax[i]] = true;
		else
			Confirm = false;
	}

	if (Confirm){
		for (int i = 0; i<dim; i++){
			for (int j=0; j<dim+1; j++){
				NewAugMatrix[PosMax[i]][j] = AugMatrix[i][j];
			}
		}

		return NewAugMatrix;
	}

	else{
		cout<<"The Matrix is not diagonally dominant."<<endl;
		cout<<" The calculation might be unstable."<<endl;
		cout<<" Do you still want to proceed (Y/N)? ";
		cin>>COntinue;
		while (COntinue != 'N' && COntinue != 'Y'){
			cout<<"Input yang bener des!";
			cin>>COntinue;
		}
		if (COntinue == 'N'){
			//return 0;
		}
		if (COntinue == 'Y'){
			return AugMatrix;
		}
	}
}