#include <cmath>
#include <iostream>
#include <vector>

#include "ScalarPointDefinition.cpp"
using namespace std;

vector<vector<cartesian> > AirfoilMesh(vector<cartesian> AirfoilCoordinate){
	int dim = (AirfoilCoordinate.size()-1)/2;
	vector<vector<cartesian> > Block1(dim);
	vector<vector<cartesian> > Block2(dim);
	vector<vector<cartesian> > Block3(dim);
	vector<vector<cartesian> > Block4(dim);
	vector<vector<cartesian> > Block5(dim);
	vector<vector<cartesian> > Block6(dim);
	double c, LE = 0, TE = 0;

	for (int i=0; i<dim; i++){
		Block1[i] = vector<cartesian>(dim);
		Block2[i] = vector<cartesian>(dim);
		Block3[i] = vector<cartesian>(dim);
		Block4[i] = vector<cartesian>(dim);
		Block5[i] = vector<cartesian>(dim);
		Block6[i] = vector<cartesian>(dim);
		
		for (int j=0; j<dim; j++){
			Block1[i][j].x = 0;
			Block2[i][j].x = 0;
			Block3[i][j].x = 0;
			Block4[i][j].x = 0;
			Block5[i][j].x = 0;
			Block6[i][j].x = 0;

			Block1[i][j].y = 0;
			Block2[i][j].y = 0;
			Block3[i][j].y = 0;
			Block4[i][j].y = 0;
			Block5[i][j].y = 0;
			Block6[i][j].y = 0;
		}
	}

	for (int i=0; i<AirfoilCoordinate.size(); i++){
		TE = max(TE, AirfoilCoordinate[i].x);
		LE = min(LE, AirfoilCoordinate[i].x);
	}

	c = abs(TE-LE);

	//Block 1 Point Definition
	//Boundary Point Definition
 	Block1[0][0].x = LE - c;
	Block1[0][0].y = 0;
	Block1[dim-1][dim-1].x = LE;
	Block1[dim-1][dim-1].y = c;
 	Block1[dim-1][0].x = LE - c;
	Block1[dim-1][0].y = c;
	Block1[0][dim-1].x = LE;
	Block1[0][dim-1].y = 0;

	for (int i=1; i<dim-1; i++){
		Block1[i][0].x = LE - c;
		Block1[i][dim-1].x = LE;

		Block1[i][0].y = exp(-(dim-1-i)/(dim/5.5)) * c;
		Block1[i][dim-1].y = exp(-(dim-1-i)/(dim/5.5)) * c;
	}

	for (int i=1; i<dim-1; i++){
		Block1[0][i].y = 0;
		Block1[dim-1][i].y = c;

		Block1[0][i].x = LE - exp(-i/(dim/5.5)) * c;
		Block1[dim-1][i].x = LE - exp(-i/(dim/5.5)) * c;
 	}
 	
 	//Internal Point Definition
 	for (int i=1; i<dim-1; i++){
 		for (int j=1; j<dim-1; j++){
		Block1[i][j].x = LE - exp(-j/(dim/5.5)) * c;

		Block1[i][j].y = exp(-(dim-1-i)/(dim/5.5)) * c;
 		}
 	}

	//Block 2 Point Definition
	for (int i=0; i<dim; i++){
		Block2[0][i].y = AirfoilCoordinate[dim-1-i].y;
		for (int j=0; j<dim; j++){
			Block2[j][i].x = AirfoilCoordinate[dim-1-i].x;
			if (j > 0){
				Block2[j][i].y = Block2[0][i].y + exp(-(dim-1-j)/(dim/5.5)) * (c - Block2[0][i].y);
			}
		}
	}

	//Block 3 Point Definition
	//Boundary Point Definition
 	Block3[0][0].x = TE;
	Block3[0][0].y = 0;
	Block3[dim-1][dim-1].x = TE + c;
	Block3[dim-1][dim-1].y = c;
 	Block3[dim-1][0].x = TE;
	Block3[dim-1][0].y = c;
	Block3[0][dim-1].x = TE + c;
	Block3[0][dim-1].y = 0;

	for (int i=1; i<dim-1; i++){
		Block3[i][0].x = TE;
		Block3[i][dim-1].x = TE + c;

		Block3[i][0].y = exp(-(dim-1-i)/(dim/5.5)) * c;
		Block3[i][dim-1].y = exp(-(dim-1-i)/(dim/5.5)) * c;
	}

	for (int i=1; i<dim-1; i++){
		Block3[0][i].y = 0;
		Block3[dim-1][i].y = c;

		Block3[0][i].x = TE + exp(-(dim-1-i)/(dim/5.5)) * c;
		Block3[dim-1][i].x = TE + exp(-(dim-1-i)/(dim/5.5)) * c;
	}
 	
 	//Internal Point Definition
 	for (int i=1; i<dim-1; i++){
 		for (int j=1; j<dim-1; j++){
		Block3[i][j].x = TE + exp(-(dim-1-j)/(dim/5.5)) * c;

		Block3[i][j].y = exp(-(dim-1-i)/(dim/5.5)) * c;
 		}
 	}

	//Block 4 Point Definition
	//Boundary Point Definition
 	Block4[0][0].x = TE;
	Block4[0][0].y = -c;
 	Block4[dim-1][0].x = TE;
	Block4[dim-1][0].y = 0;
	Block4[0][dim-1].x = TE + c;
	Block4[0][dim-1].y = -c;
	Block4[dim-1][dim-1].x = TE + c;
	Block4[dim-1][dim-1].y = 0;

	for (int i=1; i<dim-1; i++){
		Block4[i][0].x = TE;
		Block4[i][dim-1].x = TE + c;

		Block4[i][0].y = exp(-i/(dim/5.5)) * -c;
		Block4[i][dim-1].y = exp(-i/(dim/5.5)) * -c;
	}

	for (int i=1; i<dim-1; i++){
		Block4[0][i].y = -c;
		Block4[dim-1][i].y = 0;

		Block4[0][i].x = TE + exp(-(dim-1-i)/(dim/5.5)) * c;
		Block4[dim-1][i].x = TE + exp(-(dim-1-i)/(dim/5.5)) * c;
	}
 	
 	//Internal Point Definition
 	for (int i=1; i<dim-1; i++){
 		for (int j=1; j<dim-1; j++){
		Block4[i][j].x = TE + exp(-(dim-1-j)/(dim/5.5)) * c;

		Block4[i][j].y = exp(-i/(dim/5.5)) * -c;
 		}
 	}

	//Block 5 Point Definition
	for (int i=dim-1; i>-1; i--){
		Block5[dim-1][i].y = AirfoilCoordinate[dim+1+i].y;
		for (int j=0; j<dim; j++){
			Block5[j][i].x = AirfoilCoordinate[dim+1+i].x;
			//Part below is not well defined
			if (j < dim-1 && j > 0){
				Block5[j][i].y = Block5[dim-1][i].y + exp(-(j)/(dim/5.5)) * (-c + Block5[dim-1][i].y);
			}
			if (j == 0){
				Block5[j][i].y = -c;
			}
		}
	}

	//Block 6 Definition
	//Boundary Point Definition
 	Block6[0][0].x = LE - c;
	Block6[0][0].y = -c;
 	Block6[dim-1][0].x = LE - c;
	Block6[dim-1][0].y = 0;
	Block6[0][dim-1].x = LE;
	Block6[0][dim-1].y = -c;
	Block6[dim-1][dim-1].x = LE;
	Block6[dim-1][dim-1].y = 0;

	for (int i=1; i<dim-1; i++){
		Block6[i][0].x = LE - c;
		Block6[i][dim-1].x = LE;

		Block6[i][0].y = exp(-i/(dim/5.5)) * -c;
		Block6[i][dim-1].y = exp(-i/(dim/5.5)) * -c;
	}

	for (int i=1; i<dim-1; i++){
		Block6[0][i].y = -c;
		Block6[dim-1][i].y = 0;

		Block6[0][i].x = LE - exp(-i/(dim/5.5)) * c;
		Block6[dim-1][i].x = LE - exp(-i/(dim/5.5)) * c;
 	}
 	
 	//Internal Point Definition
 	for (int i=1; i<dim-1; i++){
 		for (int j=1; j<dim-1; j++){
		Block6[i][j].x = LE - exp(-j/(dim/5.5)) * c;

		Block6[i][j].y = exp(-i/(dim/5.5)) * -c;
 		}
 	}
 	
 	vector<vector<cartesian> > ScalarBlock1 = ScalarPointDefinition(Block1);
 	vector<vector<cartesian> > ScalarBlock2 = ScalarPointDefinition(Block2);
 	vector<vector<cartesian> > ScalarBlock3 = ScalarPointDefinition(Block3);
 	vector<vector<cartesian> > ScalarBlock4 = ScalarPointDefinition(Block4);
 	vector<vector<cartesian> > ScalarBlock5 = ScalarPointDefinition(Block5);
 	vector<vector<cartesian> > ScalarBlock6 = ScalarPointDefinition(Block6);
 	
 	int dimy = ScalarBlock1.size() + ScalarBlock6.size();
 	int dimx = ScalarBlock1[0].size() + ScalarBlock2[0].size() + ScalarBlock3[0].size();

 	vector<vector<cartesian> > Hasil(dimy);

	for (int i=0; i<dimy; i++){
		Hasil[i] = vector<cartesian>(dimx);
		for (int j=0; j<dimx; j++){
			if (i < ScalarBlock6.size()){
		 		if (j < ScalarBlock1[0].size()){
		 			Hasil[i][j].x = ScalarBlock6[i][j].x;
		 			Hasil[i][j].y = ScalarBlock6[i][j].y;
 		 		}
		 		else if (j < (ScalarBlock1[0].size() + ScalarBlock2[0].size())){
		 			Hasil[i][j].x = ScalarBlock5[i][j - ScalarBlock1[0].size()].x;
		 			Hasil[i][j].y = ScalarBlock5[i][j - ScalarBlock1[0].size()].y;
		 		}
		 		else{
		 			Hasil[i][j].x = ScalarBlock4[i][j - (ScalarBlock1[0].size() + ScalarBlock2[0].size())].x;
		 			Hasil[i][j].y = ScalarBlock4[i][j - (ScalarBlock1[0].size() + ScalarBlock2[0].size())].y;
		 		}
		 	}
		 	else{
		 		if (j < ScalarBlock1[0].size()){
		 			Hasil[i][j].x = ScalarBlock1[i - ScalarBlock6.size()][j].x;
		 			Hasil[i][j].y = ScalarBlock1[i - ScalarBlock6.size()][j].y;
		 		}
		 		else if (j < (ScalarBlock1[0].size() + ScalarBlock2[0].size())){
		 			Hasil[i][j].x = ScalarBlock2[i - ScalarBlock6.size()][j - ScalarBlock1[0].size()].x;
		 			Hasil[i][j].y = ScalarBlock2[i - ScalarBlock6.size()][j - ScalarBlock1[0].size()].y;
		 		}
		 		else{
		 			Hasil[i][j].x = ScalarBlock3[i - ScalarBlock6.size()][j - (ScalarBlock1[0].size() + ScalarBlock2[0].size())].x;
		 			Hasil[i][j].y = ScalarBlock3[i - ScalarBlock6.size()][j - (ScalarBlock1[0].size() + ScalarBlock2[0].size())].y;
		 		}
		 	}
		}
	}

	//Deleting unnecessary vector
	Block1.erase(Block1.begin(), Block1.begin()+Block1.size());
	Block2.erase(Block2.begin(), Block2.begin()+Block2.size());
	Block3.erase(Block3.begin(), Block3.begin()+Block3.size());
	Block4.erase(Block4.begin(), Block4.begin()+Block4.size());
	Block5.erase(Block5.begin(), Block5.begin()+Block5.size());
	Block6.erase(Block6.begin(), Block6.begin()+Block6.size());
	ScalarBlock1.erase(ScalarBlock1.begin(), ScalarBlock1.begin()+ScalarBlock1.size());
	ScalarBlock2.erase(ScalarBlock2.begin(), ScalarBlock2.begin()+ScalarBlock2.size());
	ScalarBlock3.erase(ScalarBlock3.begin(), ScalarBlock3.begin()+ScalarBlock3.size());
	ScalarBlock4.erase(ScalarBlock4.begin(), ScalarBlock4.begin()+ScalarBlock4.size());
	ScalarBlock5.erase(ScalarBlock5.begin(), ScalarBlock5.begin()+ScalarBlock5.size());
	ScalarBlock6.erase(ScalarBlock6.begin(), ScalarBlock6.begin()+ScalarBlock6.size());
	return Hasil;
}
