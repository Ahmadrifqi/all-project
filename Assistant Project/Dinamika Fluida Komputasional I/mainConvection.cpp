#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#include "cartesian.cpp"
#include "CaseDefinition.cpp"
#include "ImportPoint.cpp"
#include "AirfoilMesh.cpp"
#include "ConvectionSolver.cpp"
using namespace std;

int main(){
	vector<vector<double> > ConvectionResults;
	float dt = 0.01;
	float totaltime = 0.0;
	ofstream OutMeshing;
	ofstream OutputSemiImplicit;
	OutMeshing.open("Meshing.txt");
	OutputSemiImplicit.open("Output Semi Implicit.txt");

	vector<cartesian> InputPoint = ImportPoint("Airfoil.txt");
	cout<<InputPoint.size()<<endl;
	vector<vector<cartesian> > ScalarPoint = AirfoilMesh(InputPoint);

	OutMeshing<<"# Meshing.txt"<<endl;
	OutMeshing<<"# X"<<"\t"<<"Y"<<endl;
	for (int i=0; i<ScalarPoint.size(); i++){
		for (int j=0; j<ScalarPoint[i].size(); j++){
			OutMeshing<<ScalarPoint[i][j].x<<"\t"<<ScalarPoint[i][j].y<<endl;
		}
		OutMeshing<<endl;
	}
	OutMeshing.close();

	//Penyelesaian Persamaan Laplace di sekitar Airfoil
	vector<vector<double> > Case = CaseDefinition(ScalarPoint[0].size(), ScalarPoint.size(), 1.0, 2.0, 3.0, 0.0);
	vector<vector<double> > Casemindt = CaseDefinition(ScalarPoint[0].size(), ScalarPoint.size(), 1.0, 2.0, 3.0, 0.0);
	vector<vector<double> > Casemin2dt = CaseDefinition(ScalarPoint[0].size(), ScalarPoint.size(), 1.0, 2.0, 3.0, 0.0);

	//Convection Solver
	do{
		totaltime += dt;

		ConvectionResults = ConvectionSolver(ScalarPoint, Case, Casemindt, Casemin2dt, dt, totaltime);
		for (int i=0; i<Case.size(); i++){
			for (int j=0; j<Case[i].size(); j++){
				Casemin2dt[i][j] = Casemindt[i][j];
				Casemindt[i][j] = ConvectionResults[i][j];
			}
		}

		OutputSemiImplicit<<"Hasil pada Time Step : "<<totaltime<<endl;
		for (int i=0; i<Case.size(); i++){
			for (int j=0; j<Case[i].size(); j++){
				OutputSemiImplicit<<ConvectionResults[i][j]<<"\t";
			}
			OutputSemiImplicit<<endl;
		}
		OutputSemiImplicit<<endl;
	}
	while(totaltime < 1);

	return 0;
}