#include <vector>
#include "cartesian.cpp"
using namespace std;


vector<vector<double> > ExplicitMethod(vector<vector<double> > &Case, vector<vector<cartesian> > &Coordinate);
vector<vector<double> > SemiImplicitMethod(vector<vector<double> > &Case, vector<vector<cartesian> > &Coordinate);
vector<vector<double> > ImplicitMethod(vector<vector<double> > &Case, vector<vector<cartesian> > &Coordinate);
