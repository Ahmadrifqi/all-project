#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#include "cartesian.cpp"
#include "CaseDefinition.cpp"
#include "ImportPoint.cpp"
#include "Method.h"
#include "AirfoilMesh.cpp"
using namespace std;

int main(){
	ofstream OutMeshing;
	ofstream OutputExplicit;
	ofstream OutputImplicit;
	ofstream OutputSemiImplicit;
	OutMeshing.open("Meshing.txt");
	OutputExplicit.open("Output Explicit.txt");
	OutputImplicit.open("Output Implicit.txt");
	OutputSemiImplicit.open("Output Semi Implicit.txt");
	int x = 5;
	int y = 4;
	double NorthBC = 0.1;
	double SouthBC = 0.1;
	double WestBC = 0.1;
	double EastBC = 0.1;

	vector<cartesian> InputPoint = ImportPoint("Airfoil.txt");
	cout<<InputPoint.size()<<endl;
	vector<vector<cartesian> > ScalarPoint = AirfoilMesh(InputPoint);

	OutMeshing<<"# Meshing.txt"<<endl;
	OutMeshing<<"# X"<<"\t"<<"Y"<<endl;
	for (int i=0; i<ScalarPoint.size(); i++){
		for (int j=0; j<ScalarPoint[i].size(); j++){
			OutMeshing<<ScalarPoint[i][j].x<<"\t"<<ScalarPoint[i][j].y<<endl;
		}
		OutMeshing<<endl;
	}
	OutMeshing.close();

	//Penyelesaian Persamaan Laplace di sekitar Airfoil
	vector<vector<double> > Case = CaseDefinition(ScalarPoint[0].size(), ScalarPoint.size(), 1.0, 2.0, 3.0, 4.0);

	vector<vector<double> > ExplicitResults = ExplicitMethod(Case, ScalarPoint);

	vector<vector<double> > ImplicitResults = ImplicitMethod(Case, ScalarPoint);

	vector<vector<double> > SemiImplicitResults = SemiImplicitMethod(Case, ScalarPoint);
	
	for (int i=0; i<ExplicitResults.size(); i++){
		for (int j=0; j<ExplicitResults[i].size(); j++){
			OutputExplicit<<ExplicitResults[i][j]<<"\t";
		}
		OutputExplicit<<endl;
	}
	OutputExplicit.close();
	
	for (int i=0; i<ImplicitResults.size(); i++){
		for (int j=0; j<ImplicitResults[i].size(); j++){
			OutputImplicit<<ImplicitResults[i][j]<<"\t";
		}
		OutputImplicit<<endl;
	}
	OutputImplicit.close();

	for (int i=0; i<SemiImplicitResults.size(); i++){
		for (int j=0; j<SemiImplicitResults[i].size(); j++){
			OutputSemiImplicit<<SemiImplicitResults[i][j]<<"\t";
		}
		OutputSemiImplicit<<endl;
	}
	OutputSemiImplicit.close();
	
	return 0;
}