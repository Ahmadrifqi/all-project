/*
The codewasmadeunder the assumption that the domain is rectangular
*/
#include <iostream>
#include <vector>
using namespace std;

vector<vector<cartesian> > ScalarPointDefinition(vector<vector<cartesian> > Points){
	vector<vector<cartesian> > Hasil(Points.size()-1);

	for(int i=0; i<Points.size()-1; i++){
		Hasil[i] = vector<cartesian>(Points[i].size()-1);
		for (int j=0; j<Points[i].size()-1; j++){
			Hasil[i][j].x = (Points[i][j].x + Points[i+1][j].x + Points[i][j+1].x + Points[i+1][j+1].x)/4.0;
			Hasil[i][j].y = (Points[i][j].y + Points[i+1][j].y + Points[i][j+1].y + Points[i+1][j+1].y)/4.0;
		}
	}
 
	return Hasil;
}
