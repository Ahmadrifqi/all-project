#include <iostream>
using namespace std;

int add(int* a, int* b, int dim);

int main(){
	int a[10], int b[10];

	for (int i=0; i<10; i++){
		a[i] = i;
		b[i] = i;
	}

	return 0;
}

int* add(int* a, int* b, int dim){
	int* c;
	c = new int[dim];

	for (int i=0; i<dim; i++){
		c[i] = a[i] + b[i];
	}

	return c;
}