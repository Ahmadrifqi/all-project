#include <vector>
using namespace std;

#ifndef _SOLVERMETHOD_H_
#define _SOLVERMETHOD_H_

vector<vector<double>> ExplicitMethod(vector<vector<double>> &Case, vector<vector<cartesian>> &Coordinate);
vector<vector<double>> SemiImplicitMethod(vector<vector<double>> &Case, vector<vector<cartesian>> &Coordinate);
vector<vector<double>> ImplicitMethod(vector<vector<double>> &Case, vector<vector<cartesian>> &Coordinate);

#endif