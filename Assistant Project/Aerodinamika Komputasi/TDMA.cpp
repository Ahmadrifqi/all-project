#include <iostream>
#include <vector>

using namespace std;

vector<double> TDMA(vector<vector<double> > &matrix, vector<double> &vector){
	//determine the matrix rows and columns
	int row = matrix.size();
	int cols = matrix[0].size();
	
	double newmatrix[row][cols];
	double newvector[row];
	std::vector<double> results(row);

	//is the matrix three-diagonal or not
	for (int i=0; i<row; i++){
		for (int j=0; j<cols; j++){
			newmatrix[i][j] = matrix[i][j];
			if (i == 0){
				if (j != 0 && j != 1){
					if (matrix[i][j] != 0){
						cout<<"Bukan Matrix Tri-Diagonal"<<endl;
						break;
					}
				}				
			}
			else if(i == row-1){
				if (j != row-2 && j != row-1){
					if (matrix[i][j] != 0){
						cout<<"Bukan Matrix Tri-Diagonal"<<endl;
						break;
					}
				}
			}
			else {
				if (j != i-1 && j != i && j!= i+1){
					if (matrix[i][j] != 0){
						cout<<"Bukan Matrix Tri-Diagonal"<<endl;
						break;						
					}
				}
			}
		}
		newvector[i] = vector[i];
		results[i] = 0;
	}
	
	//new matrix and vector definition
	//backward substitutions will be used
	double coefficient = 0;
	for (int i=1; i<row; i++){
		coefficient = newmatrix[i][i-1]/newmatrix[i-1][i-1];
		newmatrix[i][i-1] = newmatrix[i][i-1] - coefficient*newmatrix[i-1][i-1];
		newmatrix[i][i] = newmatrix[i][i] - coefficient*newmatrix[i-1][i];
		newvector[i] = vector[i] - coefficient*newvector[i-1];
	}
	
	//applying backward substitutions
	for (int i=row-1; i>-1; i--){
		if (i == row-1){
			results[i] = newvector[i]/newmatrix[i][i];
		}
		else{
			results[i] = (newvector[i] - newmatrix[i][i+1]*results[i+1])/newmatrix[i][i];
		}
	}
	
	return results;
}
