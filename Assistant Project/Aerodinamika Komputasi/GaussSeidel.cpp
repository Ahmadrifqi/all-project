#include <cmath>
#include <vector>
#include <iostream>
#include "Pivoting.h"
using namespace std;

vector<double> GaussSeidel(vector<vector<double> > &Matrix, vector<double> &Vector){
	int dim = Matrix.size();
	double CalMat[dim][dim];
	double CalVec[dim];
	vector<double> results(dim);
	double error = 0;
	double sum = 0;
	
	vector<vector<double> > AugMat = Pivoting(Matrix, Vector);

	for (int i=0; i<dim; i++){
		for (int j=0; j<dim; j++){
			CalMat[i][j] = AugMat[i][j];
		}
		CalVec[i] = AugMat[i][dim];
		results[i] = 0;
	}

	do{
		error = 0;
		double Temp[dim];
		for (int i=0; i<dim; i++){
			Temp[i] = results[i];
			for (int j=0; j<dim; j++){
				if (j != i)
					sum -= results[j] * CalMat[i][j];
			}
			sum += CalVec[i];
			results[i] = sum/CalMat[i][i];
			sum = 0;
		}

		for (int i=0; i<dim; i++){
			error = max(error, abs(results[i] - Temp[i]));
		}
	}
	while(error > 0.0001);

	/*
	Delete vector AugMat
	*/
	AugMat.erase(AugMat.begin(), AugMat.begin()+AugMat.size());

	return results;
}
