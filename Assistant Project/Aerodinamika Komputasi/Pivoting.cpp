#include <iostream>
#include <cmath>
#include <vector>
#include "GaussSeidel.cpp"
using namespace std;

int main(){
	double matrix[3][3] = {{2, 5, 1}, {3, 5, 9}, {-7, 1, 1}};
	std::vector<std::vector<double> > a(3);
	std::vector<double> b = {1,2,3};
	double AugMatrix[3][4];
	double NewAugMatrix[3][4];
	int PosMax[3];
	double Max = 0;
	bool Pos[3];
	bool Confirm = true;
	char COntinue;
	std::vector<double> v(3);

	for (int i=0; i<3; i++){
		a[i] = std::vector<double>(3);
		for (int j=0; j<3; j++){
			a[i][j] = matrix[i][j];
		}
	}

	v = GaussSeidel(a, b);
	for (int i=0; i<3; i++){
		cout<<"Hasil : "<<v[i]<<endl;
	}

	/*for (int i=0; i<3; i++){
		for (int j=0; j<4; j++){
			if (j<3)
				AugMatrix[i][j] = matrix[i][j];
			else
				AugMatrix[i][j] = vector[i];
		}
		PosMax[i] = 0;
		Pos[i] = false;
	}

	for (int i=0; i<3; ++i){
		for (int j=0; j<3; j++){
			Max = AugMatrix[i][PosMax[i]];
			if (abs(AugMatrix[i][j]) > abs(Max)){
				PosMax[i] = j;
			}
		}
	}

	for (int i = 0; i < 3; ++i){
		if (Pos[PosMax[i]] == false)
			Pos[PosMax[i]] = true;
		else
			Confirm = false;
	}

	if (Confirm){
		for (int i = 0; i<3; i++){
			for (int j=0; j<4; j++){
				NewAugMatrix[PosMax[i]][j] = AugMatrix[i][j];
			}
		}

		for (int i = 0; i<3; i++){
			for (int j=0; j<4; j++){
				cout<<NewAugMatrix[i][j]<<"\t";
			}
			cout<<endl;
		}
	}

	else{
		cout<<"The matrix is not diagonally dominant."<<endl;
		cout<<" The calculation might be unstable."<<endl;
		cout<<" Do you still want to proceed (Y/N)? ";
		cin>>COntinue;
		while (COntinue != 'N' && COntinue != 'Y'){
			cout<<"Input yang bener des!";
			cin>>COntinue;
		}
		if (COntinue == 'N'){
			return 0;
		}
		if (COntinue == 'Y'){
			for (int i=0; i<3; i++){
				for (int j=0; j<4; j++){
					cout<<AugMatrix[i][j]<<"\t";
				}
				cout<<endl;
			}			
		}
	}*/

	return 0;
}